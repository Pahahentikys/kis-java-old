package ru.it2g.kis.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.it2g.kis.entity.Inventory;
import ru.it2g.kis.entity.ReceiptOrder;
import ru.it2g.kis.repository.ReceiptOrderRepository;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Optional;

/**
 * Created by RyabushevSA on 29.09.2017
 */
@Service
public class ReceiptOrderServiceImpl implements ReceiptOrderService {

    private final ReceiptOrderRepository receiptOrderRepository;

    @Autowired
    public ReceiptOrderServiceImpl(ReceiptOrderRepository receiptOrderRepository) {
        this.receiptOrderRepository = receiptOrderRepository;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ReceiptOrder create(String name) {

        if (name == null || name.isEmpty())
            throw new IllegalArgumentException("Название Приходного ордера не может быть пустым");

        ReceiptOrder newRecipeOrder = ReceiptOrder.builder()
                .name(name)
                .dateCreate(new Date())
                .inventories(Collections.emptyList())
                .build();

        return receiptOrderRepository.save(newRecipeOrder);
    }

    @Override
    @Transactional
    public ReceiptOrder addInventory(ReceiptOrder receiptOrder, Inventory inventory) {

        receiptOrder.getInventories().add(inventory);
        inventory.setReceiptOrder(receiptOrder);
        return receiptOrderRepository.save(receiptOrder);
    }

    @Override
    @Transactional
    public ReceiptOrder removeInventory(ReceiptOrder receiptOrder, Inventory inventory) {

        receiptOrder.getInventories().remove(inventory);
        return receiptOrder;
    }

    /**
     * "Мягкое" удаления ордера.
     *
     * @param id - идентификатор ордера.
     */
    @Override
    @Transactional
    public void remove(Long id) {
        ReceiptOrder receiptOrder = Optional.ofNullable(receiptOrderRepository.findOne(id))
                .orElseThrow(() -> new IllegalArgumentException("Сущности с данным id не найдено!"));

        receiptOrder.setActive(false);
        receiptOrderRepository.save(receiptOrder);
    }

    @Override
    public ReceiptOrder getById(Long id) {
        return receiptOrderRepository.findOne(id);
    }

    @Override
    public Collection<ReceiptOrder> getAll() {
        return receiptOrderRepository.findAll();
    }

    @Override
    public ReceiptOrder create(ReceiptOrder receiptOrder) {
        return receiptOrderRepository.save(receiptOrder);
    }

    @Override
    public ReceiptOrder save(ReceiptOrder receiptOrder) {
        return receiptOrderRepository.save(receiptOrder);
    }

    /**
     * Поиск ордера по типу.
     *
     * @param typeId - идентификатор ордера.
     * @return - ордер с определённым типом.
     */
    @Override
    @Transactional
    public Collection<ReceiptOrder> findByOrderTypeId(Long typeId) {
        return receiptOrderRepository.findByOrderTypeId(typeId);
    }
}
