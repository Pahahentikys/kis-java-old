package ru.it2g.kis.service;

import lombok.NonNull;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.it2g.kis.entity.dictionary.InventoryCategory;
import ru.it2g.kis.repository.InventoryCategoryRepository;

import java.util.Collection;
import java.util.Optional;

/**
 * @author Pahahentikys on 04.04.2018
 * Реализация сервиса для сущности "категория продуктов" (InventoryCategory).
 */
@Service
public class InventoryCategoryServiceImpl implements InventoryCategoryService {

    /**
     * Репозиторий для сушности "категория продуктов".
     */
    private final InventoryCategoryRepository inventoryCategoryRepository;

    /**
     * Контруктор сервиса для сущности "категория продуктов".
     *
     * @param inventoryCategoryRepository - репозиторий для сущности "категория продуктов".
     */
    InventoryCategoryServiceImpl(InventoryCategoryRepository inventoryCategoryRepository) {
        this.inventoryCategoryRepository = inventoryCategoryRepository;
    }

    /**
     * Создание категории продуктов.
     *
     * @param inventoryCategory - экземпляр сущности "категория продукта".
     * @return - созданная категория продуктов.
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public InventoryCategory create(@NonNull InventoryCategory inventoryCategory) {

        if (inventoryCategory.getId() != null)
            throw new IllegalArgumentException("При создании категории не должно быть указано id!");

        return inventoryCategoryRepository.save(inventoryCategory);
    }

    /**
     * Обновление информации о категории продуктов.
     *
     * @param inventoryCategory - экземпляр сущности "категория продукта".
     * @return - категория с обновлённой информацией.
     */
    @Override
    public InventoryCategory update(@NonNull InventoryCategory inventoryCategory) {

        if (inventoryCategory.getId() == null)
            throw new IllegalArgumentException("Id изменяемого объекта \"категория продуктов\" не должен быть null");

        if (inventoryCategoryRepository.findOne(inventoryCategory.getId()) == null)
            throw new IllegalArgumentException("Записи с таким идентификатором не существует в БД");

        return inventoryCategoryRepository.save(inventoryCategory);
    }

    /**
     * Получение всех (существующий и несуществующих) категорий продуктов.
     *
     * @return - коллекция существующий и несуществующих категорий продуктов.
     */
    @Override
    public Collection<InventoryCategory> getAll() {

        return inventoryCategoryRepository.findAll();
    }

    /**
     * Получение категории продукта по идентификатору.
     *
     * @param id - идентификатор категории.
     * @return - продукт с конкретным идентификатором.
     */
    @Override
    public InventoryCategory getById(@NonNull Long id) {

        return inventoryCategoryRepository.findOne(id);
    }

    /**
     * Получение активных категорий продуктов по наименованию.
     *
     * @param categoryName - наименование категории.
     * @return - коллекция активных категорий продуктов с определённым именем.
     */
    @Override
    public Collection<InventoryCategory> findByName(@NonNull String categoryName) {

        return inventoryCategoryRepository.findByNameStartingWithIgnoreCaseAndActiveTrue(categoryName);
    }

    /**
     * Получение активных категорий по идентификатору вышестоящей категории.
     *
     * @param parentCategoryId - идентификатор вышестоящей категории.
     * @return - коллекция активных категорий продуктов с определённым идентификатором вышестоящей категории.
     */
    @Override
    public Collection<InventoryCategory> findByParent(@NonNull Long parentCategoryId) {

        return inventoryCategoryRepository.findByParent_IdAndActiveTrue(parentCategoryId);
    }

    /**
     * "Мягкое" удаление категории продуктов.
     *
     * @param id - идентификатор категории.
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void remove(@NonNull Long id) {

        InventoryCategory inventoryCategory = Optional.ofNullable(inventoryCategoryRepository.findOne(id))
                .orElseThrow(() -> new IllegalArgumentException(String.format("С таким id=%d категории продуктов не найдено!", id)));

        inventoryCategory.setActive(false);
        inventoryCategoryRepository.save(inventoryCategory);
    }

    /**
     * Поиск активных категорий продуктов.
     *
     * @return - коллекция активных категорий продуктов.
     */
    @Override
    public Collection<InventoryCategory> getByActiveTrue() {

        return inventoryCategoryRepository.findByActiveTrue();
    }
}
