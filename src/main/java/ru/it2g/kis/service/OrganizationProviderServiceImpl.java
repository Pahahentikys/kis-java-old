package ru.it2g.kis.service;

import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.it2g.kis.entity.dictionary.OrganizationProvider;
import ru.it2g.kis.repository.OrganizationProviderRepository;

import java.util.Collection;
import java.util.Optional;

/**
 * @author Pahahentikys on 08.02.2018
 * Реализация сервиса для сущности: организация-поставщик (OrganizationProvider).
 */
@Service
public class OrganizationProviderServiceImpl implements OrganizationProviderService {

    /**
     * Репозиторий для работы с сущностью: организация-поставщик.
     */
    private final OrganizationProviderRepository organizationProviderRepository;

    /**
     * Конструктор объекта сервиса для работы с организацией-поставщиком.
     *
     * @param organizationProviderRepository - репозиторий для работы с сущностью: организация-поставщик.
     */
    @Autowired
    OrganizationProviderServiceImpl(OrganizationProviderRepository organizationProviderRepository) {
        this.organizationProviderRepository = organizationProviderRepository;
    }

    /**
     * Создание организации-поставщика.
     *
     * @param organizationProvider - экземпляр организации-поставщика.
     * @return - созданная сущность организации-поставщика.
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public OrganizationProvider create(@NonNull OrganizationProvider organizationProvider) {

        if (organizationProvider.getId() != null)
            throw new IllegalArgumentException("При создании организации-поставщика не должно быть указано id!");

        return organizationProviderRepository.save(organizationProvider);
    }

    /**
     * Обновление организации-поставщика.
     *
     * @param organizationProvider - экземпляр организации-поставщика.
     * @return - обновлённая сущность организации-поставщика.
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public OrganizationProvider update(@NonNull OrganizationProvider organizationProvider) {
        if (organizationProvider.getId() == null)
            throw new IllegalArgumentException("Идентификатор изменяемого объекта организация-поставщика не должен быть null!");

        if (organizationProviderRepository.findOne(organizationProvider.getId()) == null)
            throw new IllegalArgumentException("Невозможно обновить организацию-поставщика, такой записи не существует в БД!");

        return organizationProviderRepository.save(organizationProvider);
    }

    /**
     * Получение всех организаций-поставщиков.
     *
     * @return - коллекция организаций-поставщиков.
     */
    @Override
    public Collection<OrganizationProvider> getAll() {
        return organizationProviderRepository.findAll();
    }

    /**
     * Получение организации-поставщика по идентификатору.
     *
     * @param id - индетификатор организации-поставщика.
     * @return - организация-поставщик с конкретным идентификатором.
     */
    @Override
    public OrganizationProvider getById(Long id) {
        return organizationProviderRepository.findOne(id);
    }

    /**
     * Получение активных организаций-поставщиков.
     *
     * @return - коллекций активный организаций-поставщиков.
     */
    @Override
    public Collection<OrganizationProvider> getByActiveTrue() {
        return organizationProviderRepository.findByActiveTrue();
    }

    /**
     * "Мягкое" удаление организации-поставщика.
     *
     * @param id - идентификатор организации поставщика.
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void remove(Long id) {
        OrganizationProvider organizationProvider = Optional.ofNullable(organizationProviderRepository.findOne(id))
                .orElseThrow(() -> new IllegalArgumentException("Сущности: организация-поставщик с данным идентификатором не найдено!"));

        organizationProvider.setActive(false);
        organizationProviderRepository.save(organizationProvider);
    }

}
