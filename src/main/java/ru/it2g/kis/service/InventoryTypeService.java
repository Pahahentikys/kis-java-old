package ru.it2g.kis.service;

import ru.it2g.kis.entity.InventoryType;

import java.util.Collection;

/**
 * Сервис бизнес логики для сущности Тип ТМЦ @see InventoryType.
 *
 * @author Created by ZotovES on 27.02.2018
 */

public interface InventoryTypeService {
    InventoryType create(String name);

    InventoryType save(InventoryType inventoryType);

    InventoryType getById(long id);

    Collection<InventoryType> getAll();

    Collection<InventoryType> getByActiveTrue();

    void remove(long id);
}
