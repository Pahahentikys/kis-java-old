package ru.it2g.kis.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.it2g.kis.entity.InventoryGroup;
import ru.it2g.kis.repository.InventoryGroupRepository;

import java.util.Collection;

/**
 * Сервис для сущности Группы товаров @see InventoryGroup
 *
 * @author Created by ZotovES on 02.02.2018
 */
@Service
public class InventoryGroupServiceImpl implements InventoryGroupService {
    private final InventoryGroupRepository inventoryGroupRepository;

    /**
     * Конструктор сервис Группы товаров.
     *
     * @param inventoryGroupRepository - репозиторий Группы товаров
     */
    @Autowired
    public InventoryGroupServiceImpl(InventoryGroupRepository inventoryGroupRepository) {
        this.inventoryGroupRepository = inventoryGroupRepository;
    }

    /**
     * Создание группы товаров
     *
     * @param name - строковое наименование группы.
     * @return экзкмпляр созданной группы.
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public InventoryGroup create(String name) {
        if (name == null || name.isEmpty())
            throw new IllegalArgumentException("Имя группы ТМЦ не может быть пустым");

        InventoryGroup inventoryGroup = InventoryGroup.builder()
                .name(name)
                .active(true)
                .build();

        return inventoryGroupRepository.save(inventoryGroup);
    }

    /**
     * Сохраняет объект
     *
     * @param inventoryGroup - объект группы товаров для сохранения.
     * @return сохранення группа товаров.
     */
    @Override
    public InventoryGroup save(InventoryGroup inventoryGroup) {
        if (inventoryGroup == null || inventoryGroup.getName().isEmpty())
            throw new IllegalArgumentException("Имя группы ТМЦ не может быть пустым");

        return inventoryGroupRepository.save(inventoryGroup);
    }

    /**
     * Удаление группы товара по идентификатору.
     *
     * @param id - удентификатор группы.
     */
    @Override
    public void remove(long id) {
        InventoryGroup inventoryGroup = inventoryGroupRepository.findOne(id);
        if (inventoryGroup == null) return;

        inventoryGroup.setActive(false);
        inventoryGroupRepository.save(inventoryGroup);
    }

    /**
     * Поиск группы товаров по идентификатору.
     *
     * @param id - идентификатор группы.
     * @return объект найденной группы.
     */
    @Override
    public InventoryGroup getById(long id) {
        return inventoryGroupRepository.findOne(id);
    }

    /**
     * Возвращает список только активных групп.
     *
     * @return - коллекция групп.
     */
    @Override
    public Collection<InventoryGroup> getByActive() {
        return inventoryGroupRepository.findByActiveTrue();
    }

    /**
     * Возвращает все группы вне зависимости от признака активности.
     *
     * @return коллекция групп.
     */
    @Override
    public Collection<InventoryGroup> getAll() {
        return inventoryGroupRepository.findAll();
    }
}
