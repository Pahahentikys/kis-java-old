package ru.it2g.kis.service;

import ru.it2g.kis.entity.dictionary.OrganizationProvider;

import java.util.Collection;

/**
 * @author Pahahentikys on 08.02.2018
 * Сервис для сущности: организация-поставщик (OrganizationProvider).
 */
public interface OrganizationProviderService {

    /**
     * Создание организации-поставщика.
     *
     * @param organizationProvider - экземпляр организации-поставщика.
     * @return - созданный экземпляр организации-поставщика.
     */
    OrganizationProvider create(OrganizationProvider organizationProvider);

    /**
     * Обновление организации-поставщика.
     *
     * @param organizationProvider - экземпляр организации-поставщика.
     * @return - обновлённый экземпляр организации-поставщика.
     */
    OrganizationProvider update(OrganizationProvider organizationProvider);

    /**
     * Получение всех организаций-поставщиков.
     *
     * @return - коллекция организаций-поставщиков.
     */
    Collection<OrganizationProvider> getAll();

    /**
     * Получение организации-поставщика по идентификатору.
     *
     * @param id - индетификатор организации-поставщика.
     * @return - организация-поставщик с определённым идентификатором.
     */
    OrganizationProvider getById(Long id);

    /**
     * Получение коллекции активных организаций-поставщиков.
     *
     * @return - коллекция активных организаций-поставщиков.
     */
    Collection<OrganizationProvider> getByActiveTrue();

    /**
     * Удаление организации-поставщика по идентификатору.
     *
     * @param id - идентификатор организации поставщика.
     */
    void remove(Long id);

}
