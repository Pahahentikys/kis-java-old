package ru.it2g.kis.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.it2g.kis.entity.dictionary.Contractor;
import ru.it2g.kis.repository.ContractorRepository;

import java.util.Collection;
import java.util.Date;

/**
 * Реализация сервиса бизнес логики для класса сущьности Контрагент.
 *
 * @author Created by ZotovES on 27.11.2017
 * @see Contractor
 */

@Service
public class ContractorServiceImpl implements ContractorService {
    private final ContractorRepository contractorRepository;

    /**
     * Конструктор объекта сервиса.
     *
     * @param contractorRepository - репозиторий сущьности Контрагента.
     */
    @Autowired
    public ContractorServiceImpl(ContractorRepository contractorRepository) {
        this.contractorRepository = contractorRepository;
    }

    /**
     * Создает и сохраняет контрагента.
     *
     * @param name - наименование контрагента.
     * @return созданный объект контрагента.
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Contractor create(String name) {
        if (name == null || name.isEmpty())
            throw new IllegalArgumentException("Наименование контрагента не может быть пустым");

        Contractor newContractor = Contractor.builder()
                .name(name)
                .dateBegin(new Date())
                .dateEnd(null)
                .active(true)
                .build();

        return contractorRepository.save(newContractor);
    }

    /**
     * Устанавливает дату окончания действия контрагента в случае переключения в статус неактивного.
     * Устанавливает дату начала действия при переключении контрагента в статус активного
     * , сбрасывает на null дату окончания действия.
     * @param contractor - Контрагент, объект класса Contractor.
     * @return Контрагент, объект класса Contractor, с измененными датами.
     */
    public Contractor changeDateBeginAndDateEnd(Contractor contractor){
        if (contractorRepository.findOne(contractor.getId()).isActive() != contractor.isActive()) {
            if (contractor.isActive()) {
                contractor.setDateBegin(new Date());
                contractor.setDateEnd(null);
            } else {
                contractor.setDateEnd(new Date());
            }
        }
        return contractor;
    }

    /**
     * Сохраняет объект контрагента.
     *
     * @param contractor - Контрагент, объект класса Contractor.
     * @return сохраненый объект.
     */
    @Override
    public Contractor save(Contractor contractor) {
        if (contractor == null || contractor.getName().isEmpty())
            throw new IllegalArgumentException("Наименование контрагента не может быть пустым");

        contractor = changeDateBeginAndDateEnd(contractor);

        return contractorRepository.save(contractor);
    }

    /**
     * Находит контрагента по идентификатору.
     *
     * @param id - идентификатор контрагента.
     * @return найденного контрагента.
     */
    @Override
    public Contractor getById(long id) {
        return contractorRepository.findOne(id);
    }

    /**
     * Получает всех контрагентов вне зависимости от статуса активности.
     *
     * @return коллекцию контрагентов.
     */
    @Override
    public Collection<Contractor> getAll() {
        return contractorRepository.findAll();
    }

    /**
     * Удаление контрагента по идентификатору.
     *
     * @param id - идентификатор контрагента.
     */
    @Override
    public void remove(long id) {
        Contractor contractor = contractorRepository.findOne(id);
        contractor.setActive(false);
        contractor = changeDateBeginAndDateEnd(contractor);
        contractorRepository.save(contractor);
    }

    /**
     * Получает список активных контрагентов.
     *
     * @return - коллекцию контрагентов.
     */
    @Override
    public Collection<Contractor> getByActiveTrue() {
        return contractorRepository.findByActiveTrue();
    }

}
