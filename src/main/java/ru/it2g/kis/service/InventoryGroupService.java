package ru.it2g.kis.service;

import ru.it2g.kis.entity.InventoryGroup;

import java.util.Collection;

/**
 * Сервис бизнес логики для сущностьсти Группа товаров @see InventoryGroup.
 *
 * @author Created by ZotovES on 02.02.2018
 */
public interface InventoryGroupService {
    InventoryGroup create(String name);

    InventoryGroup save(InventoryGroup inventoryGroup);

    void remove(long id);

    InventoryGroup getById(long id);

    Collection<InventoryGroup> getByActive();

    Collection<InventoryGroup> getAll();

}
