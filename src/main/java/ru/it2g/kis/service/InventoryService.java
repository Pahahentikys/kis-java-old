package ru.it2g.kis.service;

import ru.it2g.kis.entity.Inventory;

/**
 * Created by RyabushevSA on 29.09.2017
 */
public interface InventoryService {

    Inventory save(Inventory inventory);

}
