package ru.it2g.kis.service;

import ru.it2g.kis.entity.dictionary.Contractor;

import java.util.Collection;
import java.util.Date;
/**
 * Сервис бизнес логики для сущьностьсти Контрагент @see Contractor.
 * @author Created by ZotovES on 27.11.2017
 */

public interface ContractorService {
    Contractor create(String name);

    Contractor save(Contractor contractor);

    Contractor changeDateBeginAndDateEnd(Contractor contractor);

    Contractor getById(long id);

    Collection<Contractor> getAll();

    Collection<Contractor> getByActiveTrue();

    void remove(long id);
}
