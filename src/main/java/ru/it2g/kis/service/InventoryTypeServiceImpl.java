package ru.it2g.kis.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.it2g.kis.entity.InventoryType;
import ru.it2g.kis.repository.InventoryTypeRepository;

import java.util.Collection;
import java.util.Optional;

/**
 * Реализация сервиса бизнес логики для сущьности Тип ТМЦ.
 *
 * @author Created by ZotovES on 27.02.2018
 * @see ru.it2g.kis.entity.InventoryType
 */

@Service
public class InventoryTypeServiceImpl implements InventoryTypeService {
    private final InventoryTypeRepository inventoryTypeRepository;

    /**
     * Конструктор сервиса Типа ТМЦ.
     *
     * @param inventoryTypeRepository - Объект репозитория типа ТМЦ.
     */
    @Autowired
    public InventoryTypeServiceImpl(InventoryTypeRepository inventoryTypeRepository) {
        this.inventoryTypeRepository = inventoryTypeRepository;
    }

    /**
     * Создает и сохраняет тип ТМЦ.
     *
     * @param name - Наимнование типа ТМЦ.
     * @return созданную сущность типа ТМЦ.
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public InventoryType create(String name) {
        if (name == null || name.isEmpty())
            throw new IllegalArgumentException("Наименование типа ТМЦ не может быть пустым");

        InventoryType inventoryType = InventoryType.builder()
                .name(name)
                .active(true)
                .build();

        return inventoryTypeRepository.save(inventoryType);
    }

    /**
     * Сохраняет измененную сущность типа ТМЦ.
     *
     * @param inventoryType входящий тип ТМЦ.
     * @return сохраненная сущьность тип ТМЦ.
     */
    @Override
    public InventoryType save(InventoryType inventoryType) {
        if (inventoryType == null || inventoryType.getName().isEmpty())
            throw new IllegalArgumentException("Наименование типа ТМЦ не может быть пустым");

        return inventoryTypeRepository.save(inventoryType);
    }

    /**
     * Получить тип ТМЦ по идентификатору.
     *
     * @param id идентификатор типа ТМЦ.
     * @return найденная сущность типа ТМЦ.
     */
    @Override
    public InventoryType getById(long id) {
        return inventoryTypeRepository.findOne(id);
    }

    /**
     * Получить все типы ТМЦ.
     *
     * @return список типов ТМЦ.
     */
    @Override
    public Collection<InventoryType> getAll() {
        return inventoryTypeRepository.findAll();
    }

    /**
     * Получить все активные типы ТМЦ.
     *
     * @return список активный типов ТМЦ.
     */
    @Override
    public Collection<InventoryType> getByActiveTrue() {
        return inventoryTypeRepository.findByActiveTrue();
    }

    /**
     * Удаление записи типа ТМЦ.
     *
     * @param id Идентификатор.
     */
    @Override
    public void remove(long id) {
        Optional.ofNullable(inventoryTypeRepository.findOne(id))
                .ifPresent(it -> {
                    it.setActive(true);
                    inventoryTypeRepository.save(it);
                });
    }
}
