package ru.it2g.kis.service;

import ru.it2g.kis.entity.Inventory;
import ru.it2g.kis.entity.ReceiptOrder;

import java.util.Collection;

/**
 * Created by RyabushevSA on 29.09.2017
 * Сервис для сущности - ReceiptOrder.
 */
public interface ReceiptOrderService {

    ReceiptOrder create(String name);

    /**
     * Добавление инвентаря в приходный ордер.
     *
     * @param receiptOrder - экземпляр приходного ордера.
     * @param inventory    - инвентарь, добавляемый в приходный ордер.
     * @return - приходный ордер с добавленным инвентарём.
     */
    ReceiptOrder addInventory(ReceiptOrder receiptOrder, Inventory inventory);

    /**
     * Удаление инвентаря в приходный ордер.
     *
     * @param receiptOrder - экземпляр приходного ордера.
     * @param inventory    - инвентарь, удаляемый из приходного ордера.
     * @return - приходный ордер с удалённым инвентарём.
     */
    ReceiptOrder removeInventory(ReceiptOrder receiptOrder, Inventory inventory);

    /**
     * "Мягкое удаление приходного ордера"
     *
     * @param id - идентификатор ордера.
     */
    void remove(Long id);

    /**
     * Получение ордера по id.
     *
     * @param id - идентификатор ордера.
     * @return - ордер с определённым id.
     */
    ReceiptOrder getById(Long id);

    /**
     * Получение всех ордеров.
     * @return - список ордеров.
     */
    Collection<ReceiptOrder> getAll();

    /**
     * Создание приходного ордера.
     * @param receiptOrder - экземпляр приходного ордера.
     * @return -  созданный экземпляр приходного.
     */
    ReceiptOrder create(ReceiptOrder receiptOrder);

    /**
     * Сохранение приходного ордера.
     * @param receiptOrder - экземпляр приходного ордера.
     * @return - сохраняемый экземпляр приходного ордера.
     */
    ReceiptOrder save(ReceiptOrder receiptOrder);

    /**
     * Поиск приходного ордера по типу ордера.
     *
     * @param typeId - идентификатор типа ордера.
     * @return - коллекция ордеров с определённым типом.
     */
    Collection<ReceiptOrder> findByOrderTypeId(Long typeId);
}
