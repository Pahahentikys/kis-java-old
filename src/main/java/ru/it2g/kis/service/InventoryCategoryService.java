package ru.it2g.kis.service;

import ru.it2g.kis.entity.dictionary.InventoryCategory;

import java.util.Collection;

/**
 * @author Pahahentikys on 04.04.2018
 * Сервис для сущности "категория продуктов" (InventoryCategory).
 */
public interface InventoryCategoryService {

    /**
     * Создание продукта.
     *
     * @param inventoryCategory - экземпляр сущности "категория продукта".
     * @return - созданная категория продуктов.
     */
    InventoryCategory create(InventoryCategory inventoryCategory);

    /**
     * Обновление информации о категории продукта.
     *
     * @param inventoryCategory - экземпляр сущности "категория продукта".
     * @return - категория продукта с обновлённой информацией.
     */
    InventoryCategory update(InventoryCategory inventoryCategory);

    /**
     * Получение всех (активных и неактивных) категорий продуктов.
     *
     * @return - коллекция всех категорий продуктов.
     */
    Collection<InventoryCategory> getAll();

    /**
     * Получение категории по идентификатору.
     *
     * @param id - идентификатор категории.
     * @return - категория с искомым идентификатором.
     */
    InventoryCategory getById(Long id);

    /**
     * Получение категорий по наименованию.
     *
     * @param categoryName - наименование категории.
     * @return - категория с искомым наименованием.
     */
    Collection<InventoryCategory> findByName(String categoryName);

    /**
     * Получение категорий по идентификатору вышестоящей категории.
     *
     * @param parentCategoryId - идентификатор вышестоящей категории.
     * @return - колекция категорий продуктов с искомым идентификатором вышестоящей категории.
     */
    Collection<InventoryCategory> findByParent(Long parentCategoryId);

    /**
     * Получение всех существующих категорий.
     *
     * @return - коллекция активных категорий.
     */
    Collection<InventoryCategory> getByActiveTrue();

    /**
     * Удаление категории по идентификатору.
     *
     * @param id - идентификатор категории.
     */
    void remove(Long id);

}
