package ru.it2g.kis.service;

import ru.it2g.kis.entity.InventoryState;

import java.util.Collection;

/**
 * Сервис бизнес логики для Статуса ТМЦ.
 *
 * @author Created by ZotovES on 12.03.2018
 */

public interface InventoryStateService {
    /**
     * Создает и сохраняет статус ТМЦ.
     *
     * @param name - Наименование статуса.
     * @return - возвращает сохраненый объект статуса ТМЦ.
     */
    InventoryState create(String name);

    /**
     * Сохраняет объект Статуса ТМЦ.
     *
     * @param inventoryState - объект статуса ТМЦ.
     * @return - сохраненый объект статуса ТМЦ.
     */
    InventoryState save(InventoryState inventoryState);

    /**
     * Поиск Статуса по идентификатору
     *
     * @param id - идентификатор статуса.
     * @return - объект статуса.
     */
    InventoryState getById(long id);

    /**
     * Возвращает список всех статусов.
     *
     * @return - коллекция статусов.
     */
    Collection<InventoryState> getAll();

    /**
     * Возвращает только активные статусы, поле active=true
     *
     * @return - коллекция статусов.
     */
    Collection<InventoryState> getByActiveTrue();

    /**
     * Удаляет статус по идентификатору.
     *
     * @param id - идентификатор статуса.
     */
    void remove(long id);
}
