package ru.it2g.kis.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.it2g.kis.entity.InventoryState;
import ru.it2g.kis.repository.InventoryStateRepository;

import java.util.Collection;

/**
 * Реализация сервиса бизнес логики для Статуса ТМЦ.
 *
 * @author Created by ZotovES on 12.03.2018
 */
@Service
public class InventoryStateServiceImpl implements InventoryStateService {

    /**
     * Конструктор сервиса
     *
     * @param inventoryStateRepository - репозиторий справочника состояний.
     */
    @Autowired
    public InventoryStateServiceImpl(InventoryStateRepository inventoryStateRepository) {
        this.inventoryStateRepository = inventoryStateRepository;
    }

    private InventoryStateRepository inventoryStateRepository;

    /**
     * Создает и сохраняет статус ТМЦ.
     *
     * @param name - Наименование статуса.
     * @return - возвращает сохраненый объект статуса ТМЦ.
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public InventoryState create(String name) {
        if (name == null || name.isEmpty())
            throw new IllegalArgumentException("Наименование статуса не может быть пустым");

        InventoryState inventoryState = InventoryState.builder()
                .name(name)
                .active(true)
                .build();
        return inventoryStateRepository.save(inventoryState);
    }

    /**
     * Сохраняет объект статуса ТМЦ
     *
     * @param inventoryState - объект статуса ТМЦ.
     * @return возврящает сохраненый объект статуса ТМЦ.
     */
    @Override
    public InventoryState save(InventoryState inventoryState) {
        if (inventoryState == null || inventoryState.getName().isEmpty())
            throw new IllegalArgumentException("Наименование статуса не может быть пустым");

        return inventoryStateRepository.save(inventoryState);
    }

    /**
     * Возвращает статус по идентификатору.
     *
     * @param id - идентификатор статуса.
     * @return Статус ТМЦ.
     */
    @Override
    public InventoryState getById(long id) {
        return inventoryStateRepository.findOne(id);
    }

    /**
     * Возврашает список статусов.
     *
     * @return список статусов.
     */
    @Override
    public Collection<InventoryState> getAll() {
        return inventoryStateRepository.findAll();
    }

    /**
     * Возвращает список активных (не удаленных) статусов.
     *
     * @return список статусов.
     */
    @Override
    public Collection<InventoryState> getByActiveTrue() {
        return inventoryStateRepository.findByActiveTrue();
    }

    /**
     * Удаляет статус по идентификатору.
     *
     * @param id - идентификатор статуса.
     */
    @Override
    public void remove(long id) {
        InventoryState inventoryState = inventoryStateRepository.findOne(id);
        if (inventoryState == null) return;
        inventoryState.setActive(false);
        inventoryStateRepository.save(inventoryState);

    }
}
