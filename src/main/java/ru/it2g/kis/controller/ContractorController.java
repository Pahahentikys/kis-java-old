package ru.it2g.kis.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.it2g.kis.converter.ContractorToDtoConverter;
import ru.it2g.kis.dto.ContractorDto;
import ru.it2g.kis.entity.dictionary.Contractor;
import ru.it2g.kis.repository.ContractorRepository;
import ru.it2g.kis.service.ContractorService;

import java.util.Collection;

import static org.springframework.web.bind.annotation.RequestMethod.*;

/**
 * REST контроллер, отдает DTO в формате JSON сущности класса @see Contractor
 *
 * @author Created by ZotovES on 27.11.2017
 */
@RestController
@RequestMapping(value = "/contractor", produces = {"application/json; charset=UTF-8"})
public class ContractorController {

    /**
     * Поле сервиса бизнес логики.
     */
    private final ContractorService contractorService;
    /**
     * Поле DTO конвертора.
     */
    private final ContractorToDtoConverter toDtoConverter;
    /**
     * Поле репозитория сущьности.
     */
    private final ContractorRepository contractorRepository;

    /**
     * Конструктор - создает объект контроллера с определенными параметрами.
     *
     * @param contractorService    - сервис бизнес логики.
     * @param toDtoConverter       - конвертер в DTO
     * @param contractorRepository - репозиторий сущьности.
     */
    @Autowired
    public ContractorController(ContractorService contractorService, ContractorToDtoConverter toDtoConverter, ContractorRepository contractorRepository) {
        this.contractorService = contractorService;
        this.toDtoConverter = toDtoConverter;
        this.contractorRepository = contractorRepository;
    }

    /**
     * Получение списка контрагентов не зависимо от статуса активности.
     *
     * @return возвращает response со списоком контрагентов в формате JSON.
     * @see Contractor
     * @see ContractorDto
     * @see ResponseEntity
     */
    @RequestMapping(value = "", method = GET)
    public ResponseEntity getContractors() {
        Collection<Contractor> contractors = contractorService.getAll();
        if (contractors.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        Collection<ContractorDto> contractorsDto = toDtoConverter.convertToCollection(contractors);
        return new ResponseEntity<>(contractorsDto, HttpStatus.OK);
    }

    /**
     * Получение списка активных контрагентов.
     *
     * @return возвращает response со списоком контрагентов в формате JSON.
     * @see Contractor
     * @see ContractorDto
     * @see ResponseEntity
     */
    @RequestMapping(value = "/working", method = GET)
    public ResponseEntity getWorkingContractors() {
        Collection<Contractor> contractors = contractorService.getByActiveTrue();
        if (contractors.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        Collection<ContractorDto> contractorsDto = toDtoConverter.convertToCollection(contractors);
        return new ResponseEntity<>(contractorsDto, HttpStatus.OK);
    }

    /**
     * Получение одного контрагентов по идентификатору не зависимо от статуса активности.
     *
     * @param id - дентификатор контрагента.
     * @return возвращает response с объектом контрагента в формате JSON.
     * @see Contractor
     * @see ContractorDto
     * @see ResponseEntity
     */
    @RequestMapping(value = "/{id}", method = GET)
    public ResponseEntity getContractorById(@PathVariable Long id) {
        Contractor contractor = contractorService.getById(id);
        if (contractor == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        ContractorDto contractorDto = toDtoConverter.convert(contractor);
        return new ResponseEntity<>(contractorDto, HttpStatus.OK);
    }

    /**
     * Удаление одного контрагента по идентификатору.
     *
     * @param id - дентификатор контрагента.
     * @return возвращает response со статусом.
     */
    @RequestMapping(value = "/{id}", method = DELETE)
    public ResponseEntity deleteContractorById(@PathVariable Long id) {
        if (!contractorRepository.exists(id)) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        contractorService.remove(id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    /**
     * Добавление одного контрагента.
     *
     * @param contractorDto - входящий JSON с данными нового контрагента.
     * @return возвращает response с созданным контрагентом в формате JSON.
     * @see Contractor
     * @see ContractorDto
     * @see ResponseEntity
     */
    @RequestMapping(value = "", method = POST)
    public ResponseEntity createContractor(@RequestBody ContractorDto contractorDto) {
        Contractor contractor = toDtoConverter.convert(contractorDto);
        contractor = contractorService.save(contractor);
        if (contractor == null)
            return new ResponseEntity( HttpStatus.BAD_REQUEST);

        ContractorDto contractorDtoResult = toDtoConverter.convert(contractor);

        return new ResponseEntity<>(contractorDtoResult, HttpStatus.CREATED);
    }

    /**
     * Редактирование одного контрагента по идентификатору.
     *
     * @param id            - идентификатор контрагента.
     * @param contractorDto - входящий JSON с данными контрагента.
     * @return возвращает response с изменненым контрагентом в формате JSON.
     * @see Contractor
     * @see ContractorDto
     * @see ResponseEntity
     */
    @RequestMapping(value = "/{id}", method = PUT)
    public ResponseEntity updateContractor(@PathVariable Long id, @RequestBody ContractorDto contractorDto) {
        if (id != contractorDto.getId())
            return new ResponseEntity(HttpStatus.CONFLICT);

        if (!contractorRepository.exists(id)) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        Contractor contractor = toDtoConverter.convert(contractorDto);
        ContractorDto contractorDtoResult = toDtoConverter.convert(contractorService.save(contractor));

        return ResponseEntity.ok(contractorDtoResult);
    }

}