package ru.it2g.kis.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.it2g.kis.entity.dictionary.Organization;
import ru.it2g.kis.repository.OrganizationRepository;

import java.util.Collection;

import static org.springframework.web.bind.annotation.RequestMethod.DELETE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

/**
 * Created by RyabushevSA on 27.10.2017
 */
@RestController
@RequestMapping(value = "/organization", produces = {"application/json; charset=UTF-8"})
public class OrganizationController {

    private final OrganizationRepository organizationRepository;

    @Autowired
    public OrganizationController(OrganizationRepository organizationRepository) {
        this.organizationRepository = organizationRepository;
    }

    @RequestMapping(value = "", method = GET)
    public ResponseEntity getOrganizationAll(){
        Collection<Organization> organizations = (Collection<Organization>) organizationRepository.findAll();

        return new ResponseEntity<>(organizations, HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = GET)
    public ResponseEntity getOrganizationById(@PathVariable Long id){
        Organization organization = organizationRepository.findOne(id);

        if (organization == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity(organization, HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = DELETE)
    public ResponseEntity deleteOrganizationById(@PathVariable Long id){
        if (!organizationRepository.exists(id)) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        organizationRepository.delete(id);

        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "", method= POST)
    public ResponseEntity createOrganization(@RequestBody Organization organization){
        Organization saved = organizationRepository.save(organization);

        return new ResponseEntity(saved, HttpStatus.OK);
    }
}
