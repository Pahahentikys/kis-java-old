package ru.it2g.kis.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.it2g.kis.converter.OrganizationProviderToDtoConverter;
import ru.it2g.kis.dto.OrganizationProviderDto;
import ru.it2g.kis.entity.dictionary.OrganizationProvider;
import ru.it2g.kis.service.OrganizationProviderService;

import java.util.Collection;

/**
 * @author Pahahentikys on 08.02.2018
 * REST-контроллер для сущности организация-поставшик(OrganizationProvider), который отдаёт DTO в виде JSON.
 */
@RestController
@RequestMapping(value = "/organization-providers", produces = {"application/json; charset=UTF-8"})
public class OrganizationProviderController {

    /**
     * Сервис сущности организация-поставщик.
     */
    private final OrganizationProviderService organizationProviderService;

    /**
     * Конвертор в DTO для сущности организация-поставщик.
     */
    private final OrganizationProviderToDtoConverter organizationProviderToDtoConverter;

    /**
     * Конструктор для контроллера.
     *
     * @param organizationProviderService        - сервис сущности организация-поставщик
     * @param organizationProviderToDtoConverter - конвертор в DTO для сущности организация-поставщик.
     */
    @Autowired
    OrganizationProviderController(OrganizationProviderService organizationProviderService,
                                   OrganizationProviderToDtoConverter organizationProviderToDtoConverter) {
        this.organizationProviderService = organizationProviderService;
        this.organizationProviderToDtoConverter = organizationProviderToDtoConverter;
    }

    /**
     * Получения списка организаций-поставщиков вне зависимости от статуса активности.
     *
     * @return - возвращает ответ в формате JSON со списком всех организаций-поставщиков.
     */
    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseEntity getOrganizationProvidersAll() {
        Collection<OrganizationProvider> organizationProviders = organizationProviderService.getAll();

        Collection<OrganizationProviderDto> organizationProviderDto = organizationProviderToDtoConverter
                .convertToCollection(organizationProviders);

        return new ResponseEntity<>(organizationProviderDto, HttpStatus.OK);
    }

    /**
     * Получение списка активных организаций-поставщиков.
     *
     * @return - возвращает ответ в формате JSON со списком всех активных организаций-поставщиков.
     */
    @RequestMapping(value = "/active", method = RequestMethod.GET)
    public ResponseEntity getActiveOrganizationProviders() {
        Collection<OrganizationProvider> organizationProviders = organizationProviderService.getByActiveTrue();

        if (organizationProviders.isEmpty())
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        Collection<OrganizationProviderDto> organizationProviderDto = organizationProviderToDtoConverter
                .convertToCollection(organizationProviders);

        return new ResponseEntity<>(organizationProviderDto, HttpStatus.OK);
    }

    /**
     * Получение организации-поставщика по идентификатору.
     *
     * @param id - идентификатор организации-поставщика.
     * @return - объект организации-поставщика в формате JSON.
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity getOrganizationProviderById(@PathVariable Long id) {
        OrganizationProvider organizationProvider = organizationProviderService.getById(id);

        if (organizationProvider == null)
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        OrganizationProviderDto organizationProviderDto = organizationProviderToDtoConverter.convert(organizationProvider);

        return ResponseEntity.ok(organizationProviderDto);
    }

    /**
     * Удаление организации-поставщика по идентификатору.
     *
     * @param id - идентификатор организации поставщика.
     * @return - возвращает ответ в виде HTTP-статуса.
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity deleteOrganizationProviderById(@PathVariable Long id) {
        OrganizationProvider organizationProvider = organizationProviderService.getById(id);

        if (organizationProvider == null)
            return new ResponseEntity(HttpStatus.NOT_FOUND);

        organizationProviderService.remove(id);

        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    /**
     * Создание новой организации-поставщика.
     *
     * @param organizationProviderDto - входящий JSON с данными новой организации-поставщика.
     * @return - возвращает созданную организацию-поставщика в формате JSON.
     */
    @RequestMapping(value = "", method = RequestMethod.POST)
    public ResponseEntity createOrganizationProvider(@RequestBody OrganizationProviderDto organizationProviderDto) {
        OrganizationProvider organizationProvider = organizationProviderToDtoConverter.convert(organizationProviderDto);
        OrganizationProvider organizationProviderSaved = organizationProviderService.create(organizationProvider);

        if (organizationProviderSaved == null)
            return new ResponseEntity(HttpStatus.BAD_REQUEST);

        OrganizationProviderDto organizationProviderDtoResult = organizationProviderToDtoConverter.convert(organizationProviderSaved);

        return ResponseEntity.ok(organizationProviderDtoResult);

    }

    /**
     * Обновление информации об организации-поставщике.
     *
     * @param id                      - идентификатор организации-поставщика.
     * @param organizationProviderDto - входящий JSON с данными об организации-поставщике.
     * @return - возвращает ответ в виде JSON с обновлёнными данными об организациях-поставщиках.
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity updateOrganizationProvider(@PathVariable Long id, @RequestBody OrganizationProviderDto organizationProviderDto) {

        if (!id.equals(organizationProviderDto.getId()))
            return new ResponseEntity(HttpStatus.CONFLICT);

        OrganizationProvider organizationProvider = organizationProviderToDtoConverter.convert(organizationProviderDto);
        OrganizationProviderDto savedOrganizationProviderDto = organizationProviderToDtoConverter.convert(organizationProviderService.update(organizationProvider));

        return ResponseEntity.ok(savedOrganizationProviderDto);
    }
}
