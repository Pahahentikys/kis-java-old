package ru.it2g.kis.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.it2g.kis.converter.ReceiptOrderToDtoConverter;
import ru.it2g.kis.entity.ReceiptOrder;
import ru.it2g.kis.service.ReceiptOrderService;

import static org.springframework.web.bind.annotation.RequestMethod.*;

@RestController
@RequestMapping(value = "/receipt-orders", produces = {"application/json; charset=UTF-8"})
public class ReceiptOrderController {

    private final ReceiptOrderService receiptOrderService;
    private final ReceiptOrderToDtoConverter toDtoConverter;

    @Autowired
    public ReceiptOrderController(ReceiptOrderService receiptOrderService, ReceiptOrderToDtoConverter toDtoConverter) {
        this.receiptOrderService = receiptOrderService;
        this.toDtoConverter = toDtoConverter;
    }

    /**
     * Получение всех ордеров с возможностью получения ордера по типу.
     *
     * @param typeId - идентификатор типа ордера, который может быть: новый, подписан, на подписи, в работе.
     * @return - коллекция ордеров с определённым типом, если параметр type задан,
     * если параметр type не задан, то коллекция всех ордеров.
     */
    @RequestMapping(value = "", method = GET)
    public ResponseEntity getReceiptOrders(@RequestParam(name = "type", required = false) Long typeId) {
        if (typeId == null) {
            return ResponseEntity.ok(receiptOrderService.getAll());
        }
        return ResponseEntity.ok(receiptOrderService.findByOrderTypeId(typeId));

    }

    @RequestMapping(value = "/{id}", method = GET)
    public ResponseEntity<?> getReceiptOrderById(@PathVariable Long id) {
        ReceiptOrder receiptOrder = receiptOrderService.getById(id);

        if (receiptOrder == null)
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        return ResponseEntity.ok(receiptOrder);
    }

    @RequestMapping(value = "/{id}", method = DELETE)
    public ResponseEntity removeReceiptOrder(@PathVariable Long id) {
        ReceiptOrder receiptOrder = receiptOrderService.getById(id);

        if (receiptOrder == null)
            return new ResponseEntity(HttpStatus.NOT_FOUND);

        receiptOrderService.remove(id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "", method = POST)
    public ResponseEntity createReceiptOrder(@RequestBody ReceiptOrder receiptOrder) {
        ReceiptOrder saved = receiptOrderService.create(receiptOrder);

        return new ResponseEntity(saved, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/{id}", method = PUT)
    public ResponseEntity updateReceiptOrder(@PathVariable Long id, @RequestBody ReceiptOrder receiptOrder) {
        if (id != receiptOrder.getId()) {
            return new ResponseEntity(HttpStatus.CONFLICT);
        }

        ReceiptOrder saved = receiptOrderService.save(receiptOrder);
        return ResponseEntity.ok(saved);
    }
}
