package ru.it2g.kis.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.it2g.kis.converter.InventoryTypeToDtoConverter;
import ru.it2g.kis.dto.InventoryTypeDto;
import ru.it2g.kis.entity.InventoryType;
import ru.it2g.kis.repository.InventoryTypeRepository;
import ru.it2g.kis.service.InventoryTypeService;

import java.util.Collection;
import java.util.Optional;

import static org.springframework.web.bind.annotation.RequestMethod.*;


/**
 * Контроллер для работы с Типом ТМЦ
 *
 * @author Created by ZotovES on 04.03.2018
 * @see ru.it2g.kis.entity.InventoryType
 */

@RestController
@RequestMapping(value = "/inventory-types", produces = {"application/json; charset=UTF-8"})
public class InventoryTypeController {
    /**
     * Сервис бизнес логики сущности Типа ТМЦ.
     */
    private final InventoryTypeService inventoryTypeService;

    /**
     * Репозиторий сущности Тип ТМЦ.
     */
    private final InventoryTypeRepository inventoryTypeRepository;

    /**
     * DTO конвертор для Типа ТМЦ.
     */
    private final InventoryTypeToDtoConverter inventoryTypeToDtoConverter;

    /**
     * Конструктор для контроллер Типа ТМЦ.
     *
     * @param inventoryTypeService        - сервис бизнес логики.
     * @param inventoryTypeRepository     - репозиторий сущьности Тип тмц.
     * @param inventoryTypeToDtoConverter - Конвертор DTO для сущьности Тип ТМЦ.
     * @see ru.it2g.kis.entity.InventoryType
     */
    @Autowired
    public InventoryTypeController(InventoryTypeService inventoryTypeService, InventoryTypeRepository inventoryTypeRepository,
                                   InventoryTypeToDtoConverter inventoryTypeToDtoConverter) {
        this.inventoryTypeService = inventoryTypeService;
        this.inventoryTypeRepository = inventoryTypeRepository;
        this.inventoryTypeToDtoConverter = inventoryTypeToDtoConverter;
    }

    /**
     * Получить список Типов ТМЦ.
     *
     * @return Список всех типов ТМЦ.
     */
    @RequestMapping(method = GET)
    public ResponseEntity getInventoryTypes() {
        Collection<InventoryType> inventoryTypes = inventoryTypeService.getAll();
        if (inventoryTypes.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        Collection<InventoryTypeDto> inventoryTypeDtos = inventoryTypeToDtoConverter.convertToCollection(inventoryTypes);
        return new ResponseEntity<>(inventoryTypeDtos, HttpStatus.OK);
    }

    /**
     * Получить список активных Типов ТМЦ.
     *
     * @return Список активных типов ТМЦ.
     */
    @RequestMapping(value = "/active", method = GET)
    public ResponseEntity getActiveInventoryTypes() {
        Collection<InventoryType> inventoryTypes = inventoryTypeService.getByActiveTrue();
        if (inventoryTypes.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        Collection<InventoryTypeDto> inventoryTypeDtos = inventoryTypeToDtoConverter.convertToCollection(inventoryTypes);
        return new ResponseEntity<>(inventoryTypeDtos, HttpStatus.OK);
    }

    /**
     * Получить Тип ТМЦ по идентивикатору.
     *
     * @param id - идентификатор Типа ТМЦ.
     * @return DTO типа ТМЦ.
     */
    @RequestMapping(value = "/{id}", method = GET)
    public ResponseEntity getInventoryTypesById(@PathVariable Long id) {
        return Optional.ofNullable(inventoryTypeService.getById(id))
                .map(inventoryTypeToDtoConverter::convert)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build()
                );
    }

    /**
     * Добавить новый Тип ТМЦ.
     *
     * @param inventoryTypeDto - входящий JSON DTO Типа ТМЦ.
     * @return Созданный тип ТМЦ.
     */
    @RequestMapping(method = POST)
    public ResponseEntity createInventoryTypes(@RequestBody InventoryTypeDto inventoryTypeDto) {
        InventoryType inventoryType = inventoryTypeToDtoConverter.convert(inventoryTypeDto);

        return Optional.ofNullable(inventoryTypeService.save(inventoryType))
                .map(inventoryTypeToDtoConverter::convert)
                .map(i -> new ResponseEntity<>(i, HttpStatus.CREATED))
                .orElse(ResponseEntity.badRequest().build());
    }

    /**
     * Редактировать  Тип ТМЦ по идентивикатору.
     *
     * @param id               - идентификатор Типа ТМЦ.
     * @param inventoryTypeDto - входящий JSON DTO Типа ТМЦ.
     * @return DTO типа ТМЦ.
     */
    @RequestMapping(value = "/{id}", method = PUT)
    public ResponseEntity updateInventoryTypes(@PathVariable Long id, @RequestBody InventoryTypeDto inventoryTypeDto) {
        if (id != inventoryTypeDto.getId())
            return new ResponseEntity(HttpStatus.CONFLICT);

        if (!inventoryTypeRepository.exists(id))
            return new ResponseEntity(HttpStatus.NOT_FOUND);

        InventoryType inventoryType = inventoryTypeToDtoConverter.convert(inventoryTypeDto);

        return Optional.ofNullable(inventoryTypeService.save(inventoryType))
                .map(inventoryTypeToDtoConverter::convert)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.badRequest().build());
    }

    /**
     * Удалить Тип ТМЦ по идентификатору.
     *
     * @param id - идентификатор Типа ТМЦ.
     * @return Статус подтверждающий удаление.
     */
    @RequestMapping(value = "/{id}", method = DELETE)
    public ResponseEntity deleteInventoryTypesById(@PathVariable Long id) {
        if (!inventoryTypeRepository.exists(id)) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        inventoryTypeService.remove(id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }


}
