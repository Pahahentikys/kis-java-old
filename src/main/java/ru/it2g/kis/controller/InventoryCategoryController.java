package ru.it2g.kis.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.it2g.kis.converter.InventoryCategoryToDTOConverter;
import ru.it2g.kis.dto.InventoryCategoryDTO;
import ru.it2g.kis.entity.dictionary.InventoryCategory;
import ru.it2g.kis.service.InventoryCategoryService;

import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author Pahahentikys on 09.04.2018
 * REST-контроллер для сущности "категория продуктов" (InventoryCatalog).
 */
@RestController
@RequestMapping(value = "/inventory-categories", produces = {"application/json; charset=UTF-8"})
public class InventoryCategoryController {

    /**
     * Сервис для сушности "категория продуктов".
     */
    private final InventoryCategoryService inventoryCategoryService;

    /**
     * DTO-конвертер для сушности "категория продуктов".
     */
    private final InventoryCategoryToDTOConverter inventoryCategoryToDTOConverter;

    /**
     * Конструктор для контроллера сущности "категория продуктов".
     *
     * @param inventoryCategoryService        - сервис для работы с категорией продукта.
     * @param inventoryCategoryToDTOConverter - DTO-конвертор для сущности "категория продуктов".
     */
    @Autowired
    InventoryCategoryController(InventoryCategoryService inventoryCategoryService,
                                InventoryCategoryToDTOConverter inventoryCategoryToDTOConverter) {

        this.inventoryCategoryService = inventoryCategoryService;
        this.inventoryCategoryToDTOConverter = inventoryCategoryToDTOConverter;
    }

    /**
     * Метод для поиска сущности "категория товаров" по паре критериев.
     *
     * @param parentId     - идентификатор вышестоящий категории товаров.
     * @param categoryName - наименование категории.
     * @return - список категорий, удовлетворяющих критериям поиска.
     */
    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public ResponseEntity<Collection<InventoryCategoryDTO>> getAllInventoryCategoriesByParams(
            @RequestParam(name = "parent_id", required = false) Long parentId,
            @RequestParam(name = "category_name", required = false) String categoryName) {

        if (parentId != null)
            return ResponseEntity.ok(inventoryCategoryService.findByParent(parentId)
                    .stream()
                    .map(inventoryCategoryToDTOConverter::convert)
                    .collect(Collectors.toList()));

        return ResponseEntity.ok(inventoryCategoryService.findByName(categoryName)
                .stream()
                .map(inventoryCategoryToDTOConverter::convert)
                .collect(Collectors.toList()));
    }

    /**
     * Получение всех (активных и неактивных) категорий.
     *
     * @return - JSON-ответ со списком всех категорий.
     */
    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseEntity<Collection<InventoryCategoryDTO>> getAllInventoryCategories() {

        return ResponseEntity.ok(inventoryCategoryService.getAll()
                .stream()
                .map(inventoryCategoryToDTOConverter::convert)
                .collect(Collectors.toList()));
    }

    /**
     * Получение всех активных категорий.
     *
     * @return - JSON ответ со списком активных категорий.
     */
    @RequestMapping(value = "/active", method = RequestMethod.GET)
    public ResponseEntity<Collection<InventoryCategoryDTO>> getAllActiveInventoryCategories() {

        return ResponseEntity.ok(inventoryCategoryService.getByActiveTrue()
                .stream()
                .map(inventoryCategoryToDTOConverter::convert)
                .collect(Collectors.toList()));
    }

    /**
     * Получение категории товаров по идентификатору.
     *
     * @param id - идентификатор категории товаров.
     * @return - категория товаров с искомым идентификатором.
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<InventoryCategoryDTO> getInventoryCategoryById(@PathVariable Long id) {

        return Optional.ofNullable(inventoryCategoryService.getById(id))
                .map(inventoryCategoryToDTOConverter::convert)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    /**
     * "Мягкое" удаление категории товаров по идентификатору.
     *
     * @param id - идентификатор категории.
     * @return - HTTP-статус.
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> deleteInventoryCategoryById(@PathVariable Long id) {

        InventoryCategory inventoryCategory = inventoryCategoryService.getById(id);

        if (inventoryCategory == null)
            return ResponseEntity.notFound().build();

        inventoryCategoryService.remove(id);

        return ResponseEntity.noContent().build();
    }

    /**
     * Создание новой категории товаров.
     *
     * @param inventoryCategoryDTO - входящий DTO сущности "категории товаров".
     * @return - созданная категория товаров.
     */
    @RequestMapping(value = "", method = RequestMethod.POST)
    public ResponseEntity<InventoryCategoryDTO> createInventoryCategory(@RequestBody InventoryCategoryDTO inventoryCategoryDTO) {

        InventoryCategory inventoryCategory = inventoryCategoryToDTOConverter.convert(inventoryCategoryDTO);

        return Optional.ofNullable(inventoryCategoryService.create(inventoryCategory))
                .map(inventoryCategoryToDTOConverter::convert)
                .map(ic -> new ResponseEntity<>(ic, HttpStatus.CREATED))
                .orElse(ResponseEntity.badRequest().build());
    }

    /**
     * Обновление информации о категории товаров.
     *
     * @param inventoryCategoryDTO - входящий DTO сущности "категория товаров".
     * @return - созданная категория товаров.
     */
    @RequestMapping(value = "", method = RequestMethod.PUT)
    public ResponseEntity<InventoryCategoryDTO> updateInventoryCategory(@RequestBody InventoryCategoryDTO inventoryCategoryDTO) {

        InventoryCategory product = inventoryCategoryToDTOConverter.convert(inventoryCategoryDTO);

        return Optional.ofNullable(inventoryCategoryService.update(product))
                .map(inventoryCategoryToDTOConverter::convert)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.badRequest().build());

    }

}
