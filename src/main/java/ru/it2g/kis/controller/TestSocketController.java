package ru.it2g.kis.controller;

import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.stereotype.Controller;

/**
 * Created by RyabushevSA on 06.10.2017
 */
@Controller
public class TestSocketController {

    @MessageMapping("/hello")
    @SendToUser("/queue/greetings")
    public String test(String message) throws Exception {
        System.err.println(message);
        return message + ": Test Test Test";
    }

}
