package ru.it2g.kis.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

/**
 * Created by RyabushevSA on 03.10.2017
 */
@RestController
@RequestMapping(value = "/test/push", produces = {"application/json; charset=UTF-8"})
public class TestController {

    @RequestMapping(value = "", method = POST)
    public ResponseEntity notify(@RequestBody String resource) {
        System.err.println(resource);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "", method = PUT)
    public ResponseEntity notifyPut(@RequestBody String resource) {
        System.err.println(resource);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
