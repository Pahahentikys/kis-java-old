package ru.it2g.kis.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.it2g.kis.converter.InventoryStateToDtoConverter;
import ru.it2g.kis.dto.InventoryStateDto;
import ru.it2g.kis.entity.InventoryState;
import ru.it2g.kis.repository.InventoryStateRepository;
import ru.it2g.kis.service.InventoryStateService;

import java.util.Collection;

import static org.springframework.web.bind.annotation.RequestMethod.*;

/**
 * REST контроллер для Статуса ТМЦ, отдает DTO в формате JSON сущности класса {@link ru.it2g.kis.entity.InventoryState}
 * Created by ZotovES on 19.03.2018
 */
@RestController
@RequestMapping(value = "/inventory-states", produces = "application/json; charset=UTF-8")
public class InventoryStateController {
    /**
     * Репозиторий сущьности Статуса ТМЦ.
     */
    private final InventoryStateRepository inventoryStateRepository;
    /**
     * DTO Конвертер
     */
    private final InventoryStateToDtoConverter inventoryStateToDtoConverter;
    /**
     * Сервис бизнес логики.
     */
    private final InventoryStateService inventoryStateService;

    /**
     * Конструктор контроллера.
     *
     * @param inventoryStateRepository     - репозиторий ствтуса ТМЦ.
     * @param inventoryStateToDtoConverter - Конвертер в DTO.
     * @param inventoryStateService        - сервис статуса ТМЦ.
     */
    @Autowired
    public InventoryStateController(InventoryStateRepository inventoryStateRepository,
                                    InventoryStateToDtoConverter inventoryStateToDtoConverter,
                                    InventoryStateService inventoryStateService) {
        this.inventoryStateRepository = inventoryStateRepository;
        this.inventoryStateToDtoConverter = inventoryStateToDtoConverter;
        this.inventoryStateService = inventoryStateService;
    }

    /**
     * Возвращает список всех статусов
     *
     * @return response со списком статусов в формате json
     */
    @RequestMapping(method = GET)
    public ResponseEntity getInventoryStates() {
        Collection<InventoryState> inventoryStates = inventoryStateService.getAll();
        if (inventoryStates.isEmpty()) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        Collection<InventoryStateDto> inventoryStateDtos = inventoryStateToDtoConverter.convertToCollection(inventoryStates);
        return ResponseEntity.ok(inventoryStateDtos);
    }

    /**
     * Возвращает список активных статусов.
     *
     * @return response со списком активных статусов в формате json
     */
    @RequestMapping(value = "/active", method = GET)
    public ResponseEntity getActiveInventoryStates() {
        Collection<InventoryState> inventoryStates = inventoryStateService.getByActiveTrue();
        if (inventoryStates.isEmpty()) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        Collection<InventoryStateDto> inventoryStateDtos = inventoryStateToDtoConverter.convertToCollection(inventoryStates);
        return ResponseEntity.ok(inventoryStateDtos);
    }

    /**
     * Возвращает статус ТМЦ по идентификатору.
     *
     * @param id - идентификатор статуса.
     * @return response со статусом ТМЦ.
     */
    @RequestMapping(value = "/{id}", method = GET)
    public ResponseEntity getInventoryStatesById(@PathVariable Long id) {
        InventoryState inventoryState = inventoryStateService.getById(id);
        if (inventoryState == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        InventoryStateDto inventoryStateDto = inventoryStateToDtoConverter.convert(inventoryState);
        return ResponseEntity.ok(inventoryStateDto);
    }

    /**
     * Добавление статуса ТМЦ.
     *
     * @param inventoryStateDto - входящий JSON c данными нового статуса.
     * @return сохраненые статус ТМЦ.
     */
    @RequestMapping(method = POST)
    public ResponseEntity createInventoryState(@RequestBody InventoryStateDto inventoryStateDto) {
        InventoryState inventoryState = inventoryStateToDtoConverter.convert(inventoryStateDto);
        InventoryState inventoryStateSaved = inventoryStateService.create(inventoryState.getName());
        if (inventoryStateSaved == null) {
            return ResponseEntity.badRequest().build();
        }

        InventoryStateDto inventoryStateDtoResult = inventoryStateToDtoConverter.convert(inventoryStateSaved);
        return new ResponseEntity<>(inventoryStateDtoResult, HttpStatus.CREATED);
    }

    /**
     * Редактирование Статуса ТМЦ.
     *
     * @param id                - идентификатор статуса.
     * @param inventoryStateDto - входящий DTO в JSON формате.
     * @return отредактированный статус.
     */
    @RequestMapping(value = "/{id}", method = PUT)
    public ResponseEntity updateInventoryState(@PathVariable Long id, @RequestBody InventoryStateDto inventoryStateDto) {
        if (id != inventoryStateDto.getId()) {
            return new ResponseEntity(HttpStatus.CONFLICT);
        }

        if (!inventoryStateRepository.exists(id)) {
            return ResponseEntity.notFound().build();
        }

        InventoryState inventoryState = inventoryStateToDtoConverter.convert(inventoryStateDto);
        InventoryStateDto inventoryStateDtoResult = inventoryStateToDtoConverter.convert(
                inventoryStateService.save(inventoryState));

        return ResponseEntity.ok(inventoryStateDtoResult);
    }

    /**
     * Удаляет статус по идентификатору.
     *
     * @param id - идентификатор статуса.
     * @return Возвращает response со тсатусом
     */
    @RequestMapping(value = "/{id}", method = DELETE)
    public ResponseEntity deleteInventoryState(@PathVariable Long id) {
        if (!inventoryStateRepository.exists(id)) {
            return ResponseEntity.notFound().build();
        }
        inventoryStateService.remove(id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
