package ru.it2g.kis.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.it2g.kis.converter.InventoryGroupToDtoConverter;
import ru.it2g.kis.dto.InventoryGroupDto;
import ru.it2g.kis.entity.InventoryGroup;
import ru.it2g.kis.repository.InventoryGroupRepository;
import ru.it2g.kis.service.InventoryGroupService;

import java.util.Collection;

import static org.springframework.web.bind.annotation.RequestMethod.*;

/**
 * REST контроллер, отдает DTO в формате JSON сущности класса @see InventoryGroup
 *
 * @author Created by ZotovES on 04.02.2018
 */
@RestController
@RequestMapping(value = "/inventory-groups", produces = {"application/json; charset=UTF-8"})
public class InventoryGroupController {
    /**
     * Поле репозитория сущьности.
     */
    private final InventoryGroupRepository inventoryGroupRepository;
    /**
     * Поле сервиса бизнес логики.
     */
    private final InventoryGroupService inventoryGroupService;
    /**
     * Поле DTO конвертора.
     */
    private final InventoryGroupToDtoConverter inventoryGroupToDtoConverter;

    /**
     * Конструктор - создает объект контроллера с определенными параметрами.
     *
     * @param inventoryGroupService        - сервис бизнес логики.
     * @param inventoryGroupToDtoConverter - конвертер в DTO
     * @param inventoryGroupRepository     - репозиторий сущьности.
     */
    @Autowired
    public InventoryGroupController(InventoryGroupRepository inventoryGroupRepository,
                                    InventoryGroupService inventoryGroupService,
                                    InventoryGroupToDtoConverter inventoryGroupToDtoConverter) {
        this.inventoryGroupRepository = inventoryGroupRepository;
        this.inventoryGroupService = inventoryGroupService;
        this.inventoryGroupToDtoConverter = inventoryGroupToDtoConverter;
    }

    /**
     * Получение списка групп ТМЦ вне зависимости от статуса активности.
     *
     * @return возвращает ResponseEntity с сериализованным в списком JSON обектов InventoryGroup.
     * @see InventoryGroup
     * @see InventoryGroupDto
     */
    @RequestMapping(value = "", method = GET)
    public ResponseEntity getInventoryGroups() {
        Collection<InventoryGroup> inventoryGroups = inventoryGroupService.getAll();

        if (inventoryGroups.isEmpty()) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        Collection<InventoryGroupDto> inventoryGroupsDto = inventoryGroupToDtoConverter.convertToCollection(inventoryGroups);
        return ResponseEntity.ok(inventoryGroupsDto);
    }

    /**
     * Получение списка только активных групп.
     *
     * @return возвращает ResponseEntity с сериализованным в списком JSON обектов InventoryGroup.
     * @see InventoryGroup
     * @see InventoryGroupDto
     */
    @RequestMapping(value = "/active", method = GET)
    public ResponseEntity getActiveInventoryGroups() {
        Collection<InventoryGroup> inventoryGroups = inventoryGroupService.getByActive();

        if (inventoryGroups.isEmpty()) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        Collection<InventoryGroupDto> inventoryGroupsDto = inventoryGroupToDtoConverter.convertToCollection(inventoryGroups);
        return ResponseEntity.ok(inventoryGroupsDto);
    }

    /**
     * Получение группы ТМЦ по идентификатору.
     *
     * @param id - идентификатор группы.
     * @return возвращает ResponseEntity с сериализованным в JSON обектом InventoryGroup.
     * @see InventoryGroup
     * @see InventoryGroupDto
     */

    @RequestMapping(value = "/{id}", method = GET)
    public ResponseEntity getByIdInventoryGroup(@PathVariable Long id) {
        InventoryGroup inventoryGroup = inventoryGroupService.getById(id);

        if (inventoryGroup == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        InventoryGroupDto inventoryGroupDto = inventoryGroupToDtoConverter.convert(inventoryGroup);

        return ResponseEntity.ok(inventoryGroupDto);
    }

    /**
     * Добавляет новую группу ТМЦ.
     *
     * @param inventoryGroupDto - входящий сериализованный в JSON объект.
     * @return - Возвращает сериализованный в JSON созданный объект или статус неудачи.
     * @see InventoryGroup
     * @see InventoryGroupDto
     */

    @RequestMapping(value = "", method = POST)
    public ResponseEntity createInventoryGroup(@RequestBody InventoryGroupDto inventoryGroupDto) {
        InventoryGroup inventoryGroup = inventoryGroupToDtoConverter.convert(inventoryGroupDto);
        InventoryGroup inventoryGroupSaved = inventoryGroupService.save(inventoryGroup);

        if (inventoryGroupSaved == null) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }

        InventoryGroupDto inventoryGroupDtoResult = inventoryGroupToDtoConverter.convert(inventoryGroupSaved);

        return new ResponseEntity<>(inventoryGroupDtoResult, HttpStatus.CREATED);
    }

    /**
     * Добавляет новую группу ТМЦ.
     *
     * @param id                - идентификатор группы ТМЦ.
     * @param inventoryGroupDto - входящий сериализованный в JSON объект.
     * @return - Возвращает сериализованный в JSON созданный объект или статус неудачи.
     * @see InventoryGroup
     * @see InventoryGroupDto
     */

    @RequestMapping(value = "/{id}", method = PUT)
    public ResponseEntity updateInventoryGroup(@PathVariable Long id, @RequestBody InventoryGroupDto inventoryGroupDto) {
        if (id != inventoryGroupDto.getId()) {
            return new ResponseEntity(HttpStatus.CONFLICT);
        }

        if (!inventoryGroupRepository.exists(id)) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        InventoryGroup inventoryGroup = inventoryGroupToDtoConverter.convert(inventoryGroupDto);
        InventoryGroupDto inventoryGroupDtoResult = inventoryGroupToDtoConverter.convert(
                inventoryGroupService.save(inventoryGroup));

        if (inventoryGroupDtoResult == null) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }

        return ResponseEntity.ok(inventoryGroupDtoResult);
    }

    /**
     * Удаляет группу ТМЦ по идентификатору.
     *
     * @param id идентификатор группы ТМЦ.
     * @return возвращает ResponseEntity со статусом операции.
     */

    @RequestMapping(value = "/{id}", method = DELETE)
    public ResponseEntity deleteByIdInventoryGroup(@PathVariable Long id) {
        if (!inventoryGroupRepository.exists(id)) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        inventoryGroupService.remove(id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
