package ru.it2g.kis.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.it2g.kis.entity.dictionary.OrganizationProvider;

import java.util.Collection;

/**
 * @author Pahahentikys on 08.02.2018
 * Репозиторий для организации-поставщика.
 */
@Repository
public interface OrganizationProviderRepository extends CrudRepository<OrganizationProvider, Long> {

    /**
     * Поиск всех активных организаций-поставщиков.
     * @return - коллекция активных организаций-поставщиков.
     */
    Collection<OrganizationProvider> findByActiveTrue();

    /**
     * Поиск всех (активных/неактивных) организаций-поставщиков.
     * @return - коллекциия всех организаций-поставщиков.
     */
    Collection<OrganizationProvider> findAll();

}
