package ru.it2g.kis.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.it2g.kis.entity.dictionary.InventoryCategory;

import java.util.Collection;

/**
 * @author Pahahentikys on 04.04.2018
 * Репозиторий для сущности "категория продуктов" (InventoryCatalog).
 */
@Repository
public interface InventoryCategoryRepository extends CrudRepository<InventoryCategory, Long> {

    /**
     * Поиск всех (активных и неактивных) категорий продуктов.
     *
     * @return - коллекция всех категорий продутов.
     */
    Collection<InventoryCategory> findAll();

    /**
     * Поиск активных категорий продуктов.
     *
     * @return - активные категории продуктов.
     */
    Collection<InventoryCategory> findByActiveTrue();

    /**
     * Поиск категорий по имени.
     *
     * @param categoryName - наименование категории.
     * @return - категория с искомым именем.
     */
    Collection<InventoryCategory> findByNameStartingWithIgnoreCaseAndActiveTrue(String categoryName);

    /**
     * Поиск категории по идентификатору вышестоящей категории.
     *
     * @param parentCategoryId - идентификатор вышестоящей категории.
     * @return - категория с искомым идентификатором вышестоящей категории.
     */
    Collection<InventoryCategory> findByParent_IdAndActiveTrue(Long parentCategoryId);

}
