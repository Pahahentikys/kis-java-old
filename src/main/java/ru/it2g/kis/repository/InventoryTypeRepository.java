package ru.it2g.kis.repository;

import org.springframework.data.repository.CrudRepository;
import ru.it2g.kis.entity.InventoryType;

import java.util.Collection;

/**
 * Репозиторий для класса Тип ТМЦ @see InventoryType
 * Базовые CRUD операции над данными.
 *
 * @author Created by ZotovES on 25.02.2018
 */
public interface InventoryTypeRepository extends CrudRepository<InventoryType, Long> {

    /**
     * Возвращает коллекцию активных Типов ТМЦ.
     *
     * @return список ативных типов ТМЦ.
     * @see InventoryType
     */
    Collection<InventoryType> findByActiveTrue();

    @Override
    Collection<InventoryType> findAll();
}
