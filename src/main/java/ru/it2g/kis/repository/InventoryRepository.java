package ru.it2g.kis.repository;

import org.springframework.data.repository.CrudRepository;
import ru.it2g.kis.entity.Inventory;

/**
 * Created by RyabushevSA on 29.09.2017
 */
public interface InventoryRepository extends CrudRepository<Inventory, Long> {

    Inventory getInventoriesByInventoryNumber(Long number);

}
