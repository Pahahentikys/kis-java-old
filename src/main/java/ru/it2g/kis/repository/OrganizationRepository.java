package ru.it2g.kis.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import ru.it2g.kis.entity.dictionary.Organization;

import java.util.Collection;

/**
 * Created by RyabushevSA on 27.10.2017
 */
public interface OrganizationRepository extends CrudRepository<Organization, Long> {

//    @Query("select o from Organization where name like :param")
//    Collection<Organization> testMethod(String param);
//
//    @Query(value = "select * from s_organization o where o.name like :param", nativeQuery = true)
//    Collection<Organization> testMethod2(String param);
//
//    Collection<ProjectionQwe> testMethod3(String param);
//
//    interface ProjectionQwe {
//        Long getId();
//        String getName();
//    }
}
