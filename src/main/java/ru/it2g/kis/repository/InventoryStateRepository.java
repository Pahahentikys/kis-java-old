package ru.it2g.kis.repository;

import org.springframework.data.repository.CrudRepository;
import ru.it2g.kis.entity.InventoryState;

import java.util.Collection;

/**
 * Репозиторий для сущности Статус ТМЦ.
 *
 * @author Created by ZotovES on 10.03.2018
 * @see InventoryState
 */

public interface InventoryStateRepository extends CrudRepository<InventoryState, Long> {
    /**
     * Получение коллекции статусов.
     *
     * @return коллекцию статусов.
     * @see InventoryState
     */
    Collection<InventoryState> findByActiveTrue();

    /**
     * Получение все статусы ТМЦ вне зависимости от признака активности.
     *
     * @return коллекцию статусов.
     * @see InventoryState
     */
    @Override
    Collection<InventoryState> findAll();

}
