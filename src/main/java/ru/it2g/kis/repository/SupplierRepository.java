package ru.it2g.kis.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.it2g.kis.entity.dictionary.Supplier;

/**
 * Created by RyabushevSA on 16.11.2017
 */
@Repository
public interface SupplierRepository extends CrudRepository<Supplier, Long>  {

}
