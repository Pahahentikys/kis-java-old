package ru.it2g.kis.repository;

import org.springframework.data.repository.CrudRepository;
import ru.it2g.kis.entity.dictionary.Contractor;

import java.util.Collection;
import java.util.Date;

/**
 * Репозиторий для класса сущьностьсти Контрагент @see Contractor.
 * Базовые CRUD операции над данными.
 *
 * @author Created by ZotovES on 27.11.2017
 */
public interface ContractorRepository extends CrudRepository<Contractor, Long> {
    /**
     * Получение коллекции контрагентов, если их дата окончания позже переданного параметра или не заполнена.
     *
     * @param dateEnd - дата, до которой не выводить контрагентов.
     * @return коллекцию контрагентов.
     * @see Contractor
     */
    Collection<Contractor> findByDateEndAfterOrDateEndIsNull(Date dateEnd);

    /**
     * Получение коллекции активных контрагентов.
     *
     * @return коллекцию контрагентов.
     * @see Contractor
     */
    Collection<Contractor> findByActiveTrue();

    /**
     * Получение всех контрагентов вне зависимости от статуса активности.
     *
     * @return коллекцию контрагентов.
     * @see Contractor
     */
    @Override
    Collection<Contractor> findAll();
}
