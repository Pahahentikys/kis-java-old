package ru.it2g.kis.repository;

import org.springframework.data.repository.CrudRepository;
import ru.it2g.kis.entity.ReceiptOrder;
import ru.it2g.kis.entity.dictionary.Organization;

import java.util.Collection;

/**
 * Created by RyabushevSA on 29.09.2017
 */
public interface ReceiptOrderRepository extends CrudRepository<ReceiptOrder, Long> {

    Collection<ReceiptOrder> findByExpenseOrganization(Organization organization);

    Collection<ReceiptOrder> findAll();

    /**
     * Запрос на получение ордера по типу
     *
     * @param typeId - индентификатор типа ордера.
     * @return - ордер с определённым типом
     */
    Collection<ReceiptOrder> findByOrderTypeId(Long typeId);

}
