package ru.it2g.kis.repository;

import org.springframework.data.repository.CrudRepository;
import ru.it2g.kis.entity.InventoryGroup;

import java.util.Collection;

/**
 * @author Created by ZotovES on 02.02.2018
 */
public interface InventoryGroupRepository extends CrudRepository<InventoryGroup, Long> {
    /**
     * Получение коллекции активных групп товаров.
     *
     * @return коллекцию контрагентов.
     * @see InventoryGroup
     */
    Collection<InventoryGroup> findByActiveTrue();

    /**
     * Получение всех групп товаров вне зависимости от статуса активности.
     *
     * @return коллекцию групп товаров.
     * @see InventoryGroup
     */
    @Override
    Collection<InventoryGroup> findAll();
}
