package ru.it2g.kis.converter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static java.util.stream.Collectors.toCollection;
import static java.util.stream.Collectors.toList;

/**
 * Created by RyabushevSA on 05.07.2016.
 * Абстрактный класс конвертера между DTO и Entity
 */
public interface AbstractDtoConverter<A, B> {

    B convert(A fromObject);

    default List<B> convertToList(final List<A> input) {
        return input.stream().map(this::convert).collect(toList());
    }

    default Collection<B> convertToCollection(final Collection<A> input) {
        return input.stream().map(this::convert).collect(toCollection(ArrayList::new));
    }
}