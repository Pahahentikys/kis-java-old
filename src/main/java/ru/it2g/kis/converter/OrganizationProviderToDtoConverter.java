package ru.it2g.kis.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.it2g.kis.dto.OrganizationProviderDto;
import ru.it2g.kis.entity.dictionary.OrganizationProvider;

/**
 * @author Pahahentikys on 07.02.2018
 * Конвертер, который преобразует объекты сущности организации-поставщика в DTO.
 */
@Component
public class OrganizationProviderToDtoConverter implements AbstractDtoConverter<OrganizationProvider, OrganizationProviderDto> {

    /**
     * Конвертер организации в DTO.
     */
    private final OrganizationToDtoConverter organizationToDtoConverter;

    /**
     * Конструктор конвертора в DTO организации поставщика.
     * @param organizationToDtoConverter - конвертор организации в DTO.
     */
    @Autowired
    OrganizationProviderToDtoConverter(OrganizationToDtoConverter organizationToDtoConverter){
        this.organizationToDtoConverter = organizationToDtoConverter;
    }

    /**
     * Преобразует объект сущности организация-поставщик в DTO.
     *
     * @param organizationProvider - объект класса организации-поставщика.
     * @return - DTO организации-поставщика.
     */
    @Override
    public OrganizationProviderDto convert(OrganizationProvider organizationProvider) {
        return OrganizationProviderDto.builder()
                .id(organizationProvider.getId())
                .organization(organizationToDtoConverter.convert(organizationProvider.getOrganization()))
                .build();
    }

    /**
     * Преобразует DTO в объект класса OrganizationProvider.
     *
     * @param organizationProviderDto - входящий DTO-объект организации-поставщика.
     * @return - объект класса OrganizationProvider.
     */
    public OrganizationProvider convert(OrganizationProviderDto organizationProviderDto) {
        return OrganizationProvider.builder()
                .id(organizationProviderDto.getId())
                .organization(organizationToDtoConverter.convert(organizationProviderDto.getOrganization()))
                .build();
    }
}
