package ru.it2g.kis.converter;

import org.springframework.stereotype.Component;
import ru.it2g.kis.dto.ReceiptOrderDto;
import ru.it2g.kis.entity.ReceiptOrder;

@Component
public class ReceiptOrderToDtoConverter implements AbstractDtoConverter<ReceiptOrder, ReceiptOrderDto> {

    @Override
    public ReceiptOrderDto convert(ReceiptOrder entity) {
        return ReceiptOrderDto.builder()
                .id(entity.getId())
                .name(entity.getName())
                .dateCreate(entity.getDateCreate())
                .expenseDocumentDate(entity.getExpenseDocumentDate())
                .expenseDocumentNumber(entity.getExpenseDocumentNumber())
                .expenseOrganizationId(entity.getExpenseOrganization().getId())
                .build();
    }
}
