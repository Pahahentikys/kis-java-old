package ru.it2g.kis.converter;

import org.springframework.stereotype.Component;
import ru.it2g.kis.dto.InventoryCategoryDTO;
import ru.it2g.kis.entity.dictionary.InventoryCategory;

/**
 * @author Pahahentikys on 04.04.2018
 * Конвертер для преобразования сущности "категория продуктов" в DTO обратно в сущность.
 */
@Component
public class InventoryCategoryToDTOConverter implements AbstractDtoConverter<InventoryCategory, InventoryCategoryDTO> {

    /**
     * Преобразует сущность "категория продуктов" в DTO.
     *
     * @param inventoryCategory - категория продуктов.
     * @return - DTO сущности "категория продуктов".
     */
    @Override
    public InventoryCategoryDTO convert(InventoryCategory inventoryCategory) {

        return InventoryCategoryDTO.builder()
                .id(inventoryCategory.getId())
                .name(inventoryCategory.getName())
                .build();
    }

    /**
     * Преобразует DTO сущности "категория продуктов" в объект класса inventoryCategory.
     *
     * @param inventoryCategoryDTO - DTO сущности "категория продуктов".
     * @return - объект класса inventoryCategory.
     */
    public InventoryCategory convert(InventoryCategoryDTO inventoryCategoryDTO) {

        return InventoryCategory.builder()
                .id(inventoryCategoryDTO.getId())
                .name(inventoryCategoryDTO.getName())
                .build();
    }
}
