package ru.it2g.kis.converter;

import org.springframework.stereotype.Component;
import ru.it2g.kis.dto.OrganizationDto;
import ru.it2g.kis.entity.dictionary.Organization;

/**
 * @author Pahahentikys on 09.02.2018
 * Конвертер, который преобразует объекты сущности организации в DTO.
 */
@Component
public class OrganizationToDtoConverter implements AbstractDtoConverter<Organization, OrganizationDto> {

    /**
     * Преобразует объект сущности организация в DTO.
     * @param organization - - объект класса организации.
     * @return - DTO организации-поставщика.
     */
    @Override
    public OrganizationDto convert(Organization organization) {
        return OrganizationDto.builder()
                .id(organization.getId())
                .name(organization.getName())
                .dateBegin(organization.getDateBegin())
                .dateEnd(organization.getDateEnd())
                .build();
    }

    /**
     * Преобразует DTO в объект класса Organization.
     * @param organizationDto - входящий DTO-объект организации.
     * @return - объект класса Organization.
     */
    public Organization convert(OrganizationDto organizationDto) {
        return Organization.builder()
                .id(organizationDto.getId())
                .name(organizationDto.getName())
                .dateBegin(organizationDto.getDateBegin())
                .dateEnd(organizationDto.getDateEnd())
                .build();
    }
}
