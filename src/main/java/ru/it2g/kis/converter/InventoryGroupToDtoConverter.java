package ru.it2g.kis.converter;

import org.springframework.stereotype.Component;
import ru.it2g.kis.dto.InventoryGroupDto;
import ru.it2g.kis.entity.InventoryGroup;

/**
 * Конвертер преобразующий объекты сущьность в DTO
 * для объекта класса @see InventoryGroup в DTO объект класса @see InventoryGroupDto.
 * И обратно из DTO объект класса @see InventoryGroupDto в объект класса @see InventoryGroup.
 *
 * @author Created by ZotovES on 27.11.2017
 * @see InventoryGroup
 * @see InventoryGroupDto
 */
@Component
public class InventoryGroupToDtoConverter implements AbstractDtoConverter<InventoryGroup, InventoryGroupDto> {
    /**
     * Ковертирует объект сущьности в DTO.
     *
     * @param fromObject - объект класса Группа ТМЦ.
     * @return возвращает DTO
     */
    @Override
    public InventoryGroupDto convert(InventoryGroup fromObject) {
        return InventoryGroupDto.builder()
                .id(fromObject.getId())
                .name(fromObject.getName())
                .build();
    }

    /**
     * Ковертирует DTO в объект класса Группа ТМЦ.
     *
     * @param fromObject - DTO Группа ТМЦ.
     * @return возвращает объект класса Группа ТМЦ
     */
    public InventoryGroup convert(InventoryGroupDto fromObject) {
        return InventoryGroup.builder()
                .id(fromObject.getId())
                .name(fromObject.getName())
                .build();
    }

}
