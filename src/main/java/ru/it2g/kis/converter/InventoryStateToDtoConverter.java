package ru.it2g.kis.converter;

import org.springframework.stereotype.Component;
import ru.it2g.kis.dto.InventoryStateDto;
import ru.it2g.kis.entity.InventoryState;

/**
 * Конвертер преобразующий объекты сущьность в DTO
 * для объекта класса {@link ru.it2g.kis.entity.InventoryState} в DTO объект класса {@link ru.it2g.kis.dto.InventoryStateDto}.
 * И обратно из DTO объект класса {@link ru.it2g.kis.dto.InventoryStateDto} в объект класса {@link ru.it2g.kis.entity.InventoryState}.
 *
 * @author Created by ZotovES on 19.03.2018
 */
@Component
public class InventoryStateToDtoConverter implements AbstractDtoConverter<InventoryState, InventoryStateDto> {
    /**
     * Ковертирует объект сущьности в DTO.
     *
     * @param fromObject - объект класса Статус ТМЦ.
     * @return возвращает DTO.
     */
    @Override
    public InventoryStateDto convert(InventoryState fromObject) {
        return InventoryStateDto.builder()
                .id(fromObject.getId())
                .name(fromObject.getName())
                .build();
    }

    /**
     * Преобразует DTO в объект класса Статус ТМЦ.
     *
     * @param fromObject - входящий DTO.
     * @return объект класса Contractor.
     */
    public InventoryState convert(InventoryStateDto fromObject) {
        return InventoryState.builder()
                .id(fromObject.getId())
                .name(fromObject.getName())
                .active(true)
                .build();
    }
}
