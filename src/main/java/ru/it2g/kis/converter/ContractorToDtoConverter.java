package ru.it2g.kis.converter;

import org.springframework.stereotype.Component;
import ru.it2g.kis.dto.ContractorDto;
import ru.it2g.kis.entity.dictionary.Contractor;

/**
 * Конвертер преобразующий объекты сущьность в DTO
 * для объекта класса @see Contractor в DTO объект класса @see ContractorDto.
 * И обратно из DTO объект класса @see ContractorDto в объект класса @see Contractor.
 *
 * @author Created by ZotovES on 27.11.2017
 * @see Contractor
 * @see ContractorDto
 */
@Component
public class ContractorToDtoConverter implements AbstractDtoConverter<Contractor, ContractorDto> {
    /**
     * Ковертирует объект сущьности в DTO.
     *
     * @param fromObject - объект класса Контрагент.
     * @return возвращает DTO
     */
    @Override
    public ContractorDto convert(Contractor fromObject) {
        return ContractorDto.builder()
                .id(fromObject.getId())
                .name(fromObject.getName())
                .build();
    }

    /**
     * Преобразует DTO в объект класса Contractor
     *
     * @param fromObject - входящий DTO.
     * @return объект класса Contractor.
     */
    public Contractor convert(ContractorDto fromObject) {
        return Contractor.builder()
                .id(fromObject.getId())
                .name(fromObject.getName())
                .build();
    }
}
