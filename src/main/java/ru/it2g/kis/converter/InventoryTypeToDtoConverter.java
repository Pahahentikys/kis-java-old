

package ru.it2g.kis.converter;

import org.springframework.stereotype.Component;
import ru.it2g.kis.dto.InventoryTypeDto;
import ru.it2g.kis.entity.InventoryType;

/**
 * Конвертер преобразующий объект сущьности в DTO
 * для объекта класса @see InventoryType в DTO объект класса @see InventoryTypeDto.
 * И обратно из DTO объект класса @see InventoryTypeDto в объект класса @see InventoryType.
 *
 * @author Created by ZotovES on 25.02.2018
 */
@Component
public class InventoryTypeToDtoConverter implements AbstractDtoConverter<InventoryType, InventoryTypeDto> {
    /**
     * Преобразует объект класса Типа ТМЦ в DTO.
     * @param fromObject объект сущьности @see InventoryType.
     * @return DTO объект.
     */
    @Override
    public InventoryTypeDto convert(InventoryType fromObject) {
        return InventoryTypeDto.builder()
                .id(fromObject.getId())
                .name(fromObject.getName())
                .build();
    }

    /**
     * Преобразует входящий DTO объект.
     * @param fromObject входящий DTO объект.
     * @return объект сущьности @see InventoryType.
     */
    public InventoryType convert(InventoryTypeDto fromObject) {
        return InventoryType.builder()
                .id(fromObject.getId())
                .name(fromObject.getName())
                .build();
    }
}
