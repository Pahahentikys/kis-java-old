package ru.it2g.kis.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author Pahahentikys on 09.02.2018
 * DTO для организации (Organization).
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OrganizationDto {

    /**
     * Идентификатор организации.
     */
    private long id;

    /**
     * Наименование организации.
     */
    private String name;

    /**
     * Дата начала действия организации.
     */
    private Date dateBegin;

    /**
     * Дата окончания действия организации.
     */
    private Date dateEnd;
}
