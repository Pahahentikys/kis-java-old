package ru.it2g.kis.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Pahahentikys on 07.02.2018
 * DTO для организации-поставщика (OrganizationProvider).
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OrganizationProviderDto {

    /**
     * Идентификатор организации-поставщика.
     */
    private Long id;

    /**
     * Идентификатор организации.
     */
    private OrganizationDto organization;

}
