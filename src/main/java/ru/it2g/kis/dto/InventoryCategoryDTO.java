package ru.it2g.kis.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Pahahentikys on 04.04.2018
 * DTO для сущности "категория продуктов" (InventoryCategory).
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class InventoryCategoryDTO {

    /**
     * Идентификатор категории продуктов.
     */
    private Long id;

    /**
     * Название категории продуктов.
     */
    private String name;

}
