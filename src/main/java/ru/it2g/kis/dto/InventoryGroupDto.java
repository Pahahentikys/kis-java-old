package ru.it2g.kis.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * DTO Класс для класса Группы ТМЦ @see InventoryGroup.
 *
 * @author Created by ZotovES on 04.02.2018
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class InventoryGroupDto {
    /**
     * Идентификатор группы.
     */
    private long id;
    /**
     * Наименование группы.
     */
    private String name;
}
