package ru.it2g.kis.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * Created by RyabushevSA on 29.09.2017
 * DTO приходного ордера
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ReceiptOrderDto {

    private long id;

    private String name;

    private Date dateCreate;

    private Date expenseDocumentDate;

    private String expenseDocumentNumber;

    private Long expenseOrganizationId;
}
