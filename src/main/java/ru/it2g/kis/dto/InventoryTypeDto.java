
package ru.it2g.kis.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * DTO для класса Тип ТМЦ
 *
 * @author Created by ZotovES on 25.02.2018
 * @see ru.it2g.kis.entity.InventoryType
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class InventoryTypeDto {
    /**
     * Идентификатор типа ТМЦ.
     */
    private long id;
    /**
     * Наименование типа ТМЦ.
     */
    private String name;
}
