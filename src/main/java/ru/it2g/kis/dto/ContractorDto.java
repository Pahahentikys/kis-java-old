package ru.it2g.kis.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
/**
 * Класс для DTO класса сущьности.
 * @author Created by ZotovES on 27.11.2017
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ContractorDto {
    /**
     * Идентификатор контрагента.
     */
    private long id;
    /**
     * Наименование контрагента.
     */
    private String name;
}
