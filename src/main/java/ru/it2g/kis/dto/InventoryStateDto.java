package ru.it2g.kis.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * DTO для Статуса ТМЦ.
 *
 * @author Created by ZotovES on 19.03.2018
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class InventoryStateDto {
    /**
     * Идентивикатор статуса.
     */
    private long id;
    /**
     * Наименование статуса.
     */
    private String name;
}
