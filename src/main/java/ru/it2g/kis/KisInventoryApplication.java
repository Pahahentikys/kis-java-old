package ru.it2g.kis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KisInventoryApplication {

	public static void main(String[] args) {
		SpringApplication.run(KisInventoryApplication.class, args);
	}
}
