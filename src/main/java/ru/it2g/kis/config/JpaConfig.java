package ru.it2g.kis.config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import liquibase.integration.spring.SpringLiquibase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;

@Configuration
@EnableJpaRepositories("ru.it2g.kis.repository")
@EnableTransactionManagement
public class JpaConfig {
    private static final Logger LOGGER = LoggerFactory.getLogger(JpaConfig.class);

    @Value("${liquibase.contexts}")
    private String liquibaseContext;

    @Value("${liquibase.change-log}")
    private String liquibaseChangeLog;

    @Value("${liquibase.default-schema}")
    private String defaultSchema;

    @Value("${liquibase.drop-first:false}")
    private boolean dropFirst;

    @Value("${spring.datasource.url}")
    private String url;

    @Value("${spring.datasource.driverClassName}")
    private String driverClassName;

    @Value("${spring.datasource.username}")
    private String user;

    @Value("${spring.datasource.password}")
    private String password;

    @Value("${spring.datasource.hikari.maximumPoolSize:5}")
    private String maximumPoolSize;

    @Bean(name = "mainDataSource")
    public DataSource dataSource() {
        LOGGER.debug("Configuring datasource {} {} {}", driverClassName, url, user);
        HikariConfig config = new HikariConfig();
        config.setDriverClassName(driverClassName);
        config.setJdbcUrl(url);
        config.setUsername(user);
        config.setPassword(password);
        config.setMaximumPoolSize(Integer.parseInt(maximumPoolSize));
        config.setAutoCommit(false);
        return new HikariDataSource(config);
    }

    @Bean
    public SpringLiquibase liquibase(DataSource dataSource) {
        SpringLiquibase sl = new SpringLiquibase();
        sl.setDefaultSchema(defaultSchema);
        sl.setDataSource(dataSource);
        sl.setContexts(liquibaseContext);
        sl.setChangeLog(liquibaseChangeLog);
        sl.setDropFirst(dropFirst);
        sl.setShouldRun(true);
        return sl;
    }
}