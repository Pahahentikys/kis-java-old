package ru.it2g.kis.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * Created by ZotovES on 10.03.2018
 * Сущность справочника статуса ТМЦ.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor

@Entity
@Table(name = "inventory_state", schema = "inventory")
public class InventoryState {

    /**
     * Идентификатор статуса.
     */
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    /**
     * Наименование статуса.
     */
    @Column(name = "name")
    private String name;

    /**
     * Признак активности.
     */
    @Column(name = "active")
    private boolean active;
}
