package ru.it2g.kis.entity;

import lombok.*;
import ru.it2g.kis.entity.dictionary.OrderType;
import ru.it2g.kis.entity.dictionary.Organization;

import javax.persistence.*;
import java.util.Collection;
import java.util.Date;

/**
 * Created by RyabushevSA on 29.09.2017
 * Сущность приходного ордера
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor

@ToString(exclude = {"inventories"})
@EqualsAndHashCode(exclude = {"inventories"})

@Entity
@Table(name = "receipt_order", schema = "inventory")
public class ReceiptOrder {

    /**
     * Идентификатор. PK
     */
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    /**
     * Название. Нужно ли?
     */
    @Column(name = "name")
    private String name;

    /**
     * Дата создания
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "date_create")
    private Date dateCreate;

    /**
     * Список ТМЦ входящих в состав ордера
     */
    @OneToMany(mappedBy = "receiptOrder")
    private Collection<Inventory> inventories;

    /**
     * Дата расходника на основе которого составлен приходник
     */
    @Temporal(TemporalType.DATE)
    @Column(name = "expense_document_date")
    private Date expenseDocumentDate;

    /**
     * Номер расходника
     */
    @Column(name = "expense_document_number")
    private String expenseDocumentNumber;

    /**
     * Организация поставщик
     */
    @ManyToOne
    @JoinColumn(name = "expense_organization_id", referencedColumnName = "id", nullable = true)
    private Organization expenseOrganization;

    /**
     * Сущность-справочник с типом ордера.
     */
    @ManyToOne
    @JoinColumn(name = "order_type_id", referencedColumnName = "id", nullable = true)
    private OrderType orderType;

    /**
     * Признак активности ордера.
     */
    @Column(name = "active", nullable = true)
    private boolean active;

}
