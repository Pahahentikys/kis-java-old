package ru.it2g.kis.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * Created by ZotovES on 16.02.2018
 * Сущьность Тип ТМЦ.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor

@Entity
@Table(name = "inventory_type", schema = "inventory")
public class InventoryType {
    /**
     * Уникальный идентификатор.
     */
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    /**
     * Наименование типа ТМЦ.
     */
    @Column(name = "name")
    private String name;

    /**
     * Признак активности.
     */
    @Column(name = "active", nullable = false)
    private boolean active;

}
