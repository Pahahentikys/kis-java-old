package ru.it2g.kis.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * Сущьность группа товаров.
 *
 * @author Created by ZotovES on 02.02.2018
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor

@Entity
@Table(name = "inventory_group", schema = "inventory")
public class InventoryGroup {
    /**
     * Уникальный идентификатор группы товаров.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private long id;
    /**
     * Наименование группы.
     */
    @Column(name = "name", nullable = false)
    private String name;
    /**
     * Признак активности.
     */
    @Column(name = "active", nullable = false)
    private boolean active;
}
