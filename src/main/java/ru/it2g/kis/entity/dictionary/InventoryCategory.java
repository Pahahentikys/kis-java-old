package ru.it2g.kis.entity.dictionary;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * @author Pahahentikys on 03.04.2018
 * Сущность - справочник для категории товара.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor

@Entity
@Table(name = "s_inventory_category", schema = "inventory")
public class InventoryCategory {

    /**
     * Идентификатор категории товара.
     */
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * Навзание категории товара.
     */
    @Column(name = "name", nullable = false)
    private String name;

    /**
     * Признак активности категории товара.
     */
    @Column(name = "active", nullable = false)
    private boolean active;

    /**
     * Ссылка на вышестоящую категорию.
     */
    @ManyToOne
    @JoinColumn(name = "parent_id", referencedColumnName = "id", nullable = true)
    private InventoryCategory parent;
}
