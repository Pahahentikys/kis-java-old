package ru.it2g.kis.entity.dictionary;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GeneratorType;

import javax.persistence.*;
import java.util.Date;

/**
 * Сущьность справочника контрагентов
 *
 * @author Created by ZotovES on 27.11.2017
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor

@Entity
@Table(name = "s_contractor", schema = "inventory")
public class Contractor {
    /**
     * Идентификатор контрагента.
     */
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    /**
     * Наименование контрагента.
     */
    @Column(name = "name", nullable = false)
    private String name;
    /**
     * Дата начала действия.
     */
    @Temporal(TemporalType.DATE)
    @Column(name = "date_beg")
    private Date dateBegin;
    /**
     * Дата окончаний действия.
     */
    @Temporal(TemporalType.DATE)
    @Column(name = "date_end")
    private Date dateEnd;
    /**
     * Признак активности.
     */
    @Column(name = "active", nullable = false)
    private boolean active;
}
