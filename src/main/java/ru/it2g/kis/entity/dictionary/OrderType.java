package ru.it2g.kis.entity.dictionary;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * Сущность - справочник типов приходных ордеров.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor

@Entity
@Table(name = "s_order_type", schema = "inventory")
public class OrderType {

    /**
     * Идентификатор типа ордера.
     */
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * Наименование типа ордера: в работе, подписан, на подписи, новый.
     */
    @Column(name = "type", nullable = false)
    private String type;
}
