package ru.it2g.kis.entity.dictionary;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * @author Pahahentikys on 06.02.2018
 * Сущность организации-поставщика.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor

@Entity
@Table(name = "s_organization_provider", schema = "inventory")
public class OrganizationProvider {

    /**
     * Идентификатор организации-поставщика.
     */
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * Идентификатор организации.
     */
    @OneToOne
    @JoinColumn(name = "organization_id", referencedColumnName = "id", nullable = true)
    private Organization organization;

    /**
     * Поле активности организации-поставщика.
     */
    @Column(name = "active", nullable = false)
    private boolean active = true;

}
