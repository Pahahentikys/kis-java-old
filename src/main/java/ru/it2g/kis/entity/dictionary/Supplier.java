package ru.it2g.kis.entity.dictionary;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * Created by RyabushevSA on 16.11.2017
 * Сущность Организация-поставщик
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor

@Entity
@Table(name="s_supplier", schema = "inventory")
public class Supplier {

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne
    @JoinColumn(name = "contractor_id", referencedColumnName = "id", nullable = false)
    private Contractor contractor;

    @Column(name = "active", nullable = false)
    private Boolean active;
}
