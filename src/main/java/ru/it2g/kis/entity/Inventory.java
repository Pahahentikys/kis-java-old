package ru.it2g.kis.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Collection;

/**
 * Created by RyabushevSA on 29.09.2017
 * Сущность инвентаризации
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor

@Entity
@Table(name = "inventory", schema = "inventory")
public class Inventory {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "name")
    private String name;

    @Column(name = "inventory_number")
    private String inventoryNumber;

    @Transient
    private Collection<Inventory> inventories;

//    @Column(name = "group_id")
//    private InventoryGroup group;
//
//    @Column(name = "type_id")
//    private InventoryType type;
//
//    @Column(name = "state_id")
//    private InventoryState state;
//
//    @Column(name = "location_id")
//    private Location location;

    @ManyToOne
    @JoinColumn(name = "receipt_order_id", referencedColumnName = "id", nullable = false)
    private ReceiptOrder receiptOrder;
}
