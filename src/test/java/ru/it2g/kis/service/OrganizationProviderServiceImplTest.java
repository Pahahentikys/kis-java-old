package ru.it2g.kis.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import ru.it2g.kis.entity.dictionary.Organization;
import ru.it2g.kis.entity.dictionary.OrganizationProvider;
import ru.it2g.kis.repository.OrganizationProviderRepository;

import java.util.ArrayList;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

/**
 * @author Pahahentikys on 12.02.2018
 * <p>
 * Тест сервиса (OrganizationProviderService) организации-поставщика.
 */
@RunWith(JUnit4.class)
public class OrganizationProviderServiceImplTest {

    /**
     * Мок для репозитория организации-поставщика.
     */
    private OrganizationProviderRepository organizationProviderRepository = mock(OrganizationProviderRepository.class);

    /**
     * Коллекция организаций-поставщиков.
     */
    private ArrayList<OrganizationProvider> organizationProviders;

    /**
     * Инициализация сервиса организации-поставщика с передачей в него мока репозитория.
     */
    private OrganizationProviderService organizationProviderService = new OrganizationProviderServiceImpl(organizationProviderRepository);

    /**
     * Инициализация тестовых данных, описание Mock-условий.
     */
    @Before
    public void initailizeTestData() {

        // Создание тестовых данных
        Organization organizationOne = Organization.builder()
                .id(1L)
                .name("testOneOrganizationOne")
                .build();

        Organization organizationTwo = Organization.builder()
                .id(2L)
                .name("testOneOrganizationTwo")
                .build();

        OrganizationProvider organizationProviderOne = OrganizationProvider.builder()
                .id(1L)
                .organization(organizationOne)
                .active(true)
                .build();

        OrganizationProvider organizationProviderTwo = OrganizationProvider.builder()
                .id(2L)
                .organization(organizationTwo)
                .active(true)
                .build();

        OrganizationProvider organizationProviderThree = OrganizationProvider.builder()
                .id(3L)
                .organization(organizationTwo)
                .active(false)
                .build();

        // Коллекция организаций-поставщиков, наполянемая тестовыми данными.
        organizationProviders = new ArrayList<>();
        organizationProviders.add(organizationProviderOne);
        organizationProviders.add(organizationProviderTwo);
        organizationProviders.add(organizationProviderThree);

        // Mock на ситуацию поиска всех организаций-поставщиков.
        when(organizationProviderRepository.findAll()).thenReturn(organizationProviders);

        // Mock на ситуацию поиска организации-поставщика по идентификатору.
        when(organizationProviderRepository.findOne(organizationProviderOne.getId())).thenReturn(organizationProviderOne);

        // Mock на ситуацию поиска всех активных организаций-поставщиков.
        when(organizationProviderRepository.findByActiveTrue()).thenReturn(
                organizationProviders.stream()
                        .filter(OrganizationProvider::isActive)
                        .collect(Collectors.toList())
        );

    }

    /**
     * Тест на поиск всех организаций-поставщиков.
     *
     * @throws Exception - проброс исключений.
     */
    @Test
    public void getAllOrganizationProvider() throws Exception {
        assertThat(organizationProviderService.getAll())
                .isNotEmpty()
                .hasSize(3);

        verify(organizationProviderRepository).findAll();
    }

    /**
     * Тест на поиск организации-поставщика по идентификатору.
     *
     * @throws Exception - проброс исключений.
     */
    @Test
    public void getOrganizationProviderById() throws Exception {
        assertThat(organizationProviderService.getById(1L))
                .as("Not null returned").isNotNull();

        verify(organizationProviderRepository).findOne(1L);
    }

    /**
     * Тест на поиск активных организаций-поставщиков.
     *
     * @throws Exception - проброс исключений.
     */
    @Test
    public void getActiveOrganizationProviders() throws Exception {
        assertThat(organizationProviderService.getByActiveTrue())
                .isNotEmpty()
                .hasSize(2);

        verify(organizationProviderRepository).findByActiveTrue();
    }

    /**
     * Тест на "мягкое" удаление организации-поставщика по идентификатору.
     *
     * @throws Exception - проброс исключений.
     */
    @Test
    public void removeOrganizationProviderById() throws Exception {
        organizationProviderService.remove(1L);
        assertThat(organizationProviders.stream()
                .allMatch(OrganizationProvider::isActive))
                .isFalse();

        verify(organizationProviderRepository).save(any(OrganizationProvider.class));

    }

    /**
     * Обновление информации об организации-поставщике по идентификатору.
     *
     * @throws Exception - проброс исключений.
     */
    @Test
    public void updateOrganizationProviderById() throws Exception {

        Organization organizationTwo = Organization.builder()
                .id(2L)
                .name("testOneOrganizationTwo")
                .build();

        OrganizationProvider organizationProviderThreeUpdated = OrganizationProvider.builder()
                .id(3L)
                .organization(organizationTwo)
                .active(true)
                .build();

        when(organizationProviderRepository.findOne(organizationProviderThreeUpdated.getId())).thenReturn(organizationProviderThreeUpdated);
        when(organizationProviderRepository.save(any(OrganizationProvider.class))).thenReturn(organizationProviderThreeUpdated);

        organizationProviderService.update(organizationProviderThreeUpdated);

        assertThat(organizationProviders)
                .hasSize(3);

        assertThat(organizationProviders.stream()
                .filter(OrganizationProvider::isActive)
                .filter(o -> o.getId() == 3L)
                .collect(Collectors.toList()));
    }

    /**
     * Тест на создлание новой организации-поставщика.
     *
     * @throws Exception - проброс исключений.
     */
    @Test
    public void createNewOrganizationProvider() throws Exception {
        Organization organizationTwo = Organization.builder()
                .id(2L)
                .name("testOneOrganizationTwo")
                .build();

        OrganizationProvider organizationProviderCreated = OrganizationProvider.builder()
                .organization(organizationTwo)
                .active(true)
                .build();

        when(organizationProviderRepository.save(any(OrganizationProvider.class))).thenReturn(organizationProviderCreated);

        OrganizationProvider organizationProvider = organizationProviderService.create(organizationProviderCreated);

        assertThat(organizationProvider)
                .as("Not null").isNotNull()
                .as("4L").isEqualTo(organizationProviderCreated);

    }
}
