package ru.it2g.kis.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.springframework.test.context.ActiveProfiles;
import ru.it2g.kis.entity.InventoryState;
import ru.it2g.kis.repository.InventoryStateRepository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

/**
 * Тестирование сервиса бизнеса логики статуса ТМЦ.
 *
 * @author Created by ZotovES on 25.03.2018
 */
@ActiveProfiles(profiles = "dev")
@RunWith(JUnit4.class)
public class InventoryStateServiceImplTest {
    private InventoryStateRepository inventoryStateRepository = mock(InventoryStateRepository.class);
    private InventoryStateService inventoryStateService = new InventoryStateServiceImpl(inventoryStateRepository);
    private ArrayList<InventoryState> inventoryStates;

    /**
     * Инициализация данных для теста.
     */
    @Before
    public void setUp() {
        this.inventoryStates = new ArrayList<>();
        this.inventoryStates.add(InventoryState.builder().name("test state1").active(true).build());
        this.inventoryStates.add(InventoryState.builder().name("test state2").active(true).build());
        this.inventoryStates.add(InventoryState.builder().name("test state3").active(false).build());
    }

    /**
     * Тестирование метода создания статуса ТМЦ.
     */
    @Test
    public void createInventoryStateTest() {
        InventoryState inventoryState = inventoryStates.get(1);

        when(inventoryStateRepository.save(any(InventoryState.class))).thenReturn(inventoryState);

        InventoryState inventoryStateReturned = inventoryStateService.create(inventoryState.getName());

        assertThat(inventoryStateReturned).isNotNull();
        assertThat(inventoryStateReturned.getName()).isEqualTo(inventoryState.getName());

        verify(inventoryStateRepository).save(any(InventoryState.class));
    }

    /**
     * Тестирование метода создания статуса ТМЦ c пустым наименованием.
     */
    @Test
    public void createEmptyNameInventoryStateTest() {
        Throwable throwable = catchThrowable(() -> inventoryStateService.create(""));

        assertThat(throwable).isInstanceOf(IllegalArgumentException.class);
        assertThat(throwable.getMessage()).isEqualTo("Наименование статуса не может быть пустым");

        verify(inventoryStateRepository, never()).save(any(InventoryState.class));
    }

    /**
     * Тестирование метода редактирования статуса ТМЦ.
     */
    @Test
    public void updateInventoryStateTest() {
        InventoryState inventoryState = inventoryStates.get(1);

        when(inventoryStateRepository.findOne(inventoryState.getId())).thenReturn(inventoryState);
        when(inventoryStateRepository.save(any(InventoryState.class))).thenReturn(inventoryState);

        InventoryState inventoryStateReturned = inventoryStateService.save(inventoryState);

        assertThat(inventoryStateReturned).isNotNull();
        assertThat(inventoryStateReturned.getName()).isEqualTo(inventoryState.getName());

        verify(inventoryStateRepository).save(any(InventoryState.class));
    }

    /**
     * Тестирование поиска статуса по идентификатору.
     */
    @Test
    public void getByIdInventoryStateTest() {
        InventoryState inventoryState = inventoryStates.get(1);

        when(inventoryStateRepository.findOne(inventoryState.getId())).thenReturn(inventoryState);

        InventoryState inventoryStateReturned = inventoryStateService.getById(inventoryState.getId());

        assertThat(inventoryStateReturned).isNotNull();
        assertThat(inventoryStateReturned.getName()).isEqualTo(inventoryState.getName());

        verify(inventoryStateRepository).findOne(inventoryState.getId());
    }

    /**
     * Тестирование вывода всех статус.
     */
    @Test
    public void getAllInventoryStateTest() {
        when(inventoryStateRepository.findAll()).thenReturn(inventoryStates);

        Collection<InventoryState> inventoryStatesReturned = inventoryStateService.getAll();

        assertThat(inventoryStatesReturned).isNotNull();
        assertThat(inventoryStatesReturned).isNotEmpty();
        assertThat(inventoryStatesReturned).contains(inventoryStates.get(1));

        verify(inventoryStateRepository).findAll();
    }

    /**
     * Тестирование вывода только активных статусов.
     */
    @Test
    public void getByActiveTrueInventoryStateTest() {
        List<InventoryState> activeInventoryStates = inventoryStates.stream()
                .filter(InventoryState::isActive)
                .collect(Collectors.toList());

        when(inventoryStateRepository.findByActiveTrue()).thenReturn(activeInventoryStates);

        Collection<InventoryState> inventoryStatesReturned = inventoryStateService.getByActiveTrue();

        assertThat(inventoryStatesReturned).isNotNull();
        assertThat(inventoryStatesReturned).isNotEmpty();
        assertThat(inventoryStatesReturned.stream()
                .filter(i -> !i.isActive())
                .count())
                .isZero();

        verify(inventoryStateRepository).findByActiveTrue();

    }

    /**
     * Тестирование удаления статуса по идентификатору.
     */
    @Test
    public void removeInventoryStateTest() {
        InventoryState inventoryState = inventoryStates.get(1);

        when(inventoryStateRepository.findOne(inventoryState.getId())).thenReturn(inventoryState);

        inventoryStateService.remove(inventoryState.getId());

        verify(inventoryStateRepository).save(inventoryState);
    }
}