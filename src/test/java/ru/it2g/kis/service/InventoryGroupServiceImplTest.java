package ru.it2g.kis.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.springframework.test.context.ActiveProfiles;
import ru.it2g.kis.entity.InventoryGroup;
import ru.it2g.kis.repository.InventoryGroupRepository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

/**
 * Тестирование сервиса безнес логики Групп ТМЦ.
 *
 * @author Created by ZotovES on 11.02.2018
 */
@ActiveProfiles(profiles = "dev")
@RunWith(JUnit4.class)
public class InventoryGroupServiceImplTest {

    private InventoryGroupRepository inventoryGroupRepository = mock(InventoryGroupRepository.class);

    private InventoryGroupService inventoryGroupService = new InventoryGroupServiceImpl(inventoryGroupRepository);

    private ArrayList<InventoryGroup> inventoryGroups;

    /**
     * Инициализация данных для теста.
     *
     * @throws Exception Пробрасываем все исключения.
     */
    @Before
    public void setUp() throws Exception {
        this.inventoryGroups = new ArrayList<>();
        this.inventoryGroups.add(InventoryGroup.builder().name("test group1").active(true).build());
        this.inventoryGroups.add(InventoryGroup.builder().name("test group2").active(true).build());
        this.inventoryGroups.add(InventoryGroup.builder().name("test group3").active(false).build());
    }

    /**
     * Тестирование метода создания новой группы ТМЦ.
     *
     * @throws Exception Пробрасываем все исключения.
     */
    @Test
    public void createInventoryGroupTest() throws Exception {
        InventoryGroup inventoryGroup = inventoryGroups.get(1);

        when(inventoryGroupRepository.save(any(InventoryGroup.class))).thenReturn(inventoryGroup);

        InventoryGroup inventoryGroupReturned = inventoryGroupService.create(inventoryGroup.getName());

        assertThat(inventoryGroupReturned).isNotNull();
        assertThat(inventoryGroupReturned.getName()).isEqualTo(inventoryGroup.getName());

        verify(inventoryGroupRepository).save(any(InventoryGroup.class));

    }

    /**
     * Тестирование метода создания новой группы тмц с пустым названием.
     *
     * @throws Exception Пробрасываем все исключения.
     */
    @Test
    public void createEmptyNameInventoryGroupTest() throws Exception {
        Throwable throwable = catchThrowable(() -> inventoryGroupService.create(""));

        assertThat(throwable).isInstanceOf(IllegalArgumentException.class);
        assertThat(throwable.getMessage()).isEqualTo("Имя группы ТМЦ не может быть пустым");

        verify(inventoryGroupRepository, never()).save(any(InventoryGroup.class));
    }

    /**
     * Тестирование метода редактирования группы тмц.
     *
     * @throws Exception Пробрасываем все исключения.
     */
    @Test
    public void updateInventoryGroupTest() throws Exception {
        InventoryGroup inventoryGroup = inventoryGroups.get(1);

        when(inventoryGroupRepository.findOne(inventoryGroup.getId())).thenReturn(inventoryGroup);
        when(inventoryGroupRepository.save(any(InventoryGroup.class))).thenReturn(inventoryGroup);
        InventoryGroup inventoryGroupReturned = inventoryGroupService.save(inventoryGroup);

        assertThat(inventoryGroupReturned).isNotNull();
        assertThat(inventoryGroupReturned).isEqualTo(inventoryGroup);

        verify(inventoryGroupRepository).save(any(InventoryGroup.class));
    }

    /**
     * Тестирование поиска группы ТМЦ по идентификатору.
     *
     * @throws Exception Пробрасываем все исключения.
     */
    @Test
    public void getByIdInventoryGroupTest() throws Exception {
        InventoryGroup inventoryGroup = inventoryGroups.get(1);

        when(inventoryGroupRepository.findOne(inventoryGroup.getId())).thenReturn(inventoryGroup);

        InventoryGroup inventoryGroupReturned = inventoryGroupService.getById(inventoryGroup.getId());

        assertThat(inventoryGroupReturned).isNotNull();
        assertThat(inventoryGroupReturned).isEqualTo(inventoryGroup);

        verify(inventoryGroupRepository).findOne(inventoryGroup.getId());
    }

    /**
     * Тестирование поиска всех активных групп ТМЦ.
     *
     * @throws Exception Пробрасываем все исключения.
     */
    @Test
    public void getByActiveInventoryGroupTest() throws Exception {
        List<InventoryGroup> activeInventoryGroup = inventoryGroups.stream()
                .filter(InventoryGroup::isActive)
                .collect(Collectors.toList());

        when(inventoryGroupRepository.findByActiveTrue()).thenReturn(activeInventoryGroup);

        Collection<InventoryGroup> inventoryGroupsReturned = inventoryGroupService.getByActive();

        assertThat(inventoryGroupsReturned).isNotNull();
        assertThat(inventoryGroupsReturned).isNotEmpty();
        assertThat(inventoryGroupsReturned.stream()
                .filter(p -> !p.isActive())
                .count())
                .isZero();
        verify(inventoryGroupRepository).findByActiveTrue();
    }

    /**
     * Тестирование вывода всех групп ТМЦ.
     *
     * @throws Exception Пробрасываем все исключения.
     */
    @Test
    public void getAllInventoryGroupTest() throws Exception {
        when(inventoryGroupRepository.findAll()).thenReturn(inventoryGroups);

        Collection<InventoryGroup> inventoryGroupsReturned = inventoryGroupService.getAll();

        assertThat(inventoryGroupsReturned).isNotNull();
        assertThat(inventoryGroupsReturned).isNotEmpty();
        assertThat(inventoryGroupsReturned).contains(inventoryGroups.get(1));

        verify(inventoryGroupRepository).findAll();
    }

    /**
     * Тестирование удаление группы ТМЦ по идентификатору.
     *
     * @throws Exception Пробрасываем все исключения.
     */
    @Test
    public void removeInventoryGroupTest() throws Exception {
        InventoryGroup inventoryGroup = inventoryGroups.get(1);

        when(inventoryGroupRepository.findOne(inventoryGroup.getId())).thenReturn(inventoryGroup);

        inventoryGroupService.remove(inventoryGroup.getId());

        verify(inventoryGroupRepository).save(inventoryGroup);
    }

}