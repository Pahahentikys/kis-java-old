package ru.it2g.kis.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import ru.it2g.kis.entity.dictionary.InventoryCategory;
import ru.it2g.kis.repository.InventoryCategoryRepository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Collectors;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.mockito.Mockito.*;

/**
 * @author Pahahentikys on 10.04.2018
 * Тест имплементации сервиса для сущности "категория продуктов" (InventoryCategory).
 */
@RunWith(JUnit4.class)
public class InventoryCategoryServiceImplTest {

    /**
     * Мок для репозитория сущности InventoryCategory.
     */
    private InventoryCategoryRepository inventoryCategoryRepository = mock(InventoryCategoryRepository.class);

    /**
     * Инициализация сервиса сущности InventoryCategory с передачей в него мока репозитория.
     */
    private InventoryCategoryService inventoryCategoryService = new InventoryCategoryServiceImpl(inventoryCategoryRepository);

    /**
     * Коллекция тестовых категорий.
     */
    private Collection<InventoryCategory> inventoryCategories;

    /**
     * Тестовые категории.
     */
    private InventoryCategory inventoryCategoryOne;
    private InventoryCategory inventoryCategoryTwo;
    private InventoryCategory inventoryCategoryThree;
    private InventoryCategory inventoryCategoryFour;

    /**
     * Инициализация тестовых данных.
     */
    @Before
    public void initTestData() {

        inventoryCategoryOne = InventoryCategory.builder()
                .id(1L)
                .name("ParentCategoryOne")
                .active(true)
                .build();

        inventoryCategoryTwo = InventoryCategory.builder()
                .id(2L)
                .name("ParentCategoryTwo")
                .active(true)
                .build();

        inventoryCategoryThree = InventoryCategory.builder()
                .id(3L)
                .name("CategoryThree")
                .active(true)
                .parent(inventoryCategoryOne)
                .build();

        inventoryCategoryFour = InventoryCategory.builder()
                .id(4L)
                .name("CategoryFour")
                .active(false)
                .parent(inventoryCategoryOne)
                .build();

        inventoryCategories = new ArrayList<>();
        inventoryCategories.add(inventoryCategoryThree);
        inventoryCategories.add(inventoryCategoryOne);
        inventoryCategories.add(inventoryCategoryTwo);
        inventoryCategories.add(inventoryCategoryFour);

    }

    /**
     * Тест на получение всех (активных и неактивных) категорий продуктов.
     */
    @Test
    public void getAll() {

        int countTestInventoryCategories = 4;

        when(inventoryCategoryRepository.findAll()).thenReturn(inventoryCategories);

        assertThat(inventoryCategoryService.getAll())
                .hasSize(countTestInventoryCategories);

        verify(inventoryCategoryRepository).findAll();

    }

    /**
     * Тест на получение конкретной категории продуктов по идентификатору.
     */
    @Test
    public void getById() {

        long idForSearchInventoryCategory = 1L;

        when(inventoryCategoryRepository.findOne(idForSearchInventoryCategory)).thenReturn(inventoryCategoryOne);

        assertThat(inventoryCategoryService.getById(idForSearchInventoryCategory))
                .isNotNull();

        verify(inventoryCategoryRepository).findOne(idForSearchInventoryCategory);
    }

    /**
     * Тест на получение активных категорий продуктов.
     */
    @Test
    public void getAllByActive() {

        int countActiveInventoryCategories = 3;

        when(inventoryCategoryRepository.findByActiveTrue()).thenReturn(
                inventoryCategories.stream()
                        .filter(InventoryCategory::isActive)
                        .collect(Collectors.toList())
        );

        assertThat(inventoryCategoryService.getByActiveTrue())
                .hasSize(countActiveInventoryCategories);

        verify(inventoryCategoryRepository).findByActiveTrue();

    }

    /**
     * Тестирование "мягкого" удаления категории продуктов по идентификатору.
     */
    @Test
    public void removeById() {

        long idForRemoveInventoryCategory = 2L;

        when(inventoryCategoryRepository.findOne(idForRemoveInventoryCategory)).thenReturn(inventoryCategoryTwo);

        inventoryCategoryService.remove(idForRemoveInventoryCategory);
        assertThat(inventoryCategories.stream()
                .filter(ic -> ic.getId().equals(idForRemoveInventoryCategory))
                .filter(ic -> !ic.isActive()));

        verify(inventoryCategoryRepository).findOne(idForRemoveInventoryCategory);
        verify(inventoryCategoryRepository).save(any(InventoryCategory.class));

    }

    /**
     * Тестирование успешного создания новой категории продуктов.
     */
    @Test
    public void create() {

        InventoryCategory createdInvCat = InventoryCategory.builder()
                .name("CreatedCat")
                .active(true)
                .build();

        when(inventoryCategoryRepository.save(any(InventoryCategory.class))).thenReturn(createdInvCat);

        InventoryCategory inventoryCategory = inventoryCategoryService.create(createdInvCat);

        assertThat(inventoryCategory)
                .isEqualTo(createdInvCat);

        verify(inventoryCategoryRepository).save(any(InventoryCategory.class));
    }

    /**
     * Негативное тестирование создания новой категории продуктов.
     */
    @Test(expected = IllegalArgumentException.class)
    public void invalidCreate() {

        InventoryCategory inventoryCategory = InventoryCategory.builder()
                .id(10L)
                .name("CreatedCat")
                .build();

        assertThat(inventoryCategoryService.create(inventoryCategory))
                .isEqualTo(IllegalArgumentException.class);

    }

    /**
     * Тестирование обновления информации о категории продуктов.
     */
    @Test
    public void update() {

        long idForUpdate = 4L;

        int countInvCats = 4;

        String updatedName = "UpdatedCategoryName";

        InventoryCategory inventoryCategoryUpdated = InventoryCategory.builder()
                .id(4L)
                .name(updatedName)
                .build();

        when(inventoryCategoryRepository.findOne(idForUpdate)).thenReturn(inventoryCategoryUpdated);
        when(inventoryCategoryRepository.save(any(InventoryCategory.class))).thenReturn(inventoryCategoryUpdated);

        inventoryCategoryService.update(inventoryCategoryUpdated);

        assertThat(inventoryCategories)
                .hasSize(countInvCats);

        assertThat(inventoryCategories.stream()
                .filter(ic -> ic.getId().equals(idForUpdate))
                .filter(ic -> ic.getName().equals(updatedName)));

        verify(inventoryCategoryRepository).save(any(InventoryCategory.class));


    }

    /**
     * Негативное тестирование обновления информации о категории продуктов.
     */
    @Test(expected = IllegalArgumentException.class)
    public void invalidUpdate() {

        InventoryCategory inventoryCategory = InventoryCategory.builder()
                .id(1775L)
                .build();

        assertThat(inventoryCategoryService.update(inventoryCategory))
                .isEqualTo(IllegalArgumentException.class);

    }

    /**
     * Тестирование метода поиска по наименованию категории.
     */
    @Test
    public void searchByCategoryName() {

        int countSearchRes = 2;

        String searchParam = "par";

        when(inventoryCategoryRepository.findByNameStartingWithIgnoreCaseAndActiveTrue(searchParam))
                .thenReturn(Arrays.asList(inventoryCategoryOne, inventoryCategoryTwo));

        assertThat(inventoryCategoryService.findByName(searchParam))
                .hasSize(countSearchRes);
    }

    /**
     * Тестирование метода поиска по вышестоящей категории.
     */
    @Test
    public void searchByParent() {

        int countSearchRes = 2;

        when(inventoryCategoryRepository.findByParent_IdAndActiveTrue(1L))
                .thenReturn(Arrays.asList(inventoryCategoryThree, inventoryCategoryFour));

        assertThat(inventoryCategoryService.findByParent(1L))
                .hasSize(countSearchRes);


    }


}
