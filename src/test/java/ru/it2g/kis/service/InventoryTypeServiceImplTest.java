package ru.it2g.kis.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.springframework.test.context.ActiveProfiles;
import ru.it2g.kis.entity.InventoryType;
import ru.it2g.kis.repository.InventoryTypeRepository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

/**
 * Тестирование сервиса Тип ТМЦ.
 *
 * @author Created by ZotovES on 05.03.2018
 */

@ActiveProfiles(profiles = "dev")
@RunWith(JUnit4.class)
public class InventoryTypeServiceImplTest {

    /**
     * Mock объект репозитория.
     */
    private InventoryTypeRepository inventoryTypeRepository = mock(InventoryTypeRepository.class);
    /**
     * Тестируемый Сервис
     */
    private InventoryTypeService inventoryTypeService = new InventoryTypeServiceImpl(inventoryTypeRepository);

    private ArrayList<InventoryType> inventoryTypes;

    /**
     * Инициализация  данных для теста.
     *
     * @throws Exception Пробрасываем все исключения.
     */
    @Before
    public void setUp() throws Exception {
        this.inventoryTypes = new ArrayList<>();
        this.inventoryTypes.add(InventoryType.builder().id(1).name("1").active(true).build());
        this.inventoryTypes.add(InventoryType.builder().id(2).name("2").active(true).build());
        this.inventoryTypes.add(InventoryType.builder().id(3).name("3").active(true).build());
    }

    /**
     * Тестирование метода создания нового Типа ТМЦ.
     *
     * @throws Exception Пробрасываем все исключения
     */
    @Test
    public void createInventoryTypeTest() throws Exception {
        InventoryType inventoryType = inventoryTypes.get(1);

        when(inventoryTypeRepository.save(any(InventoryType.class))).thenReturn(inventoryType);

        InventoryType inventoryTypeReturned = inventoryTypeService.create(inventoryType.getName());

        assertThat(inventoryTypeReturned).isNotNull();
        assertThat(inventoryTypeReturned.getName()).isEqualTo(inventoryType.getName());

        verify(inventoryTypeRepository).save(any(InventoryType.class));
    }

    /**
     * Тестирование метода создания нового типа ТМЦ c пустым наименованием.
     *
     * @throws Exception Пробрасываем все исключения
     */
    @Test
    public void createEmptyNameInventoryTypeTest() throws Exception {
        Throwable throwable = catchThrowable(() -> inventoryTypeService.create(""));

        assertThat(throwable).isInstanceOf(IllegalArgumentException.class);
        assertThat(throwable.getMessage()).isEqualTo("Наименование типа ТМЦ не может быть пустым");

        verify(inventoryTypeRepository, never()).save(any(InventoryType.class));
    }

    /**
     * Тестирование метода сохранения типа ТМЦ.
     *
     * @throws Exception Пробрасываем все исключения
     */
    @Test
    public void updateInventoryTypeTest() throws Exception {
        InventoryType inventoryType = inventoryTypes.get(1);

        when(inventoryTypeRepository.findOne(inventoryType.getId())).thenReturn(inventoryType);
        when(inventoryTypeRepository.save(any(InventoryType.class))).thenReturn(inventoryType);

        InventoryType inventoryTypeReturned = inventoryTypeService.save(inventoryType);

        assertThat(inventoryTypeReturned).isNotNull();
        assertThat(inventoryTypeReturned.getName()).isEqualTo(inventoryType.getName());

        verify(inventoryTypeRepository).save(any(InventoryType.class));
    }

    /**
     * Тестирование метода поиска контрагента по идентификатору.
     *
     * @throws Exception Пробрасываем все исключения
     */
    @Test
    public void getByIdInventoryTypeTest() throws Exception {
        InventoryType inventoryType = inventoryTypes.get(1);

        when(inventoryTypeRepository.findOne(inventoryType.getId())).thenReturn(inventoryType);

        InventoryType inventoryTypeReturned = inventoryTypeService.getById(inventoryType.getId());

        assertThat(inventoryTypeReturned).isNotNull();
        assertThat(inventoryTypeReturned).isEqualTo(inventoryType);

        verify(inventoryTypeRepository).findOne(inventoryType.getId());
    }

    /**
     * Тестирование метода возврата всех типов ТМЦ вне зависимости от активности.
     *
     * @throws Exception Пробрасываем все исключения
     */
    @Test
    public void getAllInventoryTypeTest() throws Exception {
        when(inventoryTypeRepository.findAll()).thenReturn(inventoryTypes);

        Collection<InventoryType> inventoryTypesReturned = inventoryTypeService.getAll();

        assertThat(inventoryTypesReturned).isNotNull();
        assertThat(inventoryTypesReturned).isNotEmpty();
        assertThat(inventoryTypesReturned).contains(inventoryTypes.get(1));

        verify(inventoryTypeRepository).findAll();
    }

    /**
     * Тестирование метода возврата всех активных типов ТМЦ.
     *
     * @throws Exception Пробрасываем все исключения
     */
    @Test
    public void getByActiveTrueInventoryTypeTest() throws Exception {
        List<InventoryType> inventoryTypesActive = this.inventoryTypes.stream()
                .filter(InventoryType::isActive)
                .collect(Collectors.toList());

        when(inventoryTypeRepository.findByActiveTrue()).thenReturn(inventoryTypesActive);

        Collection<InventoryType> inventoryTypesReturned = inventoryTypeService.getByActiveTrue();

        assertThat(inventoryTypesReturned).isNotNull();
        assertThat(inventoryTypesReturned).isNotEmpty();
        assertThat(inventoryTypesReturned.stream().filter(i -> !i.isActive()).count()).isZero();

        verify(inventoryTypeRepository).findByActiveTrue();
    }

    /**
     * Тестирование метода удаления типа ТМЦ по идентификатору.
     *
     * @throws Exception Пробрасываем все исключения
     */
    @Test
    public void remove() throws Exception {
        InventoryType inventoryType = inventoryTypes.get(0);

        when(inventoryTypeRepository.findOne(inventoryType.getId())).thenReturn(inventoryType);

        inventoryTypeService.remove(inventoryType.getId());

        verify(inventoryTypeRepository).save(any(InventoryType.class));
    }

}