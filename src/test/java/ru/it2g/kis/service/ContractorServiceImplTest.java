package ru.it2g.kis.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.springframework.test.context.ActiveProfiles;
import ru.it2g.kis.entity.dictionary.Contractor;
import ru.it2g.kis.repository.ContractorRepository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

/**
 * Тестирование сервиса Контрагентов.
 *
 * @author Created by ZotovES on 21.12.2017
 */
@ActiveProfiles(profiles = "dev")
@RunWith(JUnit4.class)
public class ContractorServiceImplTest {

    private ContractorRepository contractorRepository = mock(ContractorRepository.class);

    private ContractorService contractorService = new ContractorServiceImpl(contractorRepository);


    private ArrayList<Contractor> contractors;

    /**
     * Инициализация  данных для теста.
     *
     * @throws Exception Пробрасываем все исключения.
     */
    @Before
    public void setUp() throws Exception {
        this.contractors = new ArrayList<>();
        this.contractors.add(Contractor.builder().id(1).name("1").active(true).build());
        this.contractors.add(Contractor.builder().id(2).name("2").active(true).build());
        this.contractors.add(Contractor.builder().id(3).name("3").active(false).build());
    }

    /**
     * Тестирование метода создания нового контрагента.
     *
     * @throws Exception Пробрасываем все исключения
     */
    @Test
    public void createContractorTest() throws Exception {
        Contractor contractor = contractors.get(1);

        when(contractorRepository.save(any(Contractor.class))).thenReturn(contractor);

        Contractor contractorReturned = contractorService.create(contractor.getName());

        assertThat(contractorReturned).isNotNull();
        assertThat(contractorReturned.getName()).isEqualTo(contractor.getName());

        verify(contractorRepository).save(any(Contractor.class));
    }

    /**
     * Тестирование метода создания нового контрагента c пустым наименованием.
     *
     * @throws Exception Пробрасываем все исключения
     */
    @Test
    public void createEmptyNameContractorTest() throws Exception {
        Throwable thrown = catchThrowable(() -> contractorService.create(""));

        assertThat(thrown).isInstanceOf(IllegalArgumentException.class);
        assertThat(thrown.getMessage()).isEqualTo("Наименование контрагента не может быть пустым");

        verify(contractorRepository, never()).save(any(Contractor.class));
    }

    /**
     * Тестирование метода сохранения контрагента.
     *
     * @throws Exception Пробрасываем все исключения
     */
    @Test
    public void updateContractorTest() throws Exception {
        Contractor contractor = contractors.get(1);

        when(contractorRepository.findOne(contractor.getId())).thenReturn(contractor);
        when(contractorRepository.save(any(Contractor.class))).thenReturn(contractor);
        Contractor contractorReturned = contractorService.save(contractor);

        assertThat(contractorReturned).isNotNull();
        assertThat(contractorReturned).isEqualTo(contractor);

        verify(contractorRepository).save(any(Contractor.class));
    }

    /**
     * Тестирование метода поиска контрагента по идентификатору.
     *
     * @throws Exception Пробрасываем все исключения
     */
    @Test
    public void getByIdContractorTest() throws Exception {
        Contractor contractor = contractors.get(1);

        when(contractorRepository.findOne(contractor.getId())).thenReturn(contractor);
        Contractor contractorReturned = contractorService.getById(contractor.getId());

        assertThat(contractorReturned).isNotNull();
        assertThat(contractorReturned).isEqualTo(contractor);

        verify(contractorRepository).findOne(contractor.getId());
    }

    /**
     * Тестирование метода возврата всех контрагентов вне зависимости от активности.
     *
     * @throws Exception Пробрасываем все исключения
     */
    @Test
    public void getAllContractorTest() throws Exception {
        when(contractorRepository.findAll()).thenReturn(contractors);

        Collection<Contractor> contractorsReturned = contractorService.getAll();

        assertThat(contractorsReturned).isNotNull();
        assertThat(contractorsReturned).isNotEmpty();
        assertThat(contractorsReturned).contains(contractors.get(1));

        verify(contractorRepository).findAll();
    }

    /**
     * Тестирование метода удаления контрагента по идентификатору.
     *
     * @throws Exception Пробрасываем все исключения
     */
    @Test
    public void removeContractorTest() throws Exception {
        Contractor contractor = contractors.get(0);

        when(contractorRepository.findOne(contractor.getId())).thenReturn(contractor);

        contractorService.remove(contractor.getId());

        verify(contractorRepository).save(contractor);
    }

    /**
     * Тестирование метода возврата всех активных контрагентов.
     *
     * @throws Exception Пробрасываем все исключения
     */
    @Test
    public void getByActiveTrueContractorTest() throws Exception {
        List<Contractor> workingContractors = this.contractors.stream()
                .filter(Contractor::isActive)
                .collect(Collectors.toList());

        when(contractorRepository.findByActiveTrue()).thenReturn(workingContractors);

        Collection<Contractor> contractorsReturned = contractorService.getByActiveTrue();

        assertThat(contractorsReturned).isNotNull();
        assertThat(contractorsReturned).isNotEmpty();
        assertThat(contractorsReturned.stream().filter(p -> !p.isActive()).count()).isZero();

        verify(contractorRepository).findByActiveTrue();
    }

    /**
     * Тестирование метода изменеия признака активности контрагента с активного на не активного.
     * Должен проставить дату окончания действия и признак активности в false.
     *
     * @throws Exception Пробрасываем все исключения
     */
    @Test
    public void changeActiveTrueToFalseChangeDatesBeginEndTest() throws  Exception{
        Contractor contractorBeforeChanged = contractors.get(0);
        Contractor contractorAfterChanged = Contractor.builder()
                .id(contractorBeforeChanged.getId())
                .name(contractorBeforeChanged.getName())
                .dateBegin(contractorBeforeChanged.getDateBegin())
                .dateEnd(contractorBeforeChanged.getDateEnd())
                .active(!contractorBeforeChanged.isActive())
                .build();

        when(contractorRepository.findOne(contractorBeforeChanged.getId())).thenReturn(contractorBeforeChanged);


        Contractor contractorReturned = contractorService.changeDateBeginAndDateEnd(contractorAfterChanged);

        assertThat(contractorReturned.isActive()).isFalse();
        assertThat(contractorReturned.getDateEnd()).isNotNull();

        verify(contractorRepository).findOne(contractorBeforeChanged.getId());

    }
    /**
     * Тестирование метода изменеия признака активности контрагента с не активного на активного.
     * Должен проставить дату окончания действия в null, заполнить дату начала действияб, признак активности в false.
     *
     * @throws Exception Пробрасываем все исключения
     */
    @Test
    public void changeActiveFalseToTrueChangeDatesBeginEndTest() throws  Exception{
        Contractor contractorBeforeChanged = contractors.get(2);
        Contractor contractorAfterChanged = Contractor.builder()
                .id(contractorBeforeChanged.getId())
                .name(contractorBeforeChanged.getName())
                .dateBegin(contractorBeforeChanged.getDateBegin())
                .dateEnd(contractorBeforeChanged.getDateEnd())
                .active(!contractorBeforeChanged.isActive())
                .build();

        when(contractorRepository.findOne(contractorBeforeChanged.getId())).thenReturn(contractorBeforeChanged);

        Contractor contractorReturned = contractorService.changeDateBeginAndDateEnd(contractorAfterChanged);

        assertThat(contractorReturned.isActive()).isTrue();
        assertThat(contractorReturned.getDateEnd()).isNull();
        assertThat(contractorReturned.getDateBegin()).isNotNull();
        verify(contractorRepository).findOne(contractorBeforeChanged.getId());

    }

}