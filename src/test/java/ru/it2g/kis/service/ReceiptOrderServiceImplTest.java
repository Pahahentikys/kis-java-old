package ru.it2g.kis.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import ru.it2g.kis.entity.Inventory;
import ru.it2g.kis.entity.ReceiptOrder;
import ru.it2g.kis.entity.dictionary.OrderType;
import ru.it2g.kis.repository.ReceiptOrderRepository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

/**
 * Created by RyabushevSA on 29.09.2017
 */
@RunWith(JUnit4.class)
public class ReceiptOrderServiceImplTest {

    private ReceiptOrderRepository receiptOrderRepository = mock(ReceiptOrderRepository.class);

    private ReceiptOrderService receiptOrderService = new ReceiptOrderServiceImpl(receiptOrderRepository);

    private ArrayList<ReceiptOrder> receiptOrders;

    /**
     * Метод для инициализации тестовых данных.
     *
     * @throws Exception - проброс исключений.
     */
    @Before
    public void initializeData() throws Exception {
        this.receiptOrders = new ArrayList<>();
        this.receiptOrders.add(ReceiptOrder.builder().id(1L).name("testOrderDelete").build());
    }

    @Test
    public void create_success() throws Exception {

        ReceiptOrder test = ReceiptOrder.builder()
                .id(1)
                .name("Test")
                .dateCreate(new Date())
                .inventories(Collections.emptyList())
                .build();

        given(receiptOrderRepository.save(any(ReceiptOrder.class))).willReturn(test);

        ReceiptOrder receiptOrder = receiptOrderService.create("Test");

        assertThat(receiptOrder).isNotNull();
        assertThat(receiptOrder.getId()).isGreaterThan(0);
        assertThat(receiptOrder.getDateCreate()).isBefore(new Date());
        assertThat(receiptOrder.getInventories()).isEmpty();
    }

    @Test(expected = Exception.class)
    public void create_empty_name() throws Exception {
//        receiptOrderService.create(null);
    }

    @Test
    public void test_add_inventory_to_order() throws Exception {

        ReceiptOrder testOrder = ReceiptOrder
                .builder()
                .name("test")
                .inventories(new ArrayList<>())
                .build();
        Inventory testInventory = Inventory.builder()
                .name("testInventoryName")
                .build();
        given(receiptOrderRepository.save(any(ReceiptOrder.class))).will((invocationOnMock -> invocationOnMock.getArguments()[0]));

        ReceiptOrder order = receiptOrderService.addInventory(testOrder, testInventory);

        assertThat(order).isNotNull();
        assertThat(order.getInventories()).isNotNull();
        assertThat(order.getInventories()).isNotEmpty();
        assertThat(order.getInventories().contains(testInventory)).isTrue();
        Inventory inventory = order.getInventories().stream().findFirst().orElse(null);
        assertThat(order).isNotNull();
        assertThat(inventory).isNotNull();
        assertThat(inventory.getReceiptOrder()).isNotNull();
    }

    @Test(expected = Exception.class)
    public void test_add_inventory_with_null_all_parameters() throws Exception {
        receiptOrderService.addInventory(null, null);
    }

    @Test(expected = Exception.class)
    public void test_add_inventory_with_null_order() throws Exception {

        Inventory testInventory = Inventory.builder()
                .name("testInventoryName")
                .build();

        receiptOrderService.addInventory(null, testInventory);
    }

    @Test(expected = Exception.class)
    public void test_add_inventory_with_null_inventory() throws Exception {

        ReceiptOrder testOrder = ReceiptOrder
                .builder()
                .name("test")
                .inventories(new ArrayList<>())
                .build();

        receiptOrderService.addInventory(testOrder, null);
    }

    @Test
    public void test_remove_inventory() throws Exception {

        ReceiptOrder testOrder = ReceiptOrder
                .builder()
                .name("test")
                .inventories(new ArrayList<>())
                .build();

        Inventory testInventory = Inventory.builder()
                .name("testInventoryName")
                .build();

        ReceiptOrder receiptOrder = receiptOrderService.removeInventory(testOrder, testInventory);
        assertThat(receiptOrder).isNotNull();
    }

    /**
     * Тестирование на поиска ордера по типу, где тип задан в виде идентификатора.
     *
     * @throws Exception - проброс исключений.
     */
    @Test
    public void findReceiptOrderBuOrderTypeId() throws Exception {

        OrderType orderTypeOne = OrderType.builder()
                .id(1L)
                .type("новый")
                .build();

        ReceiptOrder receiptOrder = ReceiptOrder.builder()
                .id(1L)
                .name("testOrder")
                .orderType(orderTypeOne)
                .build();

        Collection<ReceiptOrder> receiptOrders = new ArrayList<>();
        receiptOrders.add(receiptOrder);

        given(receiptOrderRepository.findByOrderTypeId(orderTypeOne.getId())).willReturn(receiptOrders);

        assertThat(receiptOrderService.findByOrderTypeId(orderTypeOne.getId())).isNotEmpty();
    }

    /**
     * Тестирование "мягкого" удаления приходного ордера.
     *
     * @throws Exception - проброс исключений.
     */
    @Test
    public void removeReceiptOrderById() throws Exception {

        ReceiptOrder receiptOrder = receiptOrders.get(0);

        when(receiptOrderRepository.findOne(receiptOrder.getId())).thenReturn(receiptOrder);

        receiptOrderService.remove(receiptOrder.getId());

        verify(receiptOrderRepository).save(receiptOrder);

    }
}

