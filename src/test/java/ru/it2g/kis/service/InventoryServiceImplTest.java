package ru.it2g.kis.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import ru.it2g.kis.entity.Inventory;
import ru.it2g.kis.repository.InventoryRepository;

import static org.mockito.BDDMockito.given;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by RyabushevSA on 29.09.2017
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class InventoryServiceImplTest {

    @MockBean
    private InventoryRepository inventoryRepository;

    @Autowired
    private InventoryService inventoryService;

    @Test
    public void save() throws Exception {
        Inventory inventory = new Inventory();
        given(inventoryRepository.save(inventory)).willReturn(Inventory.builder().id(1).build());

        Inventory saved = inventoryService.save(inventory);
        assertThat(saved).isNotNull();
        assertThat(saved.getId()).isEqualTo(1);
    }

}