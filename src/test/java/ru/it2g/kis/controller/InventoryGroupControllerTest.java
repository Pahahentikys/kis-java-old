package ru.it2g.kis.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.it2g.kis.converter.InventoryGroupToDtoConverter;
import ru.it2g.kis.dto.InventoryGroupDto;
import ru.it2g.kis.entity.InventoryGroup;
import ru.it2g.kis.repository.InventoryGroupRepository;
import ru.it2g.kis.service.InventoryGroupService;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.hamcrest.Matchers.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Тест для REST контроллера Групп ТМЦ.
 *
 * @author ZotovES on 11.02.2018.
 */
@ActiveProfiles(profiles = "dev")
@RunWith(SpringRunner.class)
@WebMvcTest({InventoryGroupController.class, InventoryGroupToDtoConverter.class})
public class InventoryGroupControllerTest {
    @Autowired
    private WebApplicationContext context;
    @Autowired
    private ObjectMapper mapper;
    @Autowired
    private InventoryGroupToDtoConverter inventoryGroupToDtoConverter;

    @MockBean
    private InventoryGroupService inventoryGroupService;
    @MockBean
    private InventoryGroupRepository inventoryGroupRepository;

    private MockMvc mockMvc;
    private ArrayList<InventoryGroup> inventoryGroups;

    private MediaType contentType = new MediaType(
            MediaType.APPLICATION_JSON_UTF8.getType(),
            MediaType.APPLICATION_JSON_UTF8.getSubtype(),
            MediaType.APPLICATION_JSON_UTF8.getCharset()
    );

    /**
     * Инициализация mock объектов.
     *
     */
    @Before
    public void setUp() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
        this.inventoryGroups = new ArrayList<>();
        this.inventoryGroups.add(InventoryGroup.builder().id(1).name("test group1").active(true).build());
        this.inventoryGroups.add(InventoryGroup.builder().id(2).name("test group2").active(true).build());
        this.inventoryGroups.add(InventoryGroup.builder().id(3).name("test group3").active(false).build());
    }

    /**
     * Тестирование метода чтения всех групп ТМЦ.
     *
     * @throws Exception Пробрасываем все исключения.
     */
    @Test
    public void getInventoryGroupsTest() throws Exception {
        when(inventoryGroupService.getAll()).thenReturn(inventoryGroups);

        ResultActions result = mockMvc.perform(get("/inventory-groups/"));
        result.andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(this.inventoryGroups.size())))
                .andExpect(jsonPath("$[0].id", is((int) this.inventoryGroups.get(0).getId())))
                .andExpect(jsonPath("$[0].name", is(this.inventoryGroups.get(0).getName())))
                .andExpect(jsonPath("$[1].id", is((int) this.inventoryGroups.get(1).getId())))
                .andExpect(jsonPath("$[1].name", is(this.inventoryGroups.get(1).getName())))
                .andDo(print());

        verify(inventoryGroupService).getAll();
    }

    /**
     * Тестирование метода поиска активных групп ТМЦ.
     *
     * @throws Exception Пробрасываем все исключения.
     */
    @Test
    public void getActiveInventoryGroupsTest() throws Exception {
        List<InventoryGroup> activeInventoryGroup = this.inventoryGroups.stream()
                .filter(InventoryGroup::isActive)
                .collect(Collectors.toList());
        InventoryGroup notActiveInventoryGroup = this.inventoryGroups.stream()
                .filter(p -> !p.isActive())
                .findFirst()
                .orElse(null);

        when(inventoryGroupService.getByActive()).thenReturn(activeInventoryGroup);

        if (notActiveInventoryGroup == null) {
            throw new NullPointerException("В списке должена быть хотя бы одина не активная группа");
        }

        ResultActions result = mockMvc.perform(get("/inventory-groups/active"));
        result.andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(activeInventoryGroup.size())))
                .andExpect(jsonPath("$[0].id", not((int) notActiveInventoryGroup.getId())))
                .andExpect(jsonPath("$[0].name", not(notActiveInventoryGroup.getName())))
                .andExpect(jsonPath("$[1].id", not((int) notActiveInventoryGroup.getId())))
                .andExpect(jsonPath("$[1].name", not(notActiveInventoryGroup.getName())))
                .andDo(print());

        verify(inventoryGroupService).getByActive();
    }

    /**
     * Тестирование поиска группы тмц по идентификатору.
     *
     * @throws Exception Пробрасываем все исключения.
     */
    @Test
    public void getByIdInventoryGroupTest() throws Exception {
        InventoryGroup inventoryGroup = this.inventoryGroups.get(1);

        when(inventoryGroupService.getById(inventoryGroup.getId())).thenReturn(inventoryGroup);

        ResultActions result = mockMvc.perform(get("/inventory-groups/{id}", inventoryGroup.getId()));
        result.andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("id", is((int) inventoryGroup.getId())))
                .andExpect(jsonPath("name", is(inventoryGroup.getName())))
                .andDo(print());

        verify(inventoryGroupService).getById(inventoryGroup.getId());
    }

    /**
     * Тестирование поиска не существующей группы тмц по идентификатору.
     *
     * @throws Exception Пробрасываем все исключения.
     */
    @Test
    public void notFoundGetByIdInventoryGroupTest() throws Exception {
        InventoryGroup inventoryGroup = this.inventoryGroups.get(1);

        when(inventoryGroupService.getById(inventoryGroup.getId())).thenReturn(null);

        mockMvc.perform(get("/inventory-groups/{id}", inventoryGroup.getId())
                .contentType(contentType))
                .andExpect(status().isNotFound())
                .andDo(print());

        verify(inventoryGroupService).getById(inventoryGroup.getId());
    }

    /**
     * Тестирование создания новой группы ТМЦ.
     *
     * @throws Exception Пробрасываем все исключения.
     */
    @Test
    public void createInventoryGroupTest() throws Exception {
        InventoryGroup inventoryGroup = this.inventoryGroups.get(1);
        InventoryGroupDto inventoryGroupDto = InventoryGroupDto.builder().name("test group").build();
        String json = mapper.writeValueAsString(inventoryGroupDto);

        when(inventoryGroupService.save(any(InventoryGroup.class))).thenReturn(inventoryGroup);

        mockMvc.perform(post("/inventory-groups")
                .content(json)
                .contentType(contentType))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("name", is(inventoryGroup.getName())))
                .andDo(print());

        verify(inventoryGroupService).save(any(InventoryGroup.class));
    }

    /**
     * Тестирование метода редактирования группы ТМЦ.
     *
     * @throws Exception Пробрасываем все исключения.
     */
    @Test
    public void updateInventoryGroupTest() throws Exception {
        InventoryGroup inventoryGroup = inventoryGroups.get(1);
        InventoryGroupDto inventoryGroupDto = inventoryGroupToDtoConverter.convert(inventoryGroup);
        String json = mapper.writeValueAsString(inventoryGroupDto);

        when(inventoryGroupRepository.exists(inventoryGroup.getId())).thenReturn(true);
        when(inventoryGroupService.save(any(InventoryGroup.class))).thenReturn(inventoryGroup);

        mockMvc.perform(put("/inventory-groups/{id}", inventoryGroup.getId())
                .content(json)
                .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonPath("name", is(inventoryGroup.getName())))
                .andDo(print());

        verify(inventoryGroupRepository).exists(inventoryGroup.getId());
        verify(inventoryGroupService).save(any(InventoryGroup.class));
    }

    /**
     * Тестирование метода редактирования группы ТМЦ
     * c не совпадающим идентификатором в параметре запроса и теле json.
     *
     * @throws Exception Пробрасываем все исключения.
     */
    @Test
    public void updateInventoryGroupConflictIdTest() throws Exception {
        InventoryGroup inventoryGroup = inventoryGroups.get(1);
        InventoryGroupDto inventoryGroupDto = InventoryGroupDto.builder()
                .id(inventoryGroup.getId() + 1)
                .name("test")
                .build();
        String json = mapper.writeValueAsString(inventoryGroupDto);

        when(inventoryGroupRepository.exists(inventoryGroup.getId())).thenReturn(true);
        when(inventoryGroupService.save(any(InventoryGroup.class))).thenReturn(inventoryGroup);

        mockMvc.perform(put("/inventory-groups/{id}", inventoryGroup.getId())
                .content(json)
                .contentType(contentType))
                .andExpect(status().isConflict())
                .andDo(print());

        verify(inventoryGroupRepository, never()).exists(inventoryGroup.getId());
        verify(inventoryGroupService, never()).save(any(InventoryGroup.class));
    }

    /**
     * Тестирование метода редактирования группы ТМЦ
     * c не несуществующим  идентификатором.
     *
     * @throws Exception Пробрасываем все исключения.
     */
    @Test
    public void updateInventoryGroupNotFoundIdTest() throws Exception {
        InventoryGroup inventoryGroup = inventoryGroups.get(1);
        InventoryGroupDto inventoryGroupDto = inventoryGroupToDtoConverter.convert(inventoryGroup);
        String json = mapper.writeValueAsString(inventoryGroupDto);

        when(inventoryGroupRepository.exists(inventoryGroup.getId())).thenReturn(false);
        when(inventoryGroupService.save(any(InventoryGroup.class))).thenReturn(inventoryGroup);

        mockMvc.perform(put("/inventory-groups/{id}", inventoryGroup.getId())
                .content(json)
                .contentType(contentType))
                .andExpect(status().isNotFound())
                .andDo(print());

        verify(inventoryGroupRepository).exists(inventoryGroup.getId());
        verify(inventoryGroupService, never()).save(any(InventoryGroup.class));
    }

    /**
     * Тестирование метода удаления группы ТМЦ по идентификатору.
     *
     * @throws Exception Пробрасываем все исключения.
     */
    @Test
    public void deleteByIdInventoryGroupTest() throws Exception {
        InventoryGroup inventoryGroup = inventoryGroups.get(1);

        when(inventoryGroupRepository.exists(inventoryGroup.getId())).thenReturn(true);

        mockMvc.perform(delete("/inventory-groups/{id}", inventoryGroup.getId())
                .contentType(contentType))
                .andExpect(status().isNoContent())
                .andDo(print());

        verify(inventoryGroupRepository).exists(inventoryGroup.getId());
        verify(inventoryGroupService).remove(inventoryGroup.getId());
    }

    /**
     * Тестирование метода удаления группы ТМЦ по идентификатору.
     *
     * @throws Exception Пробрасываем все исключения.
     */
    @Test
    public void deleteByIdInventoryGroupNotFoundTest() throws Exception {
        InventoryGroup inventoryGroup = inventoryGroups.get(1);

        when(inventoryGroupRepository.exists(inventoryGroup.getId())).thenReturn(false);

        mockMvc.perform(delete("/inventory-groups/{id}", inventoryGroup.getId())
                .contentType(contentType))
                .andExpect(status().isNotFound())
                .andDo(print());

        verify(inventoryGroupRepository).exists(inventoryGroup.getId());
        verify(inventoryGroupService, never()).remove(inventoryGroup.getId());
    }
}