package ru.it2g.kis.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.it2g.kis.converter.ReceiptOrderToDtoConverter;
import ru.it2g.kis.entity.ReceiptOrder;
import ru.it2g.kis.entity.dictionary.OrderType;
import ru.it2g.kis.entity.dictionary.Organization;
import ru.it2g.kis.service.ReceiptOrderService;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ActiveProfiles(profiles = "dev")
@RunWith(SpringRunner.class)
@WebMvcTest({ReceiptOrderController.class, ReceiptOrderToDtoConverter.class})
public class ReceiptOrderControllerTest {

    @Autowired
    private WebApplicationContext context;

    private MockMvc mockMvc;

    private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(),
            Charset.forName("utf8"));

    @MockBean
    private ReceiptOrderService receiptOrderService;

    @Before
    public void setUp() throws Exception {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
    }

    @Test
    public void getDocument() throws Exception {


        Organization organization1 = Organization.builder().id(1).build();
        ReceiptOrder receiptOrder = ReceiptOrder.builder()
                .id(123)
                .name("Test 1")
                .dateCreate(new Date())
                .expenseDocumentNumber("123456")
                .expenseOrganization(organization1)
                .build();

        given(receiptOrderService.getById(123L)).willReturn(receiptOrder);

        mockMvc.perform(get("/receipt-orders/{id}", receiptOrder.getId()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("id", is((int) receiptOrder.getId())))
                .andExpect(jsonPath("name", is(receiptOrder.getName())))
                .andExpect(jsonPath("dateCreate", is(receiptOrder.getDateCreate().getTime())))
                .andExpect(jsonPath("expenseDocumentNumber", is(receiptOrder.getExpenseDocumentNumber())))
                .andExpect(jsonPath("expenseOrganizationId", is((int) receiptOrder.getExpenseOrganization().getId())))
                .andDo(print());
    }

    /**
     * Тестирование контроллера поиска ордеров по типу, где тип ордера задан определённым идентификатором.
     *
     * @throws Exception - если тип ордера указан в некорректной форме.
     */
    @Test
    public void getOrderByTypeId() throws Exception {
        OrderType orderTypeOne = OrderType.builder()
                .id(1L)
                .type("новый")
                .build();

        ReceiptOrder receiptOrder = ReceiptOrder.builder()
                .id(1L)
                .name("testOrder")
                .orderType(orderTypeOne)
                .build();

        List<ReceiptOrder> receiptOrderCollection = new ArrayList<ReceiptOrder>();

        receiptOrderCollection.add(receiptOrder);

        given(receiptOrderService.findByOrderTypeId(1L)).willReturn(receiptOrderCollection);

        mockMvc.perform(get("/receipt-orders?type=1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType));
    }
}