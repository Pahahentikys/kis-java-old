package ru.it2g.kis.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.it2g.kis.converter.InventoryTypeToDtoConverter;
import ru.it2g.kis.dto.InventoryTypeDto;
import ru.it2g.kis.entity.InventoryType;
import ru.it2g.kis.repository.InventoryTypeRepository;
import ru.it2g.kis.service.InventoryTypeService;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Тест для REST контроллера Типа ТМЦ.
 *
 * @author Created by ZotovES on 05.03.2018
 */

@ActiveProfiles(profiles = "dev")
@RunWith(SpringRunner.class)
@WebMvcTest({InventoryTypeController.class, InventoryTypeToDtoConverter.class})
public class InventoryTypeControllerTest {

    @Autowired
    private WebApplicationContext context;
    @Autowired
    private ObjectMapper mapper;

    private MediaType contentType = new MediaType(
            MediaType.APPLICATION_JSON_UTF8.getType(),
            MediaType.APPLICATION_JSON_UTF8.getSubtype(),
            MediaType.APPLICATION_JSON_UTF8.getCharset());

    private MockMvc mockMvc;
    private ArrayList<InventoryType> inventoryTypes;

    @MockBean
    private InventoryTypeService inventoryTypeService;
    @MockBean
    private InventoryTypeRepository inventoryTypeRepository;
    @Autowired
    private InventoryTypeToDtoConverter inventoryTypeToDtoConverter;

    /**
     * Инициализация  mock объектов.
     *
     */
    @Before
    public void setUp() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
        this.inventoryTypes = new ArrayList<>();
        this.inventoryTypes.add(InventoryType.builder().id(1).name("1").active(true).build());
        this.inventoryTypes.add(InventoryType.builder().id(2).name("2").active(true).build());
        this.inventoryTypes.add(InventoryType.builder().id(3).name("3").active(false).build());
    }

    /**
     * Тестирование метода чтения всех контрагентов.
     *
     * @throws Exception Пробрасываем все исключения.
     */
    @Test
    public void getAllInventoryTypesTest() throws Exception {
        when(inventoryTypeService.getAll()).thenReturn(this.inventoryTypes);
        mockMvc.perform(get("/inventory-types").contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(this.inventoryTypes.size())))
                .andExpect(jsonPath("$[0].id", is((int) this.inventoryTypes.get(0).getId())))
                .andExpect(jsonPath("$[0].name", is(this.inventoryTypes.get(0).getName())))
                .andExpect(jsonPath("$[1].id", is((int) this.inventoryTypes.get(1).getId())))
                .andExpect(jsonPath("$[1].name", is(this.inventoryTypes.get(1).getName())))
                .andDo(print());
        verify(inventoryTypeService).getAll();
    }

    /**
     * Тестирование получения только активных типов ТМЦ.
     *
     * @throws Exception Пробрасываем все исключения.
     */
    @Test
    public void getActiveInventoryTypesTest() throws Exception {
        List<InventoryType> inventoryTypesActive = this.inventoryTypes.stream()
                .filter(InventoryType::isActive)
                .collect(Collectors.toList());

        InventoryType nonActiveInventoryType = this.inventoryTypes.stream()
                .filter(i -> !i.isActive())
                .findFirst()
                .orElse(null);

        when(inventoryTypeService.getByActiveTrue()).thenReturn(inventoryTypesActive);

        if (nonActiveInventoryType == null) {
            throw new NullPointerException("В списке должен быть хотябы один не активный тип");
        }

        ResultActions result = mockMvc.perform(get("/inventory-types/active")
                .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(inventoryTypesActive.size())))
                .andExpect(jsonPath("$[0].id", is((int) inventoryTypesActive.get(0).getId())))
                .andExpect(jsonPath("$[0].name", is(inventoryTypesActive.get(0).getName())))
                .andExpect(jsonPath("$[1].id", is((int) inventoryTypesActive.get(1).getId())))
                .andExpect(jsonPath("$[1].name", is(inventoryTypesActive.get(1).getName())))
                .andDo(print());

        verify(inventoryTypeService).getByActiveTrue();
    }

    /**
     * Проверка метода поиска типа ТМЦ по идентификатору
     *
     * @throws Exception Пробрасываем все исключения.
     */
    @Test
    public void getInventoryTypesByIdTest() throws Exception {
        InventoryType inventoryType = this.inventoryTypes.get(1);

        when(inventoryTypeService.getById(inventoryType.getId())).thenReturn(inventoryType);

        mockMvc.perform(get("/inventory-types/{id}", inventoryType.getId())
                .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonPath("id", is((int) inventoryType.getId())))
                .andExpect(jsonPath("name", is(inventoryType.getName())))
                .andDo(print());

        verify(inventoryTypeService).getById(inventoryType.getId());
    }

    /**
     * Проверка метода поиска типа ТМЦ по несуществующему идентификатору
     *
     * @throws Exception Пробрасываем все исключения.
     */
    @Test
    public void notFoundInventoryTypeByIdTest() throws Exception {
        InventoryType inventoryType = this.inventoryTypes.get(1);

        when(inventoryTypeService.getById(inventoryType.getId())).thenReturn(null);

        mockMvc.perform(get("/inventory-types/{id}", inventoryType.getId())
                .contentType(contentType))
                .andExpect(status().isNotFound())
                .andDo(print());

        verify(inventoryTypeService).getById(inventoryType.getId());
    }

    /**
     * Тестирование добавления нового экземпляра типа ТМЦ.
     *
     * @throws Exception Пробрасываем все исключения
     */
    @Test
    public void createInventoryTypesTest() throws Exception {
        InventoryType inventoryType = this.inventoryTypes.get(1);
        InventoryTypeDto inventoryTypeDto = InventoryTypeDto.builder().name("test").build();
        String json = mapper.writeValueAsString(inventoryTypeDto);

        when(inventoryTypeService.save(any(InventoryType.class))).thenReturn(inventoryType);

        mockMvc.perform(post("/inventory-types")
                .content(json)
                .contentType(contentType))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("name", is(inventoryType.getName())))
                .andDo(print());

        verify(inventoryTypeService).save(any(InventoryType.class));
    }

    /**
     * Тестирование редактирования типа ТМЦ.
     *
     * @throws Exception Пробрасываем все исключения.
     */
    @Test
    public void updateInventoryTypesTest() throws Exception {
        InventoryType inventoryType = this.inventoryTypes.get(1);
        InventoryTypeDto inventoryTypeDto = inventoryTypeToDtoConverter.convert(inventoryType);
        String json = mapper.writeValueAsString(inventoryTypeDto);

        when(inventoryTypeRepository.exists(inventoryType.getId())).thenReturn(true);
        when(inventoryTypeService.save(any(InventoryType.class))).thenReturn(inventoryType);

        mockMvc.perform(put("/inventory-types/{id}", inventoryType.getId())
                .content(json)
                .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonPath("name", is(inventoryType.getName())))
                .andDo(print());

        verify(inventoryTypeRepository).exists(inventoryType.getId());
        verify(inventoryTypeService).save(any(InventoryType.class));
    }

    /**
     * Тестирование редактирования Типа ТМЦ
     * с несовпадающими идентификаторами в параметре запроса и теле json.
     *
     * @throws Exception Пробрасываем все исключения.
     */
    @Test
    public void updateInventoryTypesConflictIdTest() throws Exception {
        InventoryType inventoryType = this.inventoryTypes.get(1);
        InventoryTypeDto inventoryTypeDto = InventoryTypeDto.builder()
                .id(inventoryType.getId() + 1)
                .name("test")
                .build();

        String json = mapper.writeValueAsString(inventoryTypeDto);

        when(inventoryTypeRepository.exists(inventoryType.getId())).thenReturn(true);
        when(inventoryTypeService.save(any(InventoryType.class))).thenReturn(inventoryType);

        mockMvc.perform(put("/inventory-types/{id}", inventoryType.getId())
                .content(json)
                .contentType(contentType))
                .andExpect(status().isConflict())
                .andDo(print());

        verify(inventoryTypeRepository, never()).exists(inventoryType.getId());
        verify(inventoryTypeService, never()).save(any(InventoryType.class));
    }

    /**
     * Тестирование редактирования Типа ТМЦ
     * с несуществующим идентификатором.
     *
     * @throws Exception Пробрасываем все исключения.
     */
    @Test
    public void updateInventoryTypesNotFoundTest() throws Exception {
        InventoryType inventoryType = this.inventoryTypes.get(1);
        InventoryTypeDto inventoryTypeDto = InventoryTypeDto.builder()
                .id(inventoryType.getId())
                .name("test")
                .build();

        String json = mapper.writeValueAsString(inventoryTypeDto);

        when(inventoryTypeRepository.exists(inventoryType.getId())).thenReturn(false);
        when(inventoryTypeService.save(any(InventoryType.class))).thenReturn(inventoryType);

        mockMvc.perform(put("/inventory-types/{id}", inventoryType.getId())
                .content(json)
                .contentType(contentType))
                .andExpect(status().isNotFound())
                .andDo(print());

        verify(inventoryTypeRepository).exists(inventoryType.getId());
        verify(inventoryTypeService, never()).save(any(InventoryType.class));
    }

    /**
     * Тестирование удаления типа ТМЦ .
     *
     * @throws Exception Пробрасываем все исключения.
     */
    @Test
    public void deleteInventoryTypesByIdTest() throws Exception {
        InventoryType inventoryType = this.inventoryTypes.get(1);

        when(inventoryTypeRepository.exists(inventoryType.getId())).thenReturn(true);

        mockMvc.perform(delete("/inventory-types/{id}", inventoryType.getId())
                .contentType(contentType))
                .andExpect(status().isNoContent())
                .andDo(print());

        verify(inventoryTypeService).remove(inventoryType.getId());
    }

    /**
     * Тестирование удаления типа ТМЦ по несуществующему идентификатору.
     *
     * @throws Exception Пробрасываем все исключения.
     */
    public void deleteInventoryTypesByIdNotFondTest() throws Exception {
        InventoryType inventoryType = this.inventoryTypes.get(1);

        when(inventoryTypeRepository.exists(inventoryType.getId())).thenReturn(false);

        mockMvc.perform(delete("/inventory-types/{id}", inventoryType.getId())
                .contentType(contentType))
                .andExpect(status().isNotFound())
                .andDo(print());

        verify(inventoryTypeService, never()).remove(inventoryType.getId());
    }
}