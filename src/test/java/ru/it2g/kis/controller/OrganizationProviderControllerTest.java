package ru.it2g.kis.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.it2g.kis.converter.OrganizationProviderToDtoConverter;
import ru.it2g.kis.converter.OrganizationToDtoConverter;
import ru.it2g.kis.dto.OrganizationDto;
import ru.it2g.kis.dto.OrganizationProviderDto;
import ru.it2g.kis.entity.dictionary.Organization;
import ru.it2g.kis.entity.dictionary.OrganizationProvider;
import ru.it2g.kis.service.OrganizationProviderService;

import java.util.ArrayList;
import java.util.stream.Collectors;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * @author Pahahentikys on 13.02.2018
 * <p>
 * Тест для контроллера организации-поставщика (OrganizationProvider).
 */
@ActiveProfiles(profiles = "dev")
@RunWith(SpringRunner.class)
@WebMvcTest({OrganizationProviderController.class, OrganizationToDtoConverter.class, OrganizationProviderToDtoConverter.class})
public class OrganizationProviderControllerTest {

    /**
     * Инъекция MockMvc.
     */
    @Autowired
    private MockMvc mockMvc;

    /**
     * Инъекция конткекста.
     */
    @Autowired
    private WebApplicationContext webApplicationContext;

    /**
     * Инъекция DTO-конвертора.
     */
    @Autowired
    private OrganizationProviderToDtoConverter organizationProviderToDtoConverter;

    /**
     * Mock на серивис.
     */
    @MockBean
    private OrganizationProviderService organizationProviderService;

    /**
     * Коллекция с тестовыми данными.
     */
    private ArrayList<OrganizationProvider> organizationProviders;

    /**
     * Массив с типом контента.
     */
    private MediaType mediaType = new MediaType(
            MediaType.APPLICATION_JSON_UTF8.getType(),
            MediaType.APPLICATION_JSON_UTF8.getSubtype(),
            MediaType.APPLICATION_JSON_UTF8.getCharset()
    );

    /**
     * Инициализация тестовых данных.
     * @throws Exception - проброс исключений.
     */
    @Before
    public void initializeTestDataAndContext() throws Exception {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();

        Organization organizationOne = Organization.builder()
                .id(1L)
                .name("testOneOrganizationOne")
                .build();

        Organization organizationTwo = Organization.builder()
                .id(2L)
                .name("testOneOrganizationTwo")
                .build();

        OrganizationProvider organizationProviderOne = OrganizationProvider.builder()
                .id(1L)
                .organization(organizationOne)
                .active(true)
                .build();

        OrganizationProvider organizationProviderTwo = OrganizationProvider.builder()
                .id(2L)
                .organization(organizationTwo)
                .active(true)
                .build();

        OrganizationProvider organizationProviderThree = OrganizationProvider.builder()
                .id(3L)
                .organization(organizationTwo)
                .active(false)
                .build();

        organizationProviders = new ArrayList<>();
        organizationProviders.add(organizationProviderOne);
        organizationProviders.add(organizationProviderTwo);
        organizationProviders.add(organizationProviderThree);

        when(organizationProviderService.getAll()).thenReturn(organizationProviders);

        when(organizationProviderService.getById(organizationOne.getId())).thenReturn(organizationProviderOne);

        when(organizationProviderService.getByActiveTrue()).thenReturn(organizationProviders.stream()
                .filter(OrganizationProvider::isActive)
                .collect(Collectors.toList())
        );

    }

    /**
     * Тест на получение всех организаций-поставщиков, активных и неактивных.
     * @throws Exception - проброс исключений.
     */
    @Test
    public void getAllOrganizationProviders() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/organization-providers"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(mediaType))
                .andExpect(jsonPath("$", hasSize(organizationProviders.size())))
                .andExpect(jsonPath("$[0].id").value(organizationProviders.get(0).getId()))
                .andExpect(jsonPath("$[1].id").value(organizationProviders.get(1).getId()))
                .andDo(print());

        verify(organizationProviderService).getAll();
    }

    /**
     * Тест на получение организации-поставщика по идентифкатору.
     * @throws Exception - проброс исключений.
     */
    @Test
    public void getOrganizationProviderById() throws Exception {
        OrganizationProvider organizationProvider = organizationProviders.get(0);

        mockMvc.perform(MockMvcRequestBuilders.get("/organization-providers/{id}", organizationProvider.getId()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(mediaType))
                .andExpect(jsonPath("$.id").value(organizationProvider.getId()))
                .andDo(print());

        verify(organizationProviderService).getById(organizationProvider.getId());
    }

    /**
     * Тест на получение активных организаций-поставщиков.
     * @throws Exception - проброс исключений.
     */
    @Test
    public void getActiveOrganizationProviders() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/organization-providers/active"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(mediaType))
                .andExpect(jsonPath("$").value(hasSize(2)))
                .andDo(print());

        verify(organizationProviderService).getByActiveTrue();
    }

    /**
     * Тест на создание организации-поставщика.
     * @throws Exception - проброс исключений.
     */
    @Test
    public void createOrganizationProvider() throws Exception {
        OrganizationProvider organizationProvider = organizationProviders.get(1);

        OrganizationDto organizationDto = OrganizationDto.builder()
                .id(1L)
                .name("testOrganizationDto")
                .build();

        OrganizationProviderDto organizationProviderDto = OrganizationProviderDto.builder()
                .id(1L)
                .organization(organizationDto)
                .build();

        ObjectMapper objectMapper = new ObjectMapper();

        String mappedToStringObject = (objectMapper.writeValueAsString(organizationProviderDto));

        when(organizationProviderService.create(any(OrganizationProvider.class))).thenReturn(organizationProvider);

        mockMvc.perform(MockMvcRequestBuilders.post("/organization-providers")
                .content(mappedToStringObject)
                .contentType(mediaType))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(organizationProvider.getId()))
                .andDo(print());

        verify(organizationProviderService).create(any(OrganizationProvider.class));
    }

    /**
     * Тестирование удаления организации-поставщика по идентификатору.
     * @throws Exception - проброс исключений.
     */
    @Test
    public void deleteOrganizationProviderById() throws Exception {
        OrganizationProvider organizationProvider = organizationProviders.get(0);

        mockMvc.perform(MockMvcRequestBuilders.delete("/organization-providers/{id}", organizationProvider.getId())
                .contentType(mediaType))
                .andExpect(status().isNoContent())
                .andDo(print());

        verify(organizationProviderService).remove(organizationProvider.getId());

    }

    /**
     * Тестирование обновления об организации-поставщике.
     * @throws Exception - проброс исключений.
     */
    @Test
    public void updateOrganizationProvider() throws Exception {
        Organization organizationThree = Organization.builder()
                .id(3L)
                .name("testOneOrganizationThree")
                .build();

        OrganizationProvider organizationProviderThreeUpdated = OrganizationProvider.builder()
                .id(3L)
                .organization(organizationThree)
                .active(true)
                .build();

        OrganizationProviderDto organizationProviderDto = organizationProviderToDtoConverter.convert(organizationProviderThreeUpdated);

        ObjectMapper objectMapper = new ObjectMapper();

        String mappedToStringObject = (objectMapper.writeValueAsString(organizationProviderDto));

        when(organizationProviderService.getById(organizationProviderThreeUpdated.getId())).thenReturn(organizationProviderThreeUpdated);
        when(organizationProviderService.update(any(OrganizationProvider.class))).thenReturn(organizationProviderThreeUpdated);

        mockMvc.perform(MockMvcRequestBuilders.put("/organization-providers/{id}", organizationProviderThreeUpdated.getId())
                .content(mappedToStringObject)
                .contentType(mediaType))
                .andExpect(status().isOk())
                .andDo(print());

        verify(organizationProviderService).update(any(OrganizationProvider.class));

    }


}
