package ru.it2g.kis.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.it2g.kis.converter.ContractorToDtoConverter;
import ru.it2g.kis.dto.ContractorDto;
import ru.it2g.kis.entity.dictionary.Contractor;
import ru.it2g.kis.repository.ContractorRepository;
import ru.it2g.kis.service.ContractorService;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.hamcrest.Matchers.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Тест для REST контроллера Контрагента.
 *
 * @author ZotovES on 05.12.2017
 */
@ActiveProfiles(profiles = "dev")
@RunWith(SpringRunner.class)
@WebMvcTest({ContractorController.class, ContractorToDtoConverter.class})
public class ContractorControllerTest {

    @Autowired
    private WebApplicationContext context;
    @Autowired
    private ObjectMapper mapper;

    private MediaType contentType = new MediaType(
            MediaType.APPLICATION_JSON_UTF8.getType(),
            MediaType.APPLICATION_JSON_UTF8.getSubtype(),
            MediaType.APPLICATION_JSON_UTF8.getCharset());


    private MockMvc mockMvc;
    private ArrayList<Contractor> contractors;

    @MockBean
    private ContractorService contractorService;

    @MockBean
    private ContractorRepository contractorRepository;

    @Autowired
    private ContractorToDtoConverter contractorToDtoConverter;

    /**
     * Инициализация  mock объектов.
     *
     * @throws Exception Пробрасываем все исключения.
     */
    @Before
    public void setUp() throws Exception {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
        this.contractors = new ArrayList<>();
        this.contractors.add(Contractor.builder().id(1).name("1").active(true).build());
        this.contractors.add(Contractor.builder().id(2).name("2").active(true).build());
        this.contractors.add(Contractor.builder().id(3).name("3").active(false).build());
    }

    /**
     * Тестирование метода чтения всех контрагентов.
     *
     * @throws Exception Пробрасываем все исключения.
     */
    @Test
    public void getContractorsTest() throws Exception {
        when(contractorService.getAll()).thenReturn(this.contractors);

        ResultActions result = mockMvc.perform(get("/contractor/"));
        result.andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(this.contractors.size())))
                .andExpect(jsonPath("$[0].id", is((int) this.contractors.get(0).getId())))
                .andExpect(jsonPath("$[0].name", is(this.contractors.get(0).getName())))
                .andExpect(jsonPath("$[1].id", is((int) this.contractors.get(1).getId())))
                .andExpect(jsonPath("$[1].name", is(this.contractors.get(1).getName())))
                .andDo(print());

        verify(contractorService).getAll();
    }

    /**
     * Тестирование получения только активных контрагентов.
     *
     * @throws Exception Пробрасываем все исключения.
     */
    @Test
    public void getWorkingContractorsTest() throws Exception {
        List<Contractor> workingContractors = this.contractors.stream()
                .filter(Contractor::isActive)
                .collect(Collectors.toList());
        Contractor notActiveContractor = this.contractors.stream()
                .filter(p -> !p.isActive())
                .findFirst()
                .orElse(null);
        when(contractorService.getByActiveTrue()).thenReturn(workingContractors);

        if (notActiveContractor == null)
            throw new NullPointerException("В списке должен быть хотябы один не активный конрагент");
        ResultActions result = mockMvc.perform(get("/contractor/working"));
        result.andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(workingContractors.size())))
                .andExpect(jsonPath("$[0].id", not((int) notActiveContractor.getId())))
                .andExpect(jsonPath("$[0].name", not(notActiveContractor.getName())))
                .andExpect(jsonPath("$[1].id", not(notActiveContractor.getId())))
                .andExpect(jsonPath("$[1].name", not(notActiveContractor.getName())))
                .andDo(print());

        verify(contractorService).getByActiveTrue();
    }

    /**
     * Проверка метода возврата контрагента по идентификатору
     *
     * @throws Exception Пробрасываем все исключения.
     */
    @Test
    public void getContractorByIdTest() throws Exception {
        Contractor contractor = this.contractors.get(1);

        when(contractorService.getById(contractor.getId())).thenReturn(contractor);

        mockMvc.perform(get("/contractor/{id}", contractor.getId())
                .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("id", is((int) contractor.getId())))
                .andExpect(jsonPath("name", is(contractor.getName())))
                .andDo(print());

        verify(contractorService).getById(contractor.getId());

    }

    @Test
    public void notFoundContractorByIdTest() throws Exception {
        Contractor contractor = this.contractors.get(1);

        when(contractorService.getById(contractor.getId())).thenReturn(null);

        mockMvc.perform(get("/contractor/{id}", contractor.getId())
                .contentType(contentType))
                .andExpect(status().isNotFound())
                .andDo(print());

        verify(contractorService).getById(contractor.getId());

    }

    /**
     * Тестирование удаления контрагента по идентификатору.
     *
     * @throws Exception Пробрасываем все исключения.
     */
    @Test
    public void deleteContractorByIdTest() throws Exception {
        Contractor contractor = this.contractors.get(1);

        when(contractorRepository.exists(contractor.getId())).thenReturn(true);

        mockMvc.perform(delete("/contractor/{id}", contractor.getId())
                .contentType(contentType))
                .andExpect(status().isNoContent())
                .andDo(print());

        verify(contractorService).remove(contractor.getId());

    }

    /**
     * Тестирование удаления контрагента по несуществующему идентификатору.
     *
     * @throws Exception Пробрасываем все исключения.
     */
    @Test
    public void deleteContractorByIdTestNotFoundTest() throws Exception {
        Contractor contractor = this.contractors.get(1);

        when(contractorRepository.exists(contractor.getId())).thenReturn(false);

        mockMvc.perform(delete("/contractor/{id}", contractor.getId())
                .contentType(contentType))
                .andExpect(status().isNotFound())
                .andDo(print());

        verify(contractorService, never()).remove(contractor.getId());

    }

    /**
     * Тестирование добавления нового экземпляра контрагента.
     *
     * @throws Exception Пробрасываем все исключения
     */
    @Test
    public void createContractorTest() throws Exception {
        Contractor contractor = this.contractors.get(1);
        ContractorDto contractorDto = ContractorDto.builder().name("test").build();
        String json = mapper.writeValueAsString(contractorDto);

        when(contractorService.save(any(Contractor.class))).thenReturn(contractor);

        mockMvc.perform(post("/contractor")
                .content(json)
                .contentType(contentType))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("name", is(contractor.getName())))
                .andDo(print());

        verify(contractorService).save(any(Contractor.class));
    }

    /**
     * Тестирование редактирования контрагента.
     * @throws Exception Пробрасываем все исключения.
     */
    @Test
    public void updateContractorTest() throws Exception {
        Contractor contractor = this.contractors.get(1);
        ContractorDto contractorDto = contractorToDtoConverter.convert(contractor);
        String json = mapper.writeValueAsString(contractorDto);

        when(contractorRepository.exists(contractor.getId())).thenReturn(true);
        when(contractorService.save(any(Contractor.class))).thenReturn(contractor);

        mockMvc.perform(put("/contractor/{id}", contractor.getId())
                .content(json)
                .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonPath("name", is(contractor.getName())))
                .andDo(print());

        verify(contractorRepository).exists(contractor.getId());
        verify(contractorService).save(any(Contractor.class));
    }

    /**
     * Тестирование редактирования контрагента
     * с несовпадающими идентификаторами в параметре запроса и теле json.
     * @throws Exception Пробрасываем все исключения.
     */
    @Test
    public void updateContractorConflictIdTest() throws Exception {
        Contractor contractor = this.contractors.get(1);
        ContractorDto contractorDto = ContractorDto.builder()
                .id(contractor.getId()+1)
                .name("test").build();
        String json = mapper.writeValueAsString(contractorDto);

        when(contractorRepository.exists(contractor.getId())).thenReturn(false);
        when(contractorService.save(any(Contractor.class))).thenReturn(contractor);

        mockMvc.perform(put("/contractor/{id}", contractor.getId())
                .content(json)
                .contentType(contentType))
                .andExpect(status().isConflict())
                .andDo(print());

        verify(contractorService, never()).save(contractor);
        verify(contractorRepository, never()).exists(contractor.getId());
    }

    /**
     * Тестирование редактирования контрагента по несуществующему идентификатору.
     * @throws Exception Пробрасываем все исключения.
     */
    @Test
    public void updateContractorNotFoundIdTest() throws Exception {
        Contractor contractor = this.contractors.get(1);
        ContractorDto contractorDto = contractorToDtoConverter.convert(contractor);
        String json = mapper.writeValueAsString(contractorDto);

        when(contractorRepository.exists(contractor.getId())).thenReturn(false);
        when(contractorService.save(any(Contractor.class))).thenReturn(contractor);

        mockMvc.perform(put("/contractor/{id}", contractor.getId())
                .content(json)
                .contentType(contentType))
                .andExpect(status().isNotFound())
                .andDo(print());

        verify(contractorRepository).exists(contractor.getId());
        verify(contractorService, never()).save(contractor);
    }

}