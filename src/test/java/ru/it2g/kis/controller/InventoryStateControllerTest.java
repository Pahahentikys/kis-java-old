package ru.it2g.kis.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.it2g.kis.converter.InventoryStateToDtoConverter;
import ru.it2g.kis.dto.InventoryStateDto;
import ru.it2g.kis.entity.InventoryState;
import ru.it2g.kis.repository.InventoryStateRepository;
import ru.it2g.kis.service.InventoryStateService;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.hamcrest.Matchers.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Тест для REST контроллера Статус ТМЦ.
 *
 * @author Created by ZotovES on 25.03.2018
 */
@ActiveProfiles(profiles = "dev")
@RunWith(SpringRunner.class)
@WebMvcTest({InventoryStateController.class, InventoryStateToDtoConverter.class})
public class InventoryStateControllerTest {
    @Autowired
    private WebApplicationContext context;
    @Autowired
    private ObjectMapper mapper;
    @Autowired
    private InventoryStateToDtoConverter inventoryStateToDtoConverter;
    @MockBean
    private InventoryStateService inventoryStateService;
    @MockBean
    private InventoryStateRepository inventoryStateRepository;

    private MockMvc mockMvc;
    private ArrayList<InventoryState> inventoryStates;

    private MediaType contentType = new MediaType(
            MediaType.APPLICATION_JSON_UTF8.getType(),
            MediaType.APPLICATION_JSON_UTF8.getSubtype(),
            MediaType.APPLICATION_JSON_UTF8.getCharset()
    );

    /**
     * Инициализация mock объектов.
     */
    @Before
    public void setUp() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
        this.inventoryStates = new ArrayList<>();
        this.inventoryStates.add(InventoryState.builder().id(1).name("test state1").active(true).build());
        this.inventoryStates.add(InventoryState.builder().id(2).name("test state2").active(true).build());
        this.inventoryStates.add(InventoryState.builder().id(3).name("test state3").active(false).build());
    }

    /**
     * Тестирование метода чтения всех статусов ТМЦ.
     *
     * @throws Exception Пробрасываем все исключения.
     */
    @Test
    public void getInventoryStatesTest() throws Exception {
        when(inventoryStateService.getAll()).thenReturn(inventoryStates);

        ResultActions resultActions = mockMvc.perform(get("/inventory-states/"));
        resultActions.andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(this.inventoryStates.size())))
                .andExpect(jsonPath("$[0].id", is((int) this.inventoryStates.get(0).getId())))
                .andExpect(jsonPath("$[0].name", is(this.inventoryStates.get(0).getName())))
                .andExpect(jsonPath("$[1].id", is((int) this.inventoryStates.get(1).getId())))
                .andExpect(jsonPath("$[1].name", is(this.inventoryStates.get(1).getName())))
                .andDo(print());

        verify(inventoryStateService).getAll();
    }

    /**
     * Тестирование метода поиска активных статусов ТМЦ.
     *
     * @throws Exception Пробрасываем все исключения.
     */
    @Test
    public void getActiveInventoryStatesTest() throws Exception {
        List<InventoryState> activeInventoryState = this.inventoryStates.stream()
                .filter(InventoryState::isActive)
                .collect(Collectors.toList());

        InventoryState nonActiveInventorySate = this.inventoryStates.stream()
                .filter(s -> !s.isActive())
                .findFirst()
                .orElseThrow(() ->
                        new IllegalArgumentException("В списке должен быть хотя бы один не активный статус")
                );

        when(inventoryStateService.getByActiveTrue()).thenReturn(activeInventoryState);

        ResultActions resultActions = mockMvc.perform(get("/inventory-states/active"));
        resultActions.andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(activeInventoryState.size())))
                .andExpect(jsonPath("$[0].id", not((int) nonActiveInventorySate.getId())))
                .andExpect(jsonPath("$[0].name", not(nonActiveInventorySate.getName())))
                .andExpect(jsonPath("$[1].id", not((int) nonActiveInventorySate.getId())))
                .andExpect(jsonPath("$[1].name", not(nonActiveInventorySate.getName())))
                .andDo(print());

        verify(inventoryStateService).getByActiveTrue();
    }

    /**
     * Тестирование поиска статуса тмц по идентификатору.
     *
     * @throws Exception Пробрасываем все исключения.
     */
    @Test
    public void getInventoryStatesByIdTest() throws Exception {
        InventoryState inventoryState = this.inventoryStates.get(1);

        when(inventoryStateService.getById(inventoryState.getId())).thenReturn(inventoryState);

        ResultActions resultActions = mockMvc.perform(get("/inventory-states/{id}", inventoryState.getId()));
        resultActions.andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("id", is((int) inventoryState.getId())))
                .andExpect(jsonPath("name", is(inventoryState.getName())))
                .andDo(print());

        verify(inventoryStateService).getById(inventoryState.getId());
    }

    /**
     * Тестирование поиска не существующей статуса тмц по идентификатору.
     *
     * @throws Exception Пробрасываем все исключения.
     */
    @Test
    public void notFoundGetInventoryStatesByIdTest() throws Exception {
        InventoryState inventoryState = this.inventoryStates.get(1);

        when(inventoryStateService.getById(inventoryState.getId())).thenReturn(null);

        mockMvc.perform(get("/inventory-states/{id}", inventoryState.getId())
                .contentType(contentType))
                .andExpect(status().isNotFound())
                .andDo(print());

        verify(inventoryStateService).getById(inventoryState.getId());
    }

    /**
     * Тестирование создания нового статуса ТМЦ.
     *
     * @throws Exception Пробрасываем все исключения.
     */
    @Test
    public void createInventoryStateTest() throws Exception {
        InventoryState inventoryState = this.inventoryStates.get(1);
        InventoryStateDto inventoryStateDto = inventoryStateToDtoConverter.convert(inventoryState);

        String json = mapper.writeValueAsString(inventoryStateDto);

        when(inventoryStateService.create(inventoryState.getName())).thenReturn(inventoryState);

        mockMvc.perform(post("/inventory-states")
                .content(json)
                .contentType(contentType))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("name", is(inventoryState.getName())))
                .andDo(print());

        verify(inventoryStateService).create(inventoryState.getName());
    }

    /**
     * Тестирование метода редактирования статуса ТМЦ.
     *
     * @throws Exception Пробрасываем все исключения.
     */
    @Test
    public void updateInventoryStateTest() throws Exception {
        InventoryState inventoryState = this.inventoryStates.get(1);
        InventoryStateDto inventoryStateDto = inventoryStateToDtoConverter.convert(inventoryState);

        String json = mapper.writeValueAsString(inventoryStateDto);

        when(inventoryStateRepository.exists(inventoryState.getId())).thenReturn(true);
        when(inventoryStateService.save(any(InventoryState.class))).thenReturn(inventoryState);

        mockMvc.perform(put("/inventory-states/{id}", inventoryState.getId())
                .content(json)
                .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonPath("name", is(inventoryState.getName())))
                .andDo(print());

        verify(inventoryStateRepository).exists(inventoryState.getId());
        verify(inventoryStateService).save(any(InventoryState.class));
    }

    /**
     * Тестирование метода редактирования статуса ТМЦ
     * c не совпадающим идентификатором в параметре запроса и теле json.
     *
     * @throws Exception Пробрасываем все исключения.
     */
    public void updateInventoryStateTestConflictIdTest() throws Exception {
        InventoryState inventoryState = this.inventoryStates.get(1);
        InventoryStateDto inventoryStateDto = InventoryStateDto.builder()
                .id(inventoryState.getId() + 1)
                .name("test state")
                .build();

        String json = mapper.writeValueAsString(inventoryStateDto);

        when(inventoryStateRepository.exists(inventoryState.getId())).thenReturn(true);
        when(inventoryStateService.save(any(InventoryState.class))).thenReturn(inventoryState);

        mockMvc.perform(post("/inventory-states/{id}", inventoryState.getId())
                .content(json)
                .contentType(contentType))
                .andExpect(status().isConflict())
                .andDo(print());

        verify(inventoryStateRepository).exists(inventoryState.getId());
        verify(inventoryStateService).save(any(InventoryState.class));
    }

    /**
     * Тестирование метода редактирования статуса ТМЦ
     * c не несуществующим  идентификатором.
     *
     * @throws Exception Пробрасываем все исключения.
     */
    public void updateInventoryStateNotFoundTest() throws Exception {
        InventoryState inventoryState = this.inventoryStates.get(1);
        InventoryStateDto inventoryStateDto = inventoryStateToDtoConverter.convert(inventoryState);

        String json = mapper.writeValueAsString(inventoryStateDto);

        when(inventoryStateRepository.exists(inventoryState.getId())).thenReturn(false);
        when(inventoryStateService.save(any(InventoryState.class))).thenReturn(inventoryState);

        mockMvc.perform(post("/inventory-states/{id}", inventoryState.getId())
                .content(json)
                .contentType(contentType))
                .andExpect(status().isNotFound())
                .andDo(print());

        verify(inventoryStateRepository).exists(inventoryState.getId());
        verify(inventoryStateService).save(any(InventoryState.class));
    }

    /**
     * Тестирование метода удаления статуса ТМЦ по идентификатору.
     *
     * @throws Exception Пробрасываем все исключения.
     */
    @Test
    public void deleteInventoryStateTest() throws Exception {
        InventoryState inventoryState = inventoryStates.get(1);

        when(inventoryStateRepository.exists(inventoryState.getId())).thenReturn(true);

        mockMvc.perform(delete("/inventory-states/{id}", inventoryState.getId())
                .contentType(contentType))
                .andExpect(status().isNoContent())
                .andDo(print());

        verify(inventoryStateRepository).exists(inventoryState.getId());
        verify(inventoryStateService).remove(inventoryState.getId());
    }

    /**
     * Тестирование метода удаления группы ТМЦ по идентификатору.
     * с не существующим идентификатором.
     *
     * @throws Exception Пробрасываем все исключения.
     */
    @Test
    public void deleteInventoryStateNotFoundTest() throws Exception {
        InventoryState inventoryState = inventoryStates.get(1);

        when(inventoryStateRepository.exists(inventoryState.getId())).thenReturn(false);

        mockMvc.perform(delete("/inventory-states/{id}", inventoryState.getId())
                .contentType(contentType))
                .andExpect(status().isNotFound())
                .andDo(print());

        verify(inventoryStateRepository).exists(inventoryState.getId());
        verify(inventoryStateService, never()).remove(inventoryState.getId());
    }
}