package ru.it2g.kis.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.it2g.kis.converter.InventoryCategoryToDTOConverter;
import ru.it2g.kis.dto.InventoryCategoryDTO;
import ru.it2g.kis.entity.dictionary.InventoryCategory;
import ru.it2g.kis.service.InventoryCategoryService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Collectors;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * @author Pahahentikys on 10.04.2018
 * Тест REST-контроллера сущности "категория товаров".
 */
@ActiveProfiles(profiles = "dev")
@RunWith(SpringRunner.class)
@WebMvcTest({InventoryCategoryController.class, InventoryCategoryToDTOConverter.class})
public class InventoryCategoryControllerTest {

    /**
     * Инъекция MockMvc.
     */
    @Autowired
    private MockMvc mockMvc;

    /**
     * Инъекция контекста.
     */
    @Autowired
    private WebApplicationContext webApplicationContext;

    /**
     * Мок на сервис сущности "категория товаров".
     */
    @MockBean
    private InventoryCategoryService inventoryCategoryService;

    /**
     * Инъекция DTO-конвертора.
     */
    @Autowired
    private InventoryCategoryToDTOConverter inventoryCategoryToDTOConverter;

    /**
     * Тип контента.
     */
    private final String CONTENT_TYPE = "application/json;charset=UTF-8";

    /**
     * Инъекция маппера.
     */
    @Autowired
    private ObjectMapper objectMapper;

    /**
     * Коллекция тестовых категорий.
     */
    private Collection<InventoryCategory> inventoryCategories;

    /**
     * Тестовые категории.
     */
    private InventoryCategory inventoryCategoryOne;
    private InventoryCategory inventoryCategoryTwo;
    private InventoryCategory inventoryCategoryThree;
    private InventoryCategory inventoryCategoryFour;

    /**
     * Инициализация тестовых данных.
     */
    @Before
    public void initTestData() {

        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();

        inventoryCategoryOne = InventoryCategory.builder()
                .id(1L)
                .name("ParentCategoryOne")
                .active(true)
                .build();

        inventoryCategoryTwo = InventoryCategory.builder()
                .id(2L)
                .name("ParentCategoryTwo")
                .active(true)
                .build();

        inventoryCategoryThree = InventoryCategory.builder()
                .id(3L)
                .name("CategoryThree")
                .active(true)
                .parent(inventoryCategoryOne)
                .build();

        inventoryCategoryFour = InventoryCategory.builder()
                .id(4L)
                .name("CategoryFour")
                .active(false)
                .parent(inventoryCategoryOne)
                .build();

        inventoryCategories = new ArrayList<>();
        inventoryCategories.add(inventoryCategoryThree);
        inventoryCategories.add(inventoryCategoryOne);
        inventoryCategories.add(inventoryCategoryTwo);
        inventoryCategories.add(inventoryCategoryFour);

    }

    /**
     * Тестирование метода получения всех (активных и неактивных) категорий товаров.
     *
     * @throws Exception - проброс исключений.
     */
    @Test
    public void getAll() throws Exception {

        when(inventoryCategoryService.getAll()).thenReturn(inventoryCategories);

        mockMvc.perform(MockMvcRequestBuilders.get("/inventory-categories"))
                .andExpect(content().contentType(CONTENT_TYPE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(4)))
                .andDo(print());

        verify(inventoryCategoryService).getAll();
    }

    /**
     * Тестирование метода получениея категории по идентификатору.
     *
     * @throws Exception - проброс исключений.
     */
    @Test
    public void getById() throws Exception {

        when(inventoryCategoryService.getById(1L)).thenReturn(inventoryCategoryOne);

        mockMvc.perform(MockMvcRequestBuilders.get("/inventory-categories/{id}", 1L))
                .andExpect(content().contentType(CONTENT_TYPE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(inventoryCategoryOne.getId()))
                .andDo(print());

        verify(inventoryCategoryService).getById(1L);

    }

    /**
     * Тестирование метода "мягкого" удаления категории товаров по идентификатору.
     *
     * @throws Exception - проброс исключений.
     */
    @Test
    public void deleteById() throws Exception {

        when(inventoryCategoryService.getById(2L)).thenReturn(inventoryCategoryTwo);

        mockMvc.perform(MockMvcRequestBuilders.delete("/inventory-categories/{id}", 2L)
                .contentType(CONTENT_TYPE))
                .andExpect(status().isNoContent())
                .andDo(print());

        verify(inventoryCategoryService).remove(2L);
    }

    /**
     * Тестирование метода получения только активных категорий товаров.
     *
     * @throws Exception - проброс исключений.
     */
    @Test
    public void getByActive() throws Exception {

        when(inventoryCategoryService.getByActiveTrue()).thenReturn(inventoryCategories.stream()
                .filter(InventoryCategory::isActive)
                .collect(Collectors.toList())
        );

        mockMvc.perform(MockMvcRequestBuilders.get("/inventory-categories/active"))
                .andExpect(content().contentType(CONTENT_TYPE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(3)))
                .andDo(print());

        verify(inventoryCategoryService).getByActiveTrue();
    }

    /**
     * Тестирование метода создания новой категории товаров.
     *
     * @throws Exception - проброс исключений.
     */
    @Test
    public void create() throws Exception {

        InventoryCategory inventoryCategory = InventoryCategory.builder()
                .name("NewCategory")
                .build();

        InventoryCategoryDTO returnDTO = inventoryCategoryToDTOConverter.convert(inventoryCategory);

        String mappedObject = objectMapper.writeValueAsString(returnDTO);

        when(inventoryCategoryService.create(any(InventoryCategory.class))).thenReturn(inventoryCategory);

        mockMvc.perform(MockMvcRequestBuilders.post("/inventory-categories")
                .contentType(CONTENT_TYPE)
                .content(mappedObject))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.name").value(inventoryCategory.getName()))
                .andDo(print());

        verify(inventoryCategoryService).create(any(InventoryCategory.class));

    }

    /**
     * Тестирование обновления информации о категории товаров.
     *
     * @throws Exception - проброс исключений.
     */
    @Test
    public void update() throws Exception {

        InventoryCategory inventoryCategoryUpd = InventoryCategory.builder()
                .id(inventoryCategoryOne.getId())
                .name("UpdatedName")
                .build();

        InventoryCategoryDTO returnDTO = inventoryCategoryToDTOConverter.convert(inventoryCategoryUpd);

        String mappedObject = objectMapper.writeValueAsString(returnDTO);

        when(inventoryCategoryService.getById(1L)).thenReturn(inventoryCategoryUpd);
        when(inventoryCategoryService.update(any(InventoryCategory.class))).thenReturn(inventoryCategoryUpd);

        mockMvc.perform(MockMvcRequestBuilders.put("/inventory-categories", 1L)
                .contentType(CONTENT_TYPE)
                .content(mappedObject))
                .andExpect(status().isOk())
                .andDo(print());

        verify(inventoryCategoryService).update(any(InventoryCategory.class));
    }

    /**
     * Тестирование метода поиска по названию категории товаров.
     *
     * @throws Exception - проброс исключений.
     */
    @Test
    public void searchByCategoryName() throws Exception {

        when(inventoryCategoryService.findByName("pare"))
                .thenReturn(Arrays.asList(inventoryCategoryOne, inventoryCategoryTwo));

        mockMvc.perform(MockMvcRequestBuilders.get("/inventory-categories/search")
                .param("category_name", "pare"))
                .andExpect(content().contentType(CONTENT_TYPE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andDo(print());

        verify(inventoryCategoryService).findByName(anyString());
    }

    /**
     * Тестирование поиска категории товаров по идентификатору родительской категории.
     *
     * @throws Exception - проброс исключений.
     */
    @Test
    public void searchByParentId() throws Exception {

        when(inventoryCategoryService.findByParent(1L))
                .thenReturn(Arrays.asList(inventoryCategoryThree, inventoryCategoryFour));

        mockMvc.perform(MockMvcRequestBuilders.get("/inventory-categories/search")
                .param("parent_id", "1"))
                .andExpect(content().contentType(CONTENT_TYPE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andDo(print());

        verify(inventoryCategoryService).findByParent(anyLong());
    }


}
