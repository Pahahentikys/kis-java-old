package ru.it2g.kis.repository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;
import ru.it2g.kis.entity.InventoryType;

import java.util.ArrayList;
import java.util.Collection;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Тестирование репозитория Типа ТМЦ.
 *
 * @author Created by ZotovES on 05.03.2018
 */
@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class InventoryTypeRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private InventoryTypeRepository inventoryTypeRepository;

    private ArrayList<InventoryType> inventoryTypes;

    /**
     * Инициализация тестовых данных.
     *
     * @throws Exception Пробрасываем все исключения.
     */
    @Before
    public void setUp() throws Exception {
        this.inventoryTypes = new ArrayList<>();

        this.inventoryTypes.add(InventoryType.builder().name("1").active(true).build());
        this.inventoryTypes.add(InventoryType.builder().name("2").active(true).build());
        this.inventoryTypes.add(InventoryType.builder().name("3").active(false).build());

        inventoryTypes.forEach(i -> entityManager.persist(i));
    }

    /**
     * Тестирование поиска всех активных типов ТМЦ.
     *
     * @throws Exception Пробрасываем все исключения
     */
    @Test
    public void findByActiveTrueTest() throws Exception {
        Collection<InventoryType> inventoryTypeReturned = inventoryTypeRepository.findByActiveTrue();
        assertThat(inventoryTypeReturned)
                .as("Not null returned").isNotNull()
                .as("All 3 order").hasSize(3);

        assertThat(inventoryTypeReturned.stream()
                .allMatch(InventoryType::isActive))
                .isTrue();
    }

    /**
     * Тестирование вывода всех типов ТМЦ вне зависимости от статуса активности.
     *
     * @throws Exception Пробрасываем все исключения.
     */
    @Test
    public void findAllTest() throws Exception {
        assertThat(inventoryTypeRepository.findAll())
                .as("Not null returned").isNotNull()
                .as("All 5 order").hasSize(5);
    }
}