package ru.it2g.kis.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import ru.it2g.kis.entity.dictionary.InventoryCategory;

import java.util.Collection;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE;


/**
 * @author Pahahentikys on 10.04.2018
 * Тест репозитория сущности "категория товаров" (InventoryCategory).
 */
@RunWith(value = SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = NONE)
public class InventoryCategoryRepositoryTest {

    /**
     * Инъекция репозитория для сущности "категория товаров".
     */
    @Autowired
    private InventoryCategoryRepository inventoryCategoryRepository;

    /**
     * Тестирование метода поиска всех (активных и неактивных) категорий товаров.
     */
    @Test
    public void getAll() {

        Collection<InventoryCategory> inventoryCategories = inventoryCategoryRepository.findAll();

        assertThat(inventoryCategories)
                .hasSize(5);
    }

    /**
     * Тестирование метода поиска активных категорий.
     */
    @Test
    public void findByActiveTrue() {

        Collection<InventoryCategory> inventoryCategories = inventoryCategoryRepository.findByActiveTrue();

        assertThat(inventoryCategories)
                .hasSize(4);
    }

    /**
     * Тестирование метода поиска категорий товаров с наличием идентификатора вышестоящей категории.
     */
    @Test
    public void findByParentId() {

        long serachParam = 1L;

        Collection<InventoryCategory> inventoryCategories = inventoryCategoryRepository
                .findByParent_IdAndActiveTrue(serachParam);

        assertThat(inventoryCategories)
                .hasSize(3);

    }

    /**
     * Тестирование метода поиска категорий, начинающихся с комбинации верных символов.
     */
    @Test
    public void validSearchByCategoryName() {

        String searchCategoryParam = "М";

        Collection<InventoryCategory> inventoryCategories = inventoryCategoryRepository
                .findByNameStartingWithIgnoreCaseAndActiveTrue(searchCategoryParam);

        assertThat(inventoryCategories)
                .hasSize(2);

    }

    /**
     * Тестирование метода поиска категорий, начинающихся с комбинации символов, по котороым категории отсутствуют.
     */
    @Test
    public void invalidSearchCategoryByName() {

        String invalidSearchParam = "ры";

        Collection<InventoryCategory> inventoryCategories = inventoryCategoryRepository
                .findByNameStartingWithIgnoreCaseAndActiveTrue(invalidSearchParam);

        assertThat(inventoryCategories)
                .isEmpty();
    }


}
