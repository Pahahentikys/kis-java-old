package ru.it2g.kis.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import ru.it2g.kis.entity.dictionary.Contractor;
import ru.it2g.kis.entity.dictionary.Supplier;

import static org.junit.Assert.*;
import static org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE;

/**
 * Created by RyabushevSA on 16.11.2017
 */
@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = NONE)
public class SupplierRepositoryTest {

    @Autowired
    private SupplierRepository supplierRepository;

    @Autowired
    private ContractorRepository contractorRepository;

    @Test
    public void test1() throws Exception {
        Contractor contractor = Contractor.builder()
                .id(0L)
                .name("Name1")
                .active(false)
                .build();

        Supplier supplier = Supplier.builder()
                .id(0L)
                .contractor(contractor)
                .active(true)
                .build();

        contractorRepository.save(contractor);
        Supplier save = supplierRepository.save(supplier);
        assertNotNull(save);
    }
}