package ru.it2g.kis.repository;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;
import ru.it2g.kis.entity.Inventory;
import ru.it2g.kis.entity.ReceiptOrder;
import ru.it2g.kis.entity.dictionary.OrderType;
import ru.it2g.kis.entity.dictionary.Organization;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = NONE)
public class ReceiptOrderRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private ReceiptOrderRepository receiptOrderRepository;

    @Autowired
    private InventoryRepository inventoryRepository;

    private Organization organization1;

    @Before
    public void initData() {
        organization1 = Organization.builder().name("Test Organisation 1").build();
        Organization organization2 = Organization.builder().name("Test Organisation 2").build();

        ReceiptOrder receiptOrder1 = ReceiptOrder.builder()
                .name("Test 1")
                .dateCreate(new Date())
                .expenseDocumentNumber("123456")
                .expenseOrganization(organization1)
                .build();

        ReceiptOrder receiptOrder2 = ReceiptOrder.builder()
                .name("Test 2")
                .dateCreate(new Date())
                .expenseDocumentNumber("999")
                .expenseOrganization(organization1)
                .build();

        ReceiptOrder receiptOrder3 = ReceiptOrder.builder()
                .name("Test 3")
                .dateCreate(new Date())
                .expenseDocumentNumber("789456132")
                .expenseOrganization(organization2)
                .build();

        entityManager.persist(organization1);
        entityManager.persist(organization2);

        entityManager.persist(receiptOrder1);
        entityManager.persist(receiptOrder2);
        entityManager.persist(receiptOrder3);
    }

    @Test
    public void selectAllTest() throws Exception {
        assertThat(receiptOrderRepository.findAll())
                .as("Not null returned").isNotNull()
                .as("All 3 order").hasSize(3);

    }

    @Test
    public void findByOrganisation() throws Exception {
        Collection<ReceiptOrder> receiptOrders = receiptOrderRepository.findByExpenseOrganization(organization1);

        assertThat(receiptOrders)
                .as("Not null returned").isNotNull()
                .as("All 2 order").hasSize(2);

        assertThat(receiptOrders.stream()
                .allMatch(o -> o.getExpenseOrganization() == organization1))
                .isTrue();
    }

    @Test
    public void addInventoryTools() throws Exception {
        ReceiptOrder receiptOrder = ReceiptOrder.builder()
                .name("Test 1")
                .dateCreate(new Date())
                .build();

        Inventory inventory = Inventory.builder()
                .name("TestInvent")
                .receiptOrder(receiptOrder)
                .build();

        receiptOrder.setInventories(new ArrayList<>());
        receiptOrder.getInventories().add(inventory);

        receiptOrderRepository.save(receiptOrder);
        inventoryRepository.save(inventory);

        assertNotNull(inventory.getReceiptOrder());

        ReceiptOrder saved = receiptOrderRepository.findOne(receiptOrder.getId());


        assertEquals(1, saved.getInventories().size());
        assertEquals(1, receiptOrder.getInventories().size());

    }

    @Test
    public void findByOrderTypeId() throws Exception {

        OrderType orderTypeOne = OrderType.builder()
                .id(1L)
                .type("новый")
                .build();

        ReceiptOrder receiptOrderTest = ReceiptOrder.builder()
                .name("Test 4")
                .dateCreate(new Date())
                .expenseDocumentNumber("8800553535")
                .orderType(orderTypeOne)
                .build();

        entityManager.persistAndFlush(receiptOrderTest);

        Collection<ReceiptOrder> receiptOrders = receiptOrderRepository.findByOrderTypeId(orderTypeOne.getId());

        Assertions.assertThat(receiptOrders).isNotEmpty();

    }
}