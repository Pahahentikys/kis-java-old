package ru.it2g.kis.repository.lesson10;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import ru.it2g.kis.entity.dictionary.Organization;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import static org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE;

/**
 * Created by RyabushevSA on 09.11.2017
 */
@ActiveProfiles("local")
@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = NONE)
public class repositoryJpql {

    @Autowired
    private EntityManager entityManager;

    @Test
    public void test() throws Exception {
        Organization q1 = Organization.builder().name("Test 1").build();
        Organization q2 = Organization.builder().name("Test 2").build();
        Organization q3 = Organization.builder().name("Test 3").build();

        entityManager.persist(q1);
        entityManager.persist(q2);
        entityManager.persist(q3);

        q3.setName("TEST 500");
        entityManager.merge(q3);

        String jpql = "select o from Organization o join fetch lkfjldkfjl";

        Query query = entityManager.createQuery(jpql, Organization.class);
        System.out.println(query.getResultList());

        entityManager.createQuery("delete from Organization")
                .executeUpdate();
    }

}
