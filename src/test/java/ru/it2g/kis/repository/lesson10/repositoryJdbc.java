package ru.it2g.kis.repository.lesson10;

import org.junit.Test;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.test.context.ActiveProfiles;
import ru.it2g.kis.entity.dictionary.Organization;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by RyabushevSA on 09.11.2017
 */
@ActiveProfiles("dev")
public class repositoryJdbc {

    @Test
    public void test() throws Exception {
        Connection connection = null;

        String url = "jdbc:postgresql://127.0.0.1:5432/kis-local";
        String name = "kis";
        String password = "kis";

        try {
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection(url, name, password);
            Statement statement = connection.createStatement();

            statement.executeUpdate("INSERT INTO inventory.s_organisation(id, name) VALUES(1, 'тест 1')");
            statement.executeUpdate("INSERT INTO inventory.s_organisation(id, name) VALUES(2, 'тест 2')");
            statement.executeUpdate("INSERT INTO inventory.s_organisation(id, name) VALUES(3, 'тест 3')");

            statement.executeUpdate("UPDATE inventory.s_organisation SET name = 'TEST 800' WHERE id = 3");

            ResultSet rs = statement.executeQuery("SELECT * FROM inventory.s_organisation");
            Mapper mapper = new Mapper();
            int rowNum = 0;
            List<Organization> organizations = new ArrayList<>();
            while (rs.next()) {
                organizations.add(mapper.mapRow(rs, rowNum++));
            }
            System.out.println(organizations);

            statement.executeUpdate("DELETE FROM inventory.s_organisation");

        } catch (Exception e) {
            System.err.println(e.getMessage());
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception e) {
                    System.err.println(e.getMessage());
                }
            }
        }
    }

    private class Mapper implements RowMapper<Organization> {

        @Override
        public Organization mapRow(ResultSet rs, int i) throws SQLException {
            Organization organization = new Organization();
            organization.setId(rs.getLong("id"));
            organization.setName(rs.getString("name"));

            return organization;
        }
    }
}
