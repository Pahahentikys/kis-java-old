package ru.it2g.kis.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;
import ru.it2g.kis.entity.InventoryState;

import java.util.Collection;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE;

/**
 * Тестировние репозитория статуса ТМЦ.
 *
 * @author Created by ZotovES on 25.03.2018
 */
@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = NONE)
public class InventoryStateRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;
    @Autowired
    private InventoryStateRepository inventoryStateRepository;

    /**
     * Тестирование метода поиска всех активных статусов.
     */
    @Test
    public void findByActiveTrueTest() {
        Collection<InventoryState> inventoryStates = inventoryStateRepository.findByActiveTrue();
        assertThat(inventoryStates)
                .as("Репозиторий должен вернуть хотя бы одну запись").isNotNull()
                .as("Кол-во записей должно быть 2").hasSize(2);

        assertThat(inventoryStates.stream()
                .allMatch(InventoryState::isActive))
                .as("Все статусы должны быть активны")
                .isTrue();
    }

    /**
     * Тестирование метода поиска всех статусов.
     */
    @Test
    public void findAllTest() {
        Collection<InventoryState> inventoryStates = inventoryStateRepository.findAll();
        assertThat(inventoryStates)
                .as("Репозиторий должен вернуть хотя бы одну запись").isNotNull()
                .as("Кол-во записей должно быть 3").hasSize(3);
    }
}