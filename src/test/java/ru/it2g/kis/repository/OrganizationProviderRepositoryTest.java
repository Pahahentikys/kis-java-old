package ru.it2g.kis.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;
import ru.it2g.kis.entity.dictionary.OrganizationProvider;

import java.util.Collection;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE;

/**
 * @author Pahahentikys on 09.02.2018
 *
 * Тест репозитория организации-поставщика (OrganizationProviderRepository).
 */
@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = NONE)
public class OrganizationProviderRepositoryTest {

    /**
     * Инъекция тестового менеджера сущностей.
     */
    @Autowired
    TestEntityManager testEntityManager;

    /**
     * Инъекция репозитория организации-поставщика.
     */
    @Autowired
    OrganizationProviderRepository organizationProviderRepository;

    /**
     * Тестирование метода поиска всех организаций-поставщиков.
     * @throws Exception - прокидываем исключения.
     */
    @Test
    public void findAll() throws Exception {
        Collection<OrganizationProvider> organizationProviders = organizationProviderRepository.findAll();
        assertThat(organizationProviders)
                .as("Not null returned").isNotNull()
                .as("One organization provider returned").hasSize(3);
    }

    /**
     * Тестирование метода поиска активный (active = true) организаций-поставщиков.
     * @throws Exception - прокидываем исключения.
     */
    @Test
    public void findByActiveTrue() throws Exception {
        Collection<OrganizationProvider> organizationProviders = organizationProviderRepository.findByActiveTrue();
        assertThat(organizationProviders)
                .as("Not null returned").isNotNull()
                .as("One organization provider returned").hasSize(1);
    }
}
