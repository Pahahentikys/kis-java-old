package ru.it2g.kis.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;
import ru.it2g.kis.entity.InventoryGroup;

import java.util.Collection;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE;

/**
 * Тестирование репозитория группы товаров.
 *
 * @author Created by ZotovES on 11.02.2018
 */
@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = NONE)
public class InventoryGroupRepositoryTest {
    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private InventoryGroupRepository inventoryGroupRepository;

    /**
     * Тестирование метода поиска всех активных групп.
     *
     * @throws Exception Пробрасываем все исключения.
     */
    @Test
    public void findByActiveTrueTest() throws Exception {
        Collection<InventoryGroup> inventoryGroupsReturned = inventoryGroupRepository.findByActiveTrue();
        assertThat(inventoryGroupsReturned)
                .as("Not null returned").isNotNull()
                .as("All 3 order").hasSize(2);

        assertThat(inventoryGroupsReturned.stream()
                .allMatch(InventoryGroup::isActive))
                .isTrue();
    }

    /**
     * Тестирование вывода всех групп.
     *
     * @throws Exception Пробрасываем все исключения.
     */
    @Test
    public void findAllTest() throws Exception {
        Collection<InventoryGroup> inventoryGroupsReturned = inventoryGroupRepository.findAll();
        assertThat(inventoryGroupsReturned)
                .as("Not null returned").isNotNull()
                .as("All 3 order").hasSize(3);
    }

}