package ru.it2g.kis.repository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;
import ru.it2g.kis.entity.dictionary.Contractor;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE;

/**
 * Тестирование репозитория Контрагентов.
 *
 * @author Created by ZotovES on 28.12.2017
 */
@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = NONE)
public class ContractorRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private ContractorRepository contractorRepository;

    private ArrayList<Contractor> contractors;

    /**
     * Инициализация  тестовых данных.
     *
     * @throws Exception Пробрасываем все исключения.
     */
    @Before
    public void setUp() throws Exception {
        this.contractors = new ArrayList<>();

        this.contractors.add(Contractor.builder().name("1").active(true).build());
        this.contractors.add(Contractor.builder().name("2").active(true).build());
        this.contractors.add(Contractor.builder().name("3").active(false).build());
        contractors.forEach(c -> entityManager.persist(c));
    }

    /**
     * Тестирование метода поиска всех контрагентов с датой окончания действия после переданной даты в параметре
     * или не заполненой датой окончания действия.
     *
     * @throws Exception Пробрасываем все исключения.
     */
    @Test
    public void findByDateEndAfterOrDateEndIsNull() throws Exception {
        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
        Date testDate = format.parse("28.12.2017");
        Collection<Contractor> contractorsReturned = contractorRepository.findByDateEndAfterOrDateEndIsNull(testDate);

        assertThat(contractorsReturned)
                .as("Not null returned").isNotNull()
                .as("All 4 order").hasSize(4);

        assertThat(contractorsReturned.stream()
                .allMatch(c -> c.getDateEnd() == null || c.getDateEnd().after(testDate)))
                .isTrue();
    }

    /**
     * Тестирование поиска всех активных контрагентов.
     *
     * @throws Exception Пробрасываем все исключения
     */
    @Test
    public void findByActiveTrue() throws Exception {
        Collection<Contractor> contractorsReturned = contractorRepository.findByActiveTrue();
        assertThat(contractorsReturned)
                .as("Not null returned").isNotNull()
                .as("All 3 order").hasSize(3);

        assertThat(contractorsReturned.stream()
                .allMatch(Contractor::isActive))
                .isTrue();
    }

    /**
     * Тестирование вывода всех контрагентов вне зависимости от статуса активности.
     *
     * @throws Exception Пробрасываем все исключения.
     */
    @Test
    public void findAll() throws Exception {
        assertThat(contractorRepository.findAll())
                .as("Not null returned").isNotNull()
                .as("All 5 order").hasSize(5);
    }

}